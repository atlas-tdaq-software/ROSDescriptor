// -*- c++ -*-
#ifndef DESCRIPTOR_READOUT_MODULE_H
#define DESCRIPTOR_READOUT_MODULE_H

#include <vector>
#include "ROSCore/ReadoutModule.h"
#include "ROSDescriptor/ObjectQueue.h"

namespace ROS {
   class RequestDescriptor;
   class DataRequestDescriptor;

   class DescriptorReadoutModule : public ReadoutModule {

   public:
      virtual void setCollectorQueue(
         ObjectQueue<RequestDescriptor>* descriptorQueue);

      virtual void registerChannels(RequestDescriptor* descriptor)=0;

      virtual void requestFragment(DataRequestDescriptor* descriptor)=0;

      virtual void clearFragments(std::vector<uint32_t>& level1Ids)=0;
   protected:
      ObjectQueue<RequestDescriptor>* m_collectorQueue;

   };

   inline void DescriptorReadoutModule::setCollectorQueue(
      ObjectQueue<RequestDescriptor>* descriptorQueue) {
      m_collectorQueue=descriptorQueue;
   }
}

#endif
