// -*- c++ -*-
// $Id: $ 

#ifndef DESCRIPTOREXCEPTION_H
#define DESCRIPTOREXCEPTION_H

#include "DFExceptions/ROSException.h"
#include <string>

class DescriptorException : public ROSException {

public:
   enum ErrorCode {INCOMPATIBLE_CHANNEL,
                   BAD_CHANNEL_ID,
                   TIMEOUT
   };

   DescriptorException(ErrorCode error) ;
   DescriptorException(ErrorCode error, std::string description) ;
   DescriptorException(ErrorCode error, const ers::Context& context) ;
   DescriptorException(ErrorCode error, std::string description, const ers::Context& context) ;
   DescriptorException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

protected:
   virtual ers::Issue * clone() const { return new DescriptorException( *this ); }
   static const char * get_uid() { return "ROS::DescriptorException"; }
   virtual const char* get_class_name() const {return get_uid();}
   virtual std::string getErrorString(unsigned int errorId) const;

};

inline
DescriptorException::DescriptorException(DescriptorException::ErrorCode error) 
   : ROSException("DescriptorPackage",error,getErrorString(error)) { }

inline
DescriptorException::DescriptorException(DescriptorException::ErrorCode error,
		std::string description) 
   : ROSException("DescriptorPackage",error,getErrorString(error),description) { }
inline
DescriptorException::DescriptorException(DescriptorException::ErrorCode error, const ers::Context& context) 
   : ROSException("DescriptorPackage",error,getErrorString(error),context) { }

inline
DescriptorException::DescriptorException(DescriptorException::ErrorCode error,
		std::string description, const ers::Context& context) 
   : ROSException("DescriptorPackage",error,getErrorString(error),description,context) { }

inline DescriptorException::DescriptorException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException(cause, "DescriptorPackage", error, getErrorString(error), description, context) {}

inline std::string DescriptorException::getErrorString(unsigned int errorId) const
{
	std::string result;    
   switch (errorId) {
   case INCOMPATIBLE_CHANNEL:
      result="Incompatible data channel. Only data channels subclassed from DescriptorDataChannel work with RequestDescriptors.";
      break;
   case BAD_CHANNEL_ID:
      result="Bad channel ID";
      break;
   case TIMEOUT:
      result="Timeout";
      break;
   default:
      result = "Unspecified error";
      break;
   }
   return(result);
}


#endif
