// -*- c++ -*-
#ifndef CLEARREQUESTDESCRIPTOR_H
#define CLEARREQUESTDESCRIPTOR_H

#include <vector>

#include "ROSDescriptor/RequestDescriptor.h"

namespace ROS {

   //! Descriptor for a clear request

   //! Clear data from all DataChannels
   class ClearRequestDescriptor : public RequestDescriptor {
   public:
      ClearRequestDescriptor();
      virtual ~ClearRequestDescriptor();

      /**
       * Initialise this request with information about which fragments
       * to clear.
       **/
      void initialise(unsigned int* level1Id, unsigned int nLevel1Ids,
                      unsigned int oldestValid,
                      unsigned int sequence);

      //void initialise(std::unique_ptr<std::vector<unsigned int> > level1IdList,
      void initialise(std::vector<unsigned int>& level1IdList,
                      unsigned int sequence);

      /**
       * Get a pointer to the array of L1 ids.
       **/
      unsigned int* level1Ids();

      /**
       * Get the number of L1 ids in this request
       **/
      unsigned int nLevel1Ids();

      /**
       * Get the oldest valid L1 id when this request was initialised
       **/
      unsigned int oldestValid();

      /**
       * Submit this request
       **/
      virtual void submit();



      void dump();
   private:
      unsigned int* m_level1Ids;
      unsigned int m_nLevel1Ids;
      unsigned int m_oldest;
   public:
      std::vector<unsigned int> m_l1idVector;
   };

   inline unsigned int* ClearRequestDescriptor::level1Ids() {
      return m_level1Ids;
   }
   inline unsigned int ClearRequestDescriptor::nLevel1Ids() {
      return m_nLevel1Ids;
   }
}
#endif
