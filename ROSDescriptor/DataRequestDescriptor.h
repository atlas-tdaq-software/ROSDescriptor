// -*- c++ -*-
#ifndef DATAREQUESTDESCRIPTOR_H
#define DATAREQUESTDESCRIPTOR_H

#include "ROSDescriptor/RequestDescriptor.h"
//---------
//#include "ROSEventFragment/FullFragment.h"
//---------
#include <sys/uio.h>

namespace ROS {
   //! Descriptor for a data request

   //! Descriptor for a request that gets data from a selection of
   //! DataChannels
   class DataRequestDescriptor : public RequestDescriptor {
   public:
      /**
       *  Constructor. Set the timeout here (same for all descriptors?
       *  Maybe should be static variable)
       **/
      DataRequestDescriptor(unsigned int timeout);

      virtual ~DataRequestDescriptor();

      /**
       *  Set up the descriptor for a particular data request (we
       *  re-use the same descriptor for many requests)
       **/
      void initialise(unsigned int level1Id,
                      const std::vector<unsigned int>* channelList,
                      unsigned int transactionId,
                      unsigned int sequence, unsigned int tag);


      /**
       * Return the L1 id of the current request
       **/
      unsigned int level1Id();

      /**
       *  Get the status of the request w.r.t. the given channel
       **/
      virtual unsigned int status(unsigned int channelId);

      /**
       *  Set the status of the request w.r.t. the given channel
       **/
      virtual void status(unsigned int channelId, unsigned int);

      /**
       * Is this request finished with?
       **/
      bool terminated();

      /**
       * Delcare this request finished with.
       **/
      void terminate();

      /**
       * Retry this request - only channels which haven't succeded so
       * far will be marked as not received.
       **/
      void retry();

      /**
       * Submit this request
       **/
      void submit();


      /**
       * Did any of the data channels in the request return a "May
       * come" status?
       **/
      bool channelsPending();

      /**
       * Test to see if the request has timed out
       **/
      bool timedOut();

      /**
       * Dump a textual description of the request to the cout stream
       **/
      virtual void dump();

      /**
       * Get the Transaction Id
       **/
      unsigned int transactionId();

      /**
       * Get the stream tag
       **/
      unsigned int tag();


      /**
       * Get an iovec with a FullFragment header and all the data
       * pages ready to send to the DC network or write to a file with
       * DataWriter
       **/
      virtual unsigned int fragment(struct iovec*,bool addHeader);

      /**
       *  Get the number of active channels in this request
       **/
      virtual unsigned int channelCount();


      bool complete();

      void submitted(bool);
      void expectReply(int nExpected=1);
      void gotReply();
      unsigned int getNumDMAs();
      void setNumDMAs(unsigned int numDMAS);
   private:
      unsigned int m_tag;

      unsigned int m_numDMAs;
   protected:
      unsigned int m_timeout;

      unsigned int m_transactionId;
      unsigned int m_level1Id;
      /**
       *  Map the chennel ID to the status retruned by the robin
       **/
      std::map<unsigned int, unsigned int> m_status;

      bool m_terminated;

      bool m_submitted;
      unsigned int m_repliesExpected;

//      FullFragment::FullHeader m_header;
      unsigned int m_header[32];
      struct iovec* m_ioVec;
   };

   inline unsigned int DataRequestDescriptor::level1Id() {
      return m_level1Id;
   }
   inline unsigned int DataRequestDescriptor::transactionId() {
      return m_transactionId;
   }
   inline unsigned int DataRequestDescriptor::status(unsigned int channelId) {
      return m_fragmentStatus[channelId];
   }
   inline void DataRequestDescriptor::status(unsigned int channelId, unsigned int status) {
      m_fragmentStatus[channelId]=status;
   }
   inline void DataRequestDescriptor::terminate() {
      m_terminated=true;
   }
   inline bool DataRequestDescriptor::terminated() {
      return m_terminated;
   }      
   inline unsigned int DataRequestDescriptor::tag() {
      return m_tag;
   }

inline void DataRequestDescriptor::submitted(bool flag) {
   m_submitted=flag;
}

inline void DataRequestDescriptor::expectReply(int nExpected) {
   m_repliesExpected+=nExpected;
}

inline void DataRequestDescriptor::gotReply() {
   m_nRepliesReceived++;
}

inline unsigned int DataRequestDescriptor::channelCount() {
   return channelList()->size();
}

}
#endif
