// -*- c++ -*- $Id:$
//
#ifndef OBJECTQUEUE_H
#define OBJECTQUEUE_H

#include <sched.h>
#include <iostream>
#include <mutex>
#include <thread>
#include <cerrno>
#include <string>

#define SEMAPHORE

#ifdef SEMAPHORE
# include <semaphore.h>
#endif


//! A simple thread safe queue using CAS based locks where needed.

//! A template queue that uses separate CAS based locks for queuing and
//! de-queuing allowing thread safety among multiple
//! producers/consumers without the producers/consumers locking each
//! other out.
//!
//! The fastPush and fastPop methods do not use the locking mechanism
//! at all allowing faster access in the case of a queue with a single
//! producer or consumer.
//!
//! The push methods will block until there is space to insert or the
//! queue is closed with a call to the close method.
//!
//! The pop methods will block until there is an object available or
//! the queue is closed with a call to the close method.
//!
template <class Type> class ObjectQueue {
public:
   ObjectQueue(int queueSize, std::string name="anonymous");
   ~ObjectQueue();
   /**
    *  Get a pointer to the object at the head of the queue and free
    * the bin it occupied. The queue will be locked against other
    * threads calling pop whilethis is done. If another thread has the
    * pop lock we will wait for it to become free before proceding. If
    * there are no entries in the queue, this method will block until
    * another thread writes to the queue or the queue is closed.
    */
   Type* pop();

   /**
    *  Get a pointer to the object at the head of the queue and free
    * the bin it occupied without locking aagainst other threads. This
    * should be used only when there is a single thread reading from
    * the queue.
    *
    * @param blocking Flag to say whether an empty queue should cause
    * us to block until something is written or return 0 immediately.
    */
   Type* fastPop(bool blocking=true);

   /**
    * Push a pointer to an object onto the queue after first obtaining
    * the write lock. If there is no space in the queue or another
    * thread has the write lock, we will wait.
    */
   void push(Type* object);

   /**
    * Push a pointer to an object onto the queue without obtaining
    * the write lock.
    *
    * @param object the object to be pushed
    *
    * @param blocking Flag to say whether if there is no space in the
    * queue we will wait or return false to say we failed.
    */
   bool fastPush(Type* object, bool blocking=true);

   Type* swap(Type* oldObject);

   /**
    * Close the queue. Force any threads waiting for a lock or for
    * data to become available to return now.
    */
   void close();

   /**
    * open the queue for business. Calls to push and pop will work
    * normally.
    */
   void open();


   void dump();

   int size();

private:
   enum Status {FREE,LOCKED,FULL};
#if 0
   enum Status m_pushLock;
   enum Status m_popLock;
#else
   std::mutex m_pushMutex;
   std::mutex m_popMutex;
#endif
   volatile struct Bin {
      enum Status status;
      Type* object;
   }* m_list;
   int m_head;
   int m_tail;
   int m_queueSize;

   unsigned int m_pushYields;
   unsigned int m_popYields;
   unsigned int m_objectsPushed;
   unsigned int m_objectsPopped;

   std::string m_name;
   volatile bool m_closed;
#ifdef SEMAPHORE
   sem_t m_semaphore;
#endif
};

template <class Type>
inline ObjectQueue<Type>::ObjectQueue(int queueSize, std::string name) : m_name(name) {
   m_list=new Bin[queueSize];
   for (int element=0; element<queueSize; element++) {
      m_list[element].status=FREE;
   }
   m_head=0;
   m_tail=0;
   m_queueSize=queueSize;
#if 0
   m_pushLock=FREE;
   m_popLock=FREE;
#endif
   open();
#ifdef SEMAPHORE
   sem_init(&m_semaphore, 0, 0);
   std::cout << "ObjectQueue constructor: semaphore version\n";
#else
   std::cout << "ObjectQueue constructor: polling version size=" << queueSize <<"\n";
#endif
}


template <class Type>
inline ObjectQueue<Type>::~ObjectQueue() {
   delete [] m_list;
#ifdef SEMAPHORE
   sem_destroy(&m_semaphore);
#endif
}


template <class Type>
inline void ObjectQueue<Type>::close() {
   if (!m_closed) {
      m_closed=true;
#ifdef SEMAPHORE
      sem_post(&m_semaphore);
#endif
      sched_yield();

      std::cout << "ObjectQueue::close()  objects pushed " << m_objectsPushed
                << ", objects popped " << m_objectsPopped
                << ", push yields " << m_pushYields
                << ", pop yields " << m_popYields
                << std::endl;
   }
}
template <class Type>
inline void ObjectQueue<Type>::open() {
   m_closed=false;
   m_popYields=0;
   m_pushYields=0;
   m_objectsPushed=0;
   m_objectsPopped=0;
}

template <class Type>
inline int ObjectQueue<Type>::size() {
   return m_objectsPushed-m_objectsPopped;
}


template <class Type>
inline void ObjectQueue<Type>::push(Type* object) {
#if 0
   while (!__sync_bool_compare_and_swap(&m_pushLock,FREE,LOCKED)) {
      if (m_closed) {
         return;
      }
      sched_yield();
   }
   // Now we know nobody else is going to change m_tail
   fastPush(object);
   m_pushLock=FREE;
#else
   std::lock_guard<std::mutex> lock(m_pushMutex);
   fastPush(object);
#endif
}

template <class Type>
inline bool ObjectQueue<Type>::fastPush(Type* object, bool blocking) {
   // we wait until the element that m_tail points to is free
   //   while (!__sync_bool_compare_and_swap(&m_list[m_tail].status,FREE,LOCKED)) {
   bool first=true;
   while (m_list[m_tail].status!=FREE) {
      if (first) {
         std::cout << "\n\t" << m_name << " fastPush Queue full\n";
         first=false;
      }
      if (!blocking) {
         return false;
      }
      if (m_closed) {
         return false;
      }
      m_pushYields++;
      std::this_thread::sleep_for(std::chrono::microseconds(1));
      //sched_yield();
   }
   m_list[m_tail].object=object;
   m_list[m_tail].status=FULL;
   if (++m_tail>=m_queueSize) {
      m_tail=0;
   }
   m_objectsPushed++;
#ifdef SEMAPHORE
   sem_post(&m_semaphore);
#endif
   return true;
}

template <class Type>
inline Type* ObjectQueue<Type>::swap(Type* oldObject) {
#if 0
   while (!__sync_bool_compare_and_swap(&m_popLock,FREE,LOCKED)) {
      if (m_closed) {
         return 0;
      }
      //      sched_yield();
   }
#else
   std::lock_guard<std::mutex> lock(m_popMutex);
#endif
   Type* object=oldObject;
   if (m_list[m_head].status==FULL) {
      object=m_list[m_head].object;
      m_list[m_head].object=oldObject;
   }
#if 0
   m_popLock=FREE;
#endif
   return object;
}

template <class Type>
inline Type* ObjectQueue<Type>::pop() {
#if 0
   while (!__sync_bool_compare_and_swap(&m_popLock,FREE,LOCKED)) {
      if (m_closed) {
         std::cout << "pop exiting, queue is closed\n";
         return 0;
      }
      //sched_yield();
   }
#else
   std::lock_guard<std::mutex> lock(m_popMutex);
#endif
   // Nobody will change m_head
   return fastPop();
}

template <class Type>
inline Type* ObjectQueue<Type>::fastPop(bool blocking) {
   if (m_closed) {
      return 0;
   }
   while (m_list[m_head].status!=FULL) {
      if (!blocking) {
         return 0;
      }
#ifdef SEMAPHORE
      sem_wait(&m_semaphore);
#else
      m_popYields++;
      sched_yield();
#endif
      if (m_closed) {
         std::cout << "queue closed\n";
         return 0;
      }
   }
   m_objectsPopped++;
   Type* object=m_list[m_head].object;
   m_list[m_head].status=FREE;
   if (++m_head>=m_queueSize) {
      m_head=0;
   }
#if 0
   m_popLock=FREE;
#endif
   return object;
}


template <class Type>
inline void ObjectQueue<Type>::dump() {
   std::cout << "** ObjectQueue size=" << m_queueSize
             << " head=" << m_head << " tail=" << m_tail << std::endl;
   for (int entry=0; entry<m_queueSize; entry++) {
      std::cout << "  " << m_list[entry].status;
   }
      std::cout << " objects pushed " << m_objectsPushed
                << ", objects popped " << m_objectsPopped
                << ", push yields " << m_pushYields
                << ", pop yields " << m_popYields
                << "**" << std::endl;
}
#endif
