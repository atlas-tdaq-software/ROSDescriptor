// -*- c++ -*-

#ifndef REQUESTDESCRIPTOR
#define REQUESTDESCRIPTOR

#include <vector>
#include <map>
#include <chrono>

#include "tbb/concurrent_unordered_map.h"

#include "ROSDescriptor/DescriptorDataChannel.h"

namespace ROS {
   //!  Abstract request descriptor class.

   //!  A pool of RequestDescriptor objects will be created at configuration
   //! time and pointers to these objects will be passed among the various 
   //! threads as each request progresses.
   class RequestDescriptor {
   public:
      RequestDescriptor();
      virtual ~RequestDescriptor();


      static RequestDescriptor* getDescriptor(uint16_t id);

      uint16_t getId();
      /**
       *  Since RequestDescriptors are re-used many times, all Request
       *  specific initialisation is done here rather than in the
       *  constructor
       **/
//      void initialise(std::unique_ptr<std::vector<unsigned int> > channelList, unsigned int sequence);
      void initialise(const std::vector<unsigned int>* channelList,
                      unsigned int sequence);

      /**
       *  Set the completion status for given channel and return true
       *  if all channels are now set.
       **/
      bool complete(unsigned int channelId);

      void gotData(unsigned int channelIndex);

      /**
       *  List of DataChannels involved in current Request (filled by
       *  initialise)
       **/
      std::vector<unsigned int>* channelList();
      std::vector<unsigned int>* allChannelList();

      std::map<unsigned int, DescriptorDataChannel*>* channels();

      unsigned int addChannel(unsigned int channelId,
                              unsigned int* pageVirtualAddress,
                              unsigned int* pagePhysicalAddress,
                              unsigned int headersize=0);


      std::vector<unsigned int>* headerPage(unsigned int channelIndex);

      unsigned int sequenceNumber();

      /**
       *  check whether we've received a response from this channel
       **/
      bool responseReceived(unsigned int channelId);

      /**
       *  How many times has this descriptor been submitted since
       *  initialise()
       **/
      unsigned int tries();

      virtual void dump(){};

      /**
       *  Get the DataPage associated with this channel.
       **/
      DataPage* dataPage(unsigned int channelIndex);


      unsigned int getChannelIndex(unsigned int channelId);

      std::vector<unsigned int*>* pages(){return &m_pageVec;};
      std::vector<unsigned int>* localIds();
   private:
   protected:
      std::vector<unsigned int> m_allChannelList;
      std::vector<unsigned int> m_channelList;
      std::vector<unsigned int> m_localList;
      std::vector<unsigned int> m_fragmentStatus;

      std::map<unsigned int, unsigned int> m_channelMap;

      std::map<unsigned int, DescriptorDataChannel*> m_channels;

      /**
       *  A DataPage will be allocated for each DataChannel by the
       *  constuctor
       **/
      std::vector<DataPage*> m_dataPage;

      std::vector<std::vector<unsigned int>* > m_headerPage;

      std::vector<unsigned int*> m_pageVec;

//      tbb::concurrent_unordered_map<unsigned int, bool> m_responseReceived;
      std::vector<bool> m_responseReceived;


      unsigned int m_nChannels;

      /**
       *  Count of the number of channels that have received a reply
       *  from the robin
       **/
      unsigned int m_nRepliesReceived;

      /**
       *  Timestamp of when this descruptor wasinitialised. Used to
       *  check for timeouts. should it be here or in
       *  DataRequestDescriptor?
       **/
      std::chrono::time_point<std::chrono::system_clock> m_initialiseTime;

      /**
       *  The sequence number of the request. Set by initialise, only
       *  a handle for debugging, maybe should be done away with!!
       **/
      unsigned int m_sequenceNumber;

      /**
       *  How many times this request has been tried.  Maybe should be
       *  in DataRequestDescriptor subclass only!!
       **/
      unsigned int m_tries;


      /**
       * Identifier of this descriptor (index into static vector below)
       **/
      uint16_t m_id;


      static uint16_t s_nextId;
      static std::vector<RequestDescriptor*> s_descriptorVector;
      static uint16_t s_nDescriptors;
   };

   inline uint16_t RequestDescriptor::getId() {
      return m_id;
   }

   inline unsigned int RequestDescriptor::getChannelIndex(unsigned int channelId) {
      return m_channelMap[channelId];
   }

   inline RequestDescriptor* RequestDescriptor::getDescriptor(uint16_t id) {
      if (id<s_nextId) {
         return s_descriptorVector[id];
      }
      else {
         std::cerr << "Invalid descruptor ID " << id << std::endl;
         return 0;
      }
   }
   inline std::vector<unsigned int>* RequestDescriptor::channelList() {
//       if (m_channelList.size()!=0) {
         return &m_channelList;
//       }
   }

   inline std::vector<unsigned int>* RequestDescriptor::allChannelList() {
      return &m_allChannelList;
   }

   inline std::map<unsigned int, DescriptorDataChannel*>* RequestDescriptor::channels() {
      return &m_channels;
   }

   inline unsigned int RequestDescriptor::sequenceNumber() {
      return m_sequenceNumber;
   }
   inline bool RequestDescriptor::responseReceived(unsigned int channelId) {
      return m_responseReceived[channelId];
   }
   inline unsigned int RequestDescriptor::tries() {
      return m_tries;
   }
}
#endif
