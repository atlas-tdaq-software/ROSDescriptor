// -*- c++ -*-
// $Id: DataChannel.h 94613 2010-09-02 14:20:57Z gcrone $

/*
  ATLAS ROS Software

  Class: DataChannel
  Authors: ATLAS ROS group 	
*/


#ifndef DESCRIPTORDATACHANNEL_H
#define DESCRIPTORDATACHANNEL_H

#include <vector>
#include <map>
#include <sstream>

#include "ROSDescriptor/ObjectQueue.h"
#include "ROSDescriptor/DataPage.h"
#include "ROSCore/DataChannel.h"
#include "ROSEventFragment/EventFragment.h"

class ISInfo;

namespace ROS {

   class RequestDescriptor;
   class ClearRequestDescriptor;
   class DataRequestDescriptor;
   class MemoryPool;


   /**
    * \brief Interface with the individual input data channels.
    *
    * Provide an API for requesting, retrieving, and deleting data fragments 
    * from an individual channel of an input module, e.g. one of the
    * ROLs connected to a ROBIN or one of the FEB conected to a ROD emulator.
    */
   class DescriptorDataChannel : public DataChannel {
   public:
      DescriptorDataChannel(unsigned int id, unsigned int configId,
                            unsigned int physicalAddress,
                            unsigned int ackQueueSize,
                            MemoryPool* mpool=(MemoryPool*)0) ;
      virtual ~DescriptorDataChannel() ;

      virtual void descriptorReleaseFragment(ClearRequestDescriptor* descriptor)=0;

      virtual void descriptorRequestFragment(DataRequestDescriptor* descriptor)=0;


      /**
       * Get a descriptor of a memory page from a pool associated with
       * this data channel.
       */
      virtual DataPage* getPage();

      /**
       * Callback to indicate that this channel has responded to the
       * request described by the RequestDescriptor. Can do error
       * checking etc. of any data from this channel before passing the
       * descriptor to the acknowledgement queue.
       */
      virtual void gotFragment(RequestDescriptor* descriptor)=0;

      /**
       * Get the acknowledgement queue associated with this
       * channel. The queue will be written to by the gotFragment
       * method and read by the thread which handles the completed
       * requests.
       */
      ObjectQueue<RequestDescriptor>* getAckQueue();

  protected:
      ObjectQueue<RequestDescriptor>* m_ackQueue;
      MemoryPool *m_memoryPool;
  };


}
#endif //DATACHANNEL_H
