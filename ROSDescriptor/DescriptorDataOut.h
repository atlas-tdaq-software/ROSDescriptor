// -*- c++ -*-
/*
  ATLAS ROS Software

  Class: DescriptorDataOut
  Authors: ATLAS ROS group 	

  $Log$
*/

#ifndef DESCRIPTORDATAOUT_H
#define DESCRIPTORDATAOUT_H

#include "ROSCore/DataOut.h"
/** @interface */
namespace ROS {
   class DataRequestDescriptor;

   //! Descriptor version of DataOut

   //! Abstract DataOut plugin for use with descriptor passing thread
   //! organisation.
   class DescriptorDataOut : public DataOut
   {
   public:  
      DescriptorDataOut(DataOut::Type type=MAIN);
      virtual ~DescriptorDataOut() noexcept {};

      //! Send/write the data from the descriptor
      virtual void send(DataRequestDescriptor* /*descriptor*/)=0;
   };

   inline DescriptorDataOut::DescriptorDataOut(Type type) : DataOut(type) {
   }
}
#endif //DATAOUT_H
