// -*- c++ -*- $Id: DcTriggerIn.h 45383 2008-02-13 17:47:44Z gcrone $
// ///////////////////////////////////////////////////////////////////////////
//
// $Log$
//
// ///////////////////////////////////////////////////////////////////////////

#ifndef DESCRIPTORTRIGGER_H
#define DESCRIPTORTRIGGER_H

#include <mutex>
//#include <atomic>

#include "ROSCore/TriggerIn.h"

#include "ROSDescriptor/DataRequestDescriptor.h"
#include "ROSDescriptor/ClearRequestDescriptor.h"

template <class type> class ObjectQueue;

typedef unsigned int NodeID;
namespace ROS {
   class DataCollector;
   class DescriptorReadoutModule;

   //! Descriptor version of TriggerIn

   //! Abstract TriggerIn class based on descriptor passing threading
   //! 
   class DescriptorTrigger : public TriggerIn {
   protected:
      DescriptorTrigger();
      void getModules(ROS::IOManager* iom);
      void setupQueues(DFCountedPointer<Config>);
      void deleteQueues();
      void start();
      void stop();
      void submitDataRequest(unsigned int level1Id, const std::vector<unsigned int>* dataChannels,
                             unsigned int transactionId,
                             unsigned int requestNumber, unsigned int tag);
      void submitClearRequest(unsigned int* level1IdList, unsigned int nClears,
                              unsigned int lastValid,
                              unsigned int sequence);
      virtual int processDescriptorQueue();

      virtual void createDescriptors(DFCountedPointer<Config> config);

      virtual void checkDescripors();

      void user(const daq::rc::UserCmd& command);
      //   private:

      DataCollector* m_dataCollector;

      //RequestDescriptor** m_dataDescriptors;
      std::vector<RequestDescriptor*> m_dataDescriptors;

      //std::atomic<unsigned int> m_nextDataDescriptor;
      unsigned int m_nextDataDescriptor;


      //      ClearRequestDescriptor** m_clearDescriptors;
      std::vector<ClearRequestDescriptor*> m_clearDescriptors;
      unsigned int m_nextClearDescriptor;
      ObjectQueue<RequestDescriptor>* m_descriptorQueue;

      unsigned int m_nDataDescriptors;
      unsigned int m_nClearDescriptors;
      bool m_triggerActive;
      std::mutex m_mutex;
      std::vector<DescriptorReadoutModule*> m_readoutModules;
   };
}

#endif
