// -*- c++ -*-
//
#ifndef ROS_DATA_DESCRIPTOR_OUTPUT_MSG_H
#define ROS_DATA_DESCRIPTOR_OUTPUT_MSG_H

#include "ROSasyncmsg/RosDataMsg.h"
#include "DFRequestDescriptor.h"
#include "ROSDescriptor/ObjectQueue.h"

namespace ROS {
class RosDataDescriptorOutputMsg: public RosDataMsg,
                        public daq::asyncmsg::OutputMessage {
public:

   explicit RosDataDescriptorOutputMsg(std::uint32_t transactionId,
                                       DFRequestDescriptor* descriptor,
                                       ObjectQueue<RequestDescriptor>* freeQ)
      : RosDataMsg(transactionId),m_descriptor(descriptor),m_freeQueue(freeQ) {
   };

   /**
    *  Destructor of the message must put the descriptor on the free queue
    **/
   virtual ~RosDataDescriptorOutputMsg(){
      //std::cout << "RosDataDescriptorOutputMsg destructor\n";
      m_freeQueue->push(m_descriptor);
   };

   virtual std::uint32_t size() const {
      return m_descriptor->size();
   }


   virtual void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const {
      //std::cout << "RosDataDescriptorOutputMsg::toBuffers()\n";
      auto channelList=m_descriptor->localIds();
      for (auto channel : *channelList) {
         auto page=m_descriptor->dataPage(channel);
         unsigned int* data=page->virtualAddress();
         std::vector<unsigned int>* header=m_descriptor->headerPage(channel);
         size_t dataSize;

         if (header!=0) {
            unsigned int headerSize=header->at(2)*sizeof(unsigned int);
            buffers.emplace_back(header->data(),headerSize);
            dataSize=header->at(1)*sizeof(unsigned int)-headerSize;
         }
         else {
            dataSize=data[1]*sizeof(unsigned int);
         }
         buffers.emplace_back(data,dataSize);
      }
   }

   DFRequestDescriptor* m_descriptor;
   ObjectQueue<RequestDescriptor>* m_freeQueue;
};
}
#endif
