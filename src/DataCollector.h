// -*- c++ -*-
#ifndef DATACOLLECTOR_H
#define DATACOLLECTOR_H

#include <vector>
#include <map>

#include <thread>

template <class type> class ObjectQueue;

namespace ROS {
   class RequestDescriptor;
   class DataRequestDescriptor;


   //! Data collector thread.

   //! Process request descriptors input via the data channels' ack queue
   //! pass completed data requests to the sending thread. 
   class DataCollector {
   public:
      //! Constructor is given a pointer to the queue used to return
      //! completed RequestDescriptors to the Trigger thread for re-use. 
      DataCollector(ObjectQueue<RequestDescriptor>* returnQueue);
      virtual ~DataCollector();


      void cleanup();

      //! Start any pre-existing SendThreads and associated queues.
      virtual void start();

      //! Stop the DataCollector thread and all the SendThread threads.
      virtual void stop();

      //! For debugging, write a textual description of the state of the
      //! DataCollector to the cout stream.
      virtual void dump();

   protected:
      //! The action thread of the DataCollector
      virtual void run();

      std::thread* m_thread;

      std::vector<unsigned int> m_channelId;
      //! Vector of input queues (one per DescriptorDataChannel) for
      //! RequestDescriptors that have been handled by the channel
      std::vector<ObjectQueue<RequestDescriptor>* > m_channelQueue;

      //! Single queue for returning RequestDescriptors to the TriggerIn
      ObjectQueue<RequestDescriptor>* m_returnQueue;

      bool m_runActive;
      unsigned int m_stopTimeout;

   private:
      // Vector of input queues (one per SendThread) for DataRequestDescriptors
      // that have been sent by the DataOut
      std::vector<ObjectQueue<DataRequestDescriptor>* > m_ackQueue;

      //! Vector of output queues (one per SendThread) for 
      //! DataRequestDescriptors to be sent to the DataOut
      std::vector<ObjectQueue<DataRequestDescriptor>* > m_sendQueue;

      std::map<unsigned int,unsigned int> m_outMap;

   };
}

#endif
