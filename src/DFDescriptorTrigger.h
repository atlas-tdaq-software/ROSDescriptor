// -*- c++ -*-
// $Id:$ 
//
// $Log:$
//
#ifndef DFDESCRIPTOR_TRIGGER_H
#define DFDESCRIPTOR_TRIGGER_H

#include <mutex>
#include <chrono>

#include "ROSasyncmsg/DataServer.h"
#include "ROSasyncmsg/ClearSession.h"
#include "ROSDescriptor/DescriptorTrigger.h"
#include "ROSInfo/DFDescriptorTriggerInfo.h"
#include "ROSDescriptor/DescriptorReadoutModule.h"

#include "monsvc/ptr.h"
#include "monsvc/MonitoringService.h"

#include "dal/Partition.h"
#include "TH1F.h"

namespace daq{
   namespace asyncmsg{
      class NameService;
   }
}
namespace ROS{

   class DescriptorDataServer;
   class DescriptorDataServerSession;
   class DFDataCollector;
   class DFRequestDescriptor;

   //! ROS DataFlow Trigger

   //! Listen for 'requests' from DataFlow for data or clears
   class DFDescriptorTrigger : public DescriptorTrigger {
   public:
      DFDescriptorTrigger();
      ~DFDescriptorTrigger() noexcept;


      ISInfo* getISInfo();
      void submitDataRequest(unsigned int level1Id,
//                             std::unique_ptr<std::vector<unsigned int> > dataChannels,
                             std::vector<unsigned int>* dataChannels,
                             DescriptorDataServerSession* session,
                             unsigned int transactionId);


      void submitClearRequest(std::vector<unsigned int>& level1IdList);

      void resubmit(DataRequestDescriptor* descriptor);

      void finalise(DFRequestDescriptor* descriptor);
   private:
      virtual int processDescriptorQueue();

      //! From DFThread not needed
      virtual void run(){};
      //! From DFThread not needed
      virtual void cleanup(){};

      //! From IOMPlugin
      virtual void configure(const daq::rc::TransitionCmd&);
      virtual void unconfigure(const daq::rc::TransitionCmd&);

      virtual void prepareForRun(const daq::rc::TransitionCmd&);
      virtual void setup(ROS::IOManager*, DFCountedPointer<Config>);
      virtual void stopGathering(const daq::rc::TransitionCmd&);

      unsigned int triggerCount();

      void resetCounters();

      //
      std::unique_ptr<std::vector<boost::asio::io_service> > m_dataIoServices;
      std::unique_ptr<std::vector<boost::asio::io_service> >m_clearIoService;

      //! Server to field data requests, inprinciple we should
      //! have a ClearServer as well but for the moment the DataServer handles
      //! clears as well
      std::shared_ptr<DataServer> m_dataServer;
      std::shared_ptr<DataServer> m_clearServer;
      std::shared_ptr<ClearSession> m_clearSession;

      daq::asyncmsg::NameService* m_nameService;

      int m_nIOServices;
      int m_nDataServerThreads;
      IPCPartition m_ipcPartition;
      bool m_standalone;
      unsigned int m_dataRequestTimeout;

      DFDescriptorTriggerInfo m_stats;
      DFDescriptorTriggerInfo m_lastStats;

      std::chrono::time_point<std::chrono::system_clock> m_counterResetTime;
      std::chrono::time_point<std::chrono::system_clock> m_lastStatsTime;

      std::mutex m_dataReqMutex;
      std::mutex m_clearReqMutex;

      std::atomic<unsigned long> m_clearRequestsReceived;
      std::atomic<unsigned long> m_requestsReceived;

      std::vector<DescriptorReadoutModule*> m_readoutModules;

      ObjectQueue<RequestDescriptor>* m_collectorQueue;

      monsvc::MonitoringService& m_monitoringService;
      monsvc::ptr<TH1F> m_timeHistogram;
      monsvc::ptr<TH1F> m_sizeHistogram;
   };



   class DescriptorDataServer: public DataServer {
   public:
#ifdef SINGLE_IOSERVICE
      DescriptorDataServer(boost::asio::io_service& ioService, IOManager* iom,
                           daq::asyncmsg::NameService* nameService,
                           DFDescriptorTrigger* trigger);
#else
      DescriptorDataServer(std::vector<boost::asio::io_service>& ioService,
                           const std::string& iomName,
                           daq::asyncmsg::NameService* nameService,
                           DFDescriptorTrigger* trigger);
#endif
      virtual ~DescriptorDataServer();
   private:
      DFDescriptorTrigger* m_trigger;
      virtual void onAccept(std::shared_ptr<daq::asyncmsg::Session>) noexcept;
   };


   class DescriptorDataServerSession: public DataServerSession {

   public:
      DescriptorDataServerSession(boost::asio::io_service& ioService,
                                  DFDescriptorTrigger* trigger);
      virtual ~DescriptorDataServerSession();
   private:
      DFDescriptorTrigger* m_trigger;
      virtual void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message);
      virtual void onReceive(std::uint32_t typeId, std::uint32_t transactionId) noexcept;
      virtual void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage>  message) noexcept;
   };


   class DescriptorClearSession: public ClearSession {
   public:
      DescriptorClearSession(boost::asio::io_service& ioService,
                             const std::string& mcAddress,
                             const std::string& mcInterface,
                             DFDescriptorTrigger* trigger):
         ClearSession(ioService, mcAddress, mcInterface),
         m_trigger(trigger) {};
      virtual ~DescriptorClearSession(){};
   private:
      virtual void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message);
      DFDescriptorTrigger* m_trigger;
   };

}
#endif
