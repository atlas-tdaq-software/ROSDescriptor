// -*- c++ -*-
#ifndef DFREQUESTDESCRIPTOR_H
#define DFREQUESTDESCRIPTOR_H

#include "ROSDescriptor/DataRequestDescriptor.h"

namespace ROS {
   class DataServerSession;

   //! Descriptor for a data request

   //! Descriptor for a request that gets data from a selection of
   //! DataChannels
   class DFRequestDescriptor : public DataRequestDescriptor {
   public:
      /**
       *  Constructor. Set the timeout here (same for all descriptors?
       *  Maybe should be static variable)
       **/
      DFRequestDescriptor(unsigned int timeout);

      virtual ~DFRequestDescriptor();

      /**
       *  Set up the descriptor for a particular data request (we
       *  re-use the same descriptor for many requests)
       **/
      void initialise(unsigned int level1Id,
//                      std::unique_ptr<std::vector<unsigned int> > channelList,
                      const std::vector<unsigned int>* channelList,
                      DataServerSession* destination,
                      unsigned int transactionId,
                      std::chrono::time_point<std::chrono::system_clock> requestTime);
      /**
       * Send the data to the requester
       **/
      virtual void send(ObjectQueue<RequestDescriptor>* ackQueue);

      /**
       * Dump a textual description of the request to the cout stream
       **/
      virtual void dump();


      unsigned int status(unsigned int channelIndex);

      /**
       *  Set the status of the request w.r.t. the given channel
       **/
      void status(unsigned int channelIndex, unsigned int);

      std::chrono::time_point<std::chrono::system_clock> requestTime() {return m_requestTime;};

      unsigned int size();

      unsigned int nChannels();

   private:
      DataServerSession* m_destination;
      std::chrono::time_point<std::chrono::system_clock> m_requestTime;
   };

   inline unsigned int DFRequestDescriptor::status(unsigned int channelIndex) {
      return m_fragmentStatus[channelIndex];
   }
   inline void DFRequestDescriptor::status(unsigned int channelIndex, unsigned int status) {
      m_fragmentStatus[channelIndex]=status;
   }
}
#endif
