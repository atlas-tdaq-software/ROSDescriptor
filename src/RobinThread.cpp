/****************************************************************/
/* file: RobinThread.cpp						*/

#ifndef HOST
  #define HOST
#endif
 
#include <iostream>
#include <new>           // This forces new to throw an execption if no memory could be allocated instead if returning 0
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <errno.h>

#include "RobinThread.h"
#include "RolThread.h"
#include "ROSRobin/Rol.h"


#include "DFDebug/DFDebug.h"
#include "DFThreads/DFThread.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"
#include "cmem_rcc/cmem_rcc.h"
#include "ROSRobin/ROSRobinExceptions.h"
//#include "robin_ppc/robin.h"

#include "ROSDescriptor/DescriptorDataChannel.h"
#include "ROSDescriptor/RequestDescriptor.h"

using namespace ROS;

unsigned int RobinThread::Message::s_serial=0;

/***********************************************************/
RobinThread::Message::Message(u_int rolIndex, u_int service,
                              u_long replyAddress,
                              u_int size, u_int *data,
                              u_int rolId,
                              RequestDescriptor* descriptor)
   : m_rolIndex(rolIndex),m_service(service), m_replyAddress(replyAddress), m_size(size),
/***********************************************************/
   m_rolId(rolId), m_requestDescriptor(descriptor) {
   m_data=new u_int[size];
   for (u_int word=0; word<size; word++) {
      m_data[word]=data[word];
   }
   //   m_serialNumber=s_serial++;
}


RobinThread::Message::~Message() {
   delete [] m_data;
}

u_int RobinThread::Message::rolIndex(){
   return m_rolIndex;
}

u_int RobinThread::Message::rolId(){
   return m_rolId;
}

u_int RobinThread::Message::service(){
   return m_service;
}

u_long RobinThread::Message::replyAddress(){
   return m_replyAddress;
}

u_int RobinThread::Message::size(){
   return m_size;
}

u_int* RobinThread::Message::data(){
   return m_data;
}

RequestDescriptor* RobinThread::Message::descriptor(){
   return m_requestDescriptor;
}

/***************************************/
void RobinThread::Message::dump() const {
/***************************************/
   unsigned int* replyPtr=reinterpret_cast<unsigned int*> (m_replyAddress);
   std::cout << "RobinThread::Message rolIndex=" << m_rolIndex
             << " service=" << m_service
             << " replyAddress=0x" << HEX(m_replyAddress)
             << " *reply=0x" << HEX(*replyPtr)
             << " size=0x" << HEX(m_size)
      //            << " mesage index=0x" << HEX(m_msgOffset)
      //            << " message number=" << m_serialNumber
             << std::endl;
   if (m_requestDescriptor) {
      m_requestDescriptor->dump();
   }
}

/***********************************************************************/
RobinThread::RobinThread(u_int slotNumber, float timeout, int queueSize)
   : Robin(slotNumber,timeout),m_doneCount(0), m_threadStarted(false) {
/***********************************************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinThread::constructor: Called for card " << slotNumber);
   m_descrMessageQueue=new ObjectQueue<Message>(queueSize);
   m_miscMessageQueue=new ObjectQueue<Message>(queueSize);

   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinThread::constructor: OK");
}


/***************************/
RobinThread::~RobinThread() {
/***************************/
   if (m_threadStarted) {
      stop();
   }

   delete m_descrMessageQueue;
   delete m_miscMessageQueue;
}

/**************************************/
Rol* RobinThread::newRol(u_int number) {
/**************************************/
   return new RolThread(this, number);
}

/***************************************************/
RolThread& RobinThread::getRolThread(u_int rolIndex){
/***************************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "Robin::getRolThread: called with rolIndex = " << rolIndex);

   // Check if the Robin object has been opened. Throws an exception if not!!
   isOpen();

   // If the client requests a ROL number more then 2, throw an exception!
   if (rolIndex > 2) {
      DEBUG_TEXT(DFDB_ROSFM, 5, "Robin::getRolThread: Illegal value passed for rolIndex");
      CREATE_ROS_EXCEPTION(ex20, ROSRobinExceptions, NROLS, "Number of requested ROLs exceeded 3!");
      throw (ex20);
   }

   // Get the Rol object from the array
   RolThread* ret=dynamic_cast<RolThread*>(m_rolVector[rolIndex]);

   // Check if it has been filled in. This could not be done in case of a ROBIN prototype
   // which has only two ROLs!
   if (ret == 0) {
      DEBUG_TEXT(DFDB_ROSFM, 5, "Robin::getRolThread: failed to find the rol object in m_rolVector");
      CREATE_ROS_EXCEPTION(ex21, ROSRobinExceptions, NROLS, "Requrested ROL is not available!");
      throw (ex21);
   }

   return *ret;
}



/*****************************************************************/
void RobinThread::sendMsg(u_int rolIndex, u_int service, u_long replyAddress) 
/*****************************************************************/
{  
  isOpen();
  DEBUG_TEXT(DFDB_ROSFM, 20, "Robin::sendMsg(1): rolIndex = " << rolIndex);
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::sendMsg(1): service = " << service);
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::sendMsg(1): replyAddress = 0x" << HEX(replyAddress));

  u_int data[1];
  sendMsg(rolIndex, service, replyAddress, 0, data); 
}


/**********************************************************************************/
void RobinThread::sendMsg(u_int rolIndex, u_int service, u_long replyAddress, u_int parameter) 
/**********************************************************************************/
{
  isOpen();
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::sendMsg(2): rolIndex = " << rolIndex);
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::sendMsg(2): service = " << service);
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::sendMsg(2): replyAddress = 0x" << HEX(replyAddress));
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::sendMsg(2): parameter = " << parameter);

  u_int data[1];
  data[0] = parameter;
  sendMsg(rolIndex, service, replyAddress, 1, data); 
}


/*****************************************************************************************************/
void RobinThread::sendMsg(u_int rolIndex, u_int service, u_long replyAddress, u_int parameter1, u_int parameter2) 
/*****************************************************************************************************/
{
  isOpen();
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::sendMsg(3): rolIndex = " << rolIndex);
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::sendMsg(3): service = " << service);
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::sendMsg(3): replyAddress = 0x" << HEX(replyAddress));
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::sendMsg(3): parameter1 = " << parameter1);
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::sendMsg(3): parameter2 = " << parameter2);

  u_int data[2];
  data[0] = parameter1;
  data[1] = parameter2;
  sendMsg(rolIndex, service, replyAddress, 2, data);
}


/******************************************************************************************/
void RobinThread::sendMsg(u_int rolIndex, u_int service, u_long replyAddress, u_int size, u_int *data) 
/******************************************************************************************/
{
  isOpen();

  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::sendMsg(4): rolIndex = " << rolIndex);
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::sendMsg(4): service = " << service);
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::sendMsg(4): replyAddress = 0x" << HEX(replyAddress));
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::sendMsg(4): size = 0x" << HEX(size));
  queueMsg(rolIndex, service, replyAddress, size, data);
}


/*****************************************************************************/
void RobinThread::queueMsg(u_int rolIndex, u_int service, u_long replyAddress,
                           u_int size, u_int *data, unsigned int rolId,
                           RequestDescriptor* descriptor) {
/*****************************************************************************/
  isOpen();
#if 0
  std::cout << "RobinThread::queueMsg(4): rolIndex=" << rolIndex
            << " service=" << service
            << " replyAddress=0x" << HEX(replyAddress)
            << " size=0x" << HEX(size)
            << " rolId=0x" << HEX(rolId)
            << std::endl;
#endif
  //Clear the first word of the message buffer (for polling in receiveMsg)
  u_int *ptr = reinterpret_cast<u_int *> (replyAddress);
  *ptr=0;

  Message* message=new Message(rolIndex, service, replyAddress, size,
                               data, rolId, descriptor);
  if (descriptor) {
     m_descrMessageQueue->fastPush(message);
  }
  else {
     m_miscMessageQueue->push(message);
  }
}

/***************************************************/
bool RobinThread::processMessage(Message* message) {
/***************************************************/
   u_int messageSize = 12 + (message->size() * 4);
   //      std::cout << "RobinThread::run: messageSize = " << messageSize << std::endl;

   int messageIndex = getDPMandFIFO(messageSize);
   if (messageIndex<0) {
      return false;
   }

   DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::run: messageIndex = 0x" << HEX(messageIndex));

   u_long pc_mem_offset = (message->replyAddress() - (u_long)reply_mem.user_virt_base) + m_mem_offset;
   DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::run: reply_mem.user_virt_base        =  0x" << HEX((u_long)reply_mem.user_virt_base));
   DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::run: reply_mem.user_phys_base        =  0x" << HEX(reply_mem.user_phys_base));
   DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::run: pc_mem_offset                   =  0x" << HEX(pc_mem_offset));

   //Write the message to the DPM
   u_int windex = messageIndex >> 2;  //convert from byte to word
   DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::run: windex = 0x" << HEX(windex));
   DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::run: Writing message to DPM virt. address " << HEX(&msg_dpm.user_virt_base[windex]));

   msg_dpm.user_virt_base[windex++]=(message->rolIndex() << 16) | message->service();
   msg_dpm.user_virt_base[windex++]=s_direct_dma_offset | pc_mem_offset;
   msg_dpm.user_virt_base[windex++]=0;
   u_int* data=message->data();
   for (u_int loop = 0; loop < message->size(); loop++) {
      DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::run: msg word " << loop + 4 << " = 0x" << HEX(data[loop]));
      msg_dpm.user_virt_base[windex++] = data[loop]; 
   }
    
   //Write a word to the FIFO    
   u_int fifo_data = ((messageSize >> 2) << 16) | ((m_dpm_offset + messageIndex) >> 2);
   //   std::cout << "msg start 0x" << HEX(messageIndex) << ", msg number "<< message->m_serialNumber << " writing 0x" << HEX(fifo_data) << " to the message FIFO\n";
   fpga_win.user_virt_base[s_fifo_index >> 2] = fifo_data;


   //   message->m_msgOffset=messageIndex;

   if (message->descriptor()!=0) {
      m_outStanding.push(message);
   }
   else {
      delete message;
   }

   return true;
}

/*************************/
void RobinThread::dump() {
/*************************/
   std::cout << "RobinThread slot=" << m_slot_number
             << " open=" << m_is_open
             << " thread condition=" << getCondition()
             << " Number of Requests outstanding=" << m_outStanding.size()
             << " fifo slots=" << m_nFifoSlots
             << " pending=" << m_pending
             << " done=" << m_doneCount
             << std::endl;


   std::cout << "dpm current index=" <<m_dpm_current_index;
   for (unsigned int index=0;index<m_nFifoSlots;index++) {
      std::cout << "   start[" << index << "]=" << m_dpm_start[index];
      if (index%8==7) std::cout << std::endl;
   }
   std::cout << std::endl;
   std::cout << "Descriptor input queue\n";
   m_descrMessageQueue->dump();
   std::cout << "Misc input queue\n";
   m_miscMessageQueue->dump();

   std::cout << "Outstanding requests:\n";
   int nreq=0;
   std::queue<Message*> tq=m_outStanding;
   while (!tq.empty()) {
      Message* msg=tq.front();
      std::cout << "outstanding " << nreq++ << ": ";
      msg->dump();
      tq.pop();
   }

}


/************************/
void RobinThread::run() {
/************************/
   m_threadStarted=true;

   struct timeval startTime;
   struct timezone dummy;
   gettimeofday(&startTime,&dummy);

   struct rusage resourcesStart;
   int status=getrusage(RUSAGE_SELF, &resourcesStart);
   if (status) {
      perror("Error from getrusage");
   }

   const int NBINS=50;
   int npolls=0;
   int pollHist[NBINS+1]={0};

   Message* message=0;
   while (m_threadStarted) {
      int actions=0;

      if (checkPending()) {
         if (message==0) {
            message=m_miscMessageQueue->fastPop(false);
            if (message) {
#if 0
               std::cout << "RobinThread::run dequeued misc message  -  ";
               message->dump();
#endif
            }
            else if (m_outStanding.size() < m_nFifoSlots-2) {
               message=m_descrMessageQueue->fastPop(false);
               if (message) {
#if 0
                  std::cout << "RobinThread::run dequeued descriptor message  -  ";
                  message->dump();
#endif
               }
            }
         }

         if (message) {
            if (processMessage(message)) {
               actions++;
               message=0;
            }
//             else {
//                std::cout << "Failed to get DPM/FIFO for message: m_outStanding.size()="<<m_outStanding.size()<<std::endl;
//             }
         }
      }

      tstamp startTime;
      if (npolls==0) {
         ts_clock(&startTime);
      }

      if (m_outStanding.size()) {
         Message* outstandingMessage=m_outStanding.front();
         volatile unsigned int* tPtr=(unsigned int*) outstandingMessage->replyAddress();
         if (*tPtr!=0 && *tPtr!=1) {
            if (npolls>NBINS) npolls=NBINS;
            pollHist[npolls]++;
            //            std::cout << "Robin reply after " << npolls << " polls\n";
            npolls=0;
            actions++;
            dynamic_cast<DescriptorDataChannel*>(DataChannel::channel(outstandingMessage->rolId()))->gotFragment(outstandingMessage->descriptor());
            m_pending--;
            m_outStanding.pop();
            delete outstandingMessage;
         }
         else {
            tstamp currentTime;
            ts_clock(&currentTime);
            if (ts_duration(startTime, currentTime) > m_timeout) {
               std::cout << "Timeout: ";
//                dump();
//                exit(0);

               CREATE_ROS_EXCEPTION(tmoExcept, ROSRobinExceptions, TIMEOUT, "waiting for reply message (polled "<<npolls<<" times)");
               ers::warning(tmoExcept);

               m_pending--;
               m_outStanding.pop();
               delete outstandingMessage;
               npolls=0;
            }
            else {
               npolls++;
            }
         }
      }

      if (actions==0) {
         yield();
      }
   }
   std::cout << "RobinThread::run exiting\n";

   std::cout << "Poll hist: ";
   for (int bin=0; bin<NBINS+1; bin++) {
      std::cout << " " << pollHist[bin];
   }
   std::cout << std::endl;
   struct rusage resourcesEnd;
   status=getrusage(RUSAGE_SELF, &resourcesEnd);
   if (status) {
      perror("Error from getrusage");
   }
   struct timeval endTime;
   gettimeofday(&endTime,&dummy);
   float elapsedTime=(endTime.tv_sec-startTime.tv_sec) +
      (endTime.tv_usec-startTime.tv_usec)/1000000.0;
   float elapsedUserTime=(resourcesEnd.ru_utime.tv_sec-resourcesStart.ru_utime.tv_sec) +
      (resourcesEnd.ru_utime.tv_usec-resourcesStart.ru_utime.tv_usec)/1000000.0;
   float elapsedSystemTime=(resourcesEnd.ru_stime.tv_sec-resourcesStart.ru_stime.tv_sec) +
      (resourcesEnd.ru_stime.tv_usec-resourcesStart.ru_stime.tv_usec)/1000000.0;
   std::cout << "RobinThread::run()  Elapsed time "<< elapsedTime << "s  (" << elapsedUserTime
             << "s user, " << elapsedSystemTime << "s system). "
             << resourcesEnd.ru_nvcsw-resourcesStart.ru_nvcsw << " voluntary context switches ("
             << resourcesEnd.ru_nivcsw-resourcesStart.ru_nivcsw<< " involuntary).\n";
}

/***************************/
void RobinThread::cleanup() {
/***************************/
   std::cout << "RobinThread::cleanup\n";
}

/************************/
void RobinThread::stop() {
/************************/
   m_threadStarted=false;
   std::cout << "RobinThread::stop closing queue\n";
   m_descrMessageQueue->close();
   m_miscMessageQueue->close();
}

/****************************************************************/
bool RobinThread::receiveMsg(u_long replyAddress, bool blocking) {
/****************************************************************/
  volatile u_int *ptr;
  tstamp startTime, currentTime; 

  isOpen();
  //  std::cout << "RobinThread::receiveMsg: replyAddress = 0x" << HEX(replyAddress) << std::endl;
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::receiveMsg: blocking = " << blocking);
  
  ptr = reinterpret_cast<u_int *> (replyAddress);
  // Now wait for the fragment to arrive
  ts_clock(&startTime);
  //  std::cout << "RobinThread::receiveMsg: Entring into while loop,  *ptr=" << HEX(*ptr) << std::endl;
  int nYields=0;
  tstamp yieldTime=startTime;
  while ((*ptr == 0) || (*ptr == 1)) {
    if (!blocking) {
      DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::receiveMsg: The message has not yet arrived");
      return(false);
    }
    ts_clock(&currentTime);
    if (ts_duration(startTime, currentTime) > m_timeout) {
       std::cout << "Timeout: replyAddress 0x" << HEX(replyAddress)
                 << ", *reply 0x" << HEX(*ptr)
                 << ", start time " << startTime.high << " " << startTime.low
                 << ", current time " << currentTime.high << " " << currentTime.low
                 << ", time since start " << ts_duration(startTime, currentTime)
                 << "s, time since yield "  << ts_duration(yieldTime, currentTime)
                 << "s  (yielded "<<nYields<<" times)\n";

       //       dump();
       if ((*ptr != 0) && (*ptr != 1)) {
          std::cerr << "Timeout cancelled\n";
       }
       else {
          CREATE_ROS_EXCEPTION(ex19, ROSRobinExceptions, TIMEOUT, "waiting for reply message (yielded "<<nYields<<" times)");
          throw (ex19);
       }
    }
    else if (ts_duration(startTime, currentTime) < 0) {
       std::cerr << "-ve elapsed time: replyAddress " << replyAddress 
                 << ", start time " << startTime.high << " " << startTime.low
                 << ", current time " << currentTime.high << " " << currentTime.low
                 << ", time since start " << ts_duration(startTime, currentTime)
                 << "s, time since yield "  << ts_duration(yieldTime, currentTime)
                 << "s  (yielded "<<nYields<<" times)\n";
    }
    else {
       DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::receiveMsg: m_timeout = " << m_timeout);
       DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::receiveMsg: time elapsed (in seconds) = " << ts_duration(startTime, currentTime));
       DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::receiveMsg: calling yield");
       yieldTime=currentTime;
       DFThread::yield();
       nYields++;
    }
  }
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::receiveMsg: back from while loop");

   __sync_add_and_fetch(&m_doneCount,1);

  return(true);
}




/********************************/
bool RobinThread::checkPending() {
/********************************/
   if (m_nFifoSlots-m_pending == 0) {
      unsigned int doneNew=m_doneCount;
      if (doneNew>0) {
         m_pending-=doneNew;
         __sync_sub_and_fetch(&m_doneCount,doneNew);
      }
   }
   return (m_nFifoSlots-m_pending != 0);
}




/******************************************/
int RobinThread::getDPMandFIFO(u_int size) {
/******************************************/
  //Align the message size to 32 bytes
  if (size & 0x1f) {
    size = (size & ~0x1f) + 0x20;    
  }
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::getDPMandFIFO: Effective message size (bytes) = " << size);
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::getDPMandFIFO: msg_dpm.index                  = " << msg_dpm.index);
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::getDPMandFIFO: msg_dpm.user_size              = " << msg_dpm.user_size);

  if (size > msg_dpm.user_size) {
    DEBUG_TEXT(DFDB_ROSFM, 5, "RobinThread::getDPMandFIFO: size (" << size << " ) is too big for available DPM window ( " << msg_dpm.user_size << " )");
    CREATE_ROS_EXCEPTION(ex29, ROSRobinExceptions, ILLSIZE, "");
    throw (ex29);
  }


  int messageStart;
  u_int messageEnd=msg_dpm.index+size;
  if(messageEnd <= msg_dpm.user_size) {
     DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::getDPMandFIFO: new message fits into rest of window");
     messageStart = msg_dpm.index;
  }
  else {
     DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::getDPMandFIFO: new message has to start at top of window");
     messageStart = 0;
     messageEnd = size;
  }
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::getDPMandFIFO: messageEnd = " << messageEnd);


  if (!checkPending()) {
     DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::getDPMandFIFO: All FIFO slots in use");
     messageStart=-1;
  }
  else {

     if (m_pending-m_doneCount!=0) {
        int nSlots=m_pending-m_doneCount;
        int index=m_dpm_current_index-nSlots;
        if (index<0) {
           index+=m_nFifoSlots;
        }
#if 0
        if (messageStart==0) {
           std::cout << "message size=" << size
                     << "  current index=" << m_dpm_current_index
                     << "  oldest index=" << index
                     << "  done=" << m_doneCount
                     << "  pending=" << m_pending
                     <<std::endl;
        }
#endif
//        for (int slot=0; slot<nSlots; slot++) {
           if (m_dpm_start[index] < messageEnd) {
              messageStart=-1;
//              break;
//            }
//            index++;
//            index=index%m_nFifoSlots;
        }
     }
  }

  if (messageStart!=-1) {
     m_pending++;
     // __sync_add_and_fetch(&m_pending,1);

     DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::getDPMandFIFO: messageStart = " << messageStart);
     DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::getDPMandFIFO: m_pending = " << m_pending);

     //Register the DPM parameters
     msg_dpm.index = messageEnd;
     m_dpm_start[m_dpm_current_index] = messageStart;
     DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::getDPMandFIFO: m_dpm_start[" << m_dpm_current_index << "] = " << m_dpm_start[m_dpm_current_index]);
  
     if (m_dpm_current_index == (m_nFifoSlots - 1)) {
        m_dpm_current_index = 0;
     }
     else {
        m_dpm_current_index++;
     }
     DEBUG_TEXT(DFDB_ROSFM, 20, "RobinThread::getDPMandFIFO: new m_dpm_current_index = " << m_dpm_current_index);
  }

  return(messageStart);
}

