// -*- c++ -*-
#ifndef DFDATACOLLECTOR_H
#define DFDATACOLLECTOR_H

#include <vector>
#include <map>

#include "DataCollector.h"

template <class type> class ObjectQueue;

namespace ROS {
   class DFDescriptorTrigger;
   class RequestDescriptor;
   class DataRequestDescriptor;
   //! Data collector thread.

   //! Process request descriptors input via the data channels' ack queue
   //! pass completed data requests to the sending thread. 
   class DFDataCollector : public DataCollector {
   public:
      //! Constructor is given a pointer to the queue used to return
      //! completed RequestDescriptors to the Trigger thread for re-use. 
      DFDataCollector(DFDescriptorTrigger* trigger,
                      ObjectQueue<RequestDescriptor>* collectionQueue,
                      ObjectQueue<RequestDescriptor>* returnQueue);
      ~DFDataCollector();

      //! The action thread of the DFDataCollector
      void run();

      //void cleanup();


      void start();

      //! Stop the DFDataCollector thread
      void stop();

      //! For debugging, write a textual description of the state of the
      //! DFDataCollector to the cout stream.
      void dump();

      uint64_t timeOuts();
   private:
      DFDescriptorTrigger* m_trigger;

      uint64_t m_requestsTimedOut;

      //! Input queues for
      //! RequestDescriptors that have been handled
      ObjectQueue<RequestDescriptor>*  m_collectionQueue;
   };
}

#endif
