// -*- c++ -*-
/*******************************************************/
/*                                                     */
/* File Robinthread.h                                        */


#ifndef ROBINTHREAD_ROBIN_H
#define ROBINTHREAD_ROBIN_H

#include <queue>

#include <sys/types.h>
#include "DFThreads/DFFastMutex.h"
#include "DFThreads/DFThread.h"
#include "ROSRobin/robindriver_common.h"

#include "ROSDescriptor/ObjectQueue.h"
#include "ROSRobin/Robin.h"
//#include "RolThread.h"

namespace ROS 
{
   class Rol;
   class RequestDescriptor;
   class RolThread;


   //! Thread version of Robin class

   //!  Here we have one thread of our own which gets passed all the
   //! messages for one robin via message queues. Responses from the
   //! robin are expected to come in the same order that we send the
   //! messages so we only need to poll for a response to the oldest
   //! outstanding message
   class RobinThread : public DFThread, public Robin  {
   public:
      /*************************************************************************/
      /*          Construction / Destruction                                   */
      /*************************************************************************/

      RobinThread(u_int slotNumber, float timeout, int queueSize=50);

      ~RobinThread();


      /*************************************************************************/
      /*                         Message passing                               */
      /*************************************************************************/

      //! Send a message with no parameters
      void sendMsg(u_int rolId, u_int service, u_long replyAddress);

      //! Send a message with only one parameter
      void sendMsg(u_int rolId, u_int service, u_long replyAddress, u_int parameter);

      //! Send a message with two parameters
      void sendMsg(u_int rolId, u_int service, u_long replyAddress, u_int parameter1, u_int parameter2);

      //! Send a message with a whole data block as a parameter

      //! This is the only one of the sendMsg methods that is
      //!different from the one in the Robin base class but
      //!unfortunately if we override one signature we have to override
      //!them all.
      //!
      //! This method will call queueMsg to add the message
      //!to the queue which will be handled by our robin specific
      //!thread instead of talking directly to the robin in the context
      //!of the caller's thread.
      void sendMsg(u_int rolId, u_int service, u_long replyAddress, u_int size, u_int *data);

      //! Poll for a reply message
      bool receiveMsg(u_long replyAddress, bool blocking);

      //! Queue a message to be dequeued and sent to the robin by our own thread
      void queueMsg(u_int rolIndex, u_int service, u_long replyAddress, u_int size, u_int *data,
                    u_int rolId=0xffffffff,
                    RequestDescriptor* descriptor=0);

      //! Dump the internal state of the RobinThread to standard output for debugging purposes
      void dump();

      //! The thread action routine that dequeues messages to send to
      //!the robin and polls for replies
      void run();
      void cleanup(); 

      //! Method to stop the thread and close the associated queues
      void stop();

      //! Get the RolThread associated with the given rolId
      RolThread& getRolThread(u_int rolId);
   protected:
      //! Virtual method to instantiate a new Rol. In this
      //!implementation we will instantiate a RolThread object rather
      //!than an object of the Rol base class
      virtual Rol* newRol(u_int number);
   private:
      int m_doneCount;


      //! A message to be sent to the robin
      class Message {
      public:
        Message(u_int rolIndex, u_int service, u_long replyAddress, u_int size, u_int *data,
                u_int rolId, RequestDescriptor* descriptor);
        ~Message();
        u_int rolIndex();
        u_int rolId();
        u_int service();
        u_long replyAddress();
        u_int size();
        u_int *data();
        RequestDescriptor* descriptor();
        void dump() const;

        int m_msgOffset;
        static unsigned int s_serial;
        unsigned int m_serialNumber;
     private:
        u_int m_rolIndex;
        u_int m_service;
        u_long m_replyAddress;
        u_int m_size;
        u_int* m_data;
        u_int m_rolId;
        RequestDescriptor* m_requestDescriptor;
     };
      ObjectQueue<Message>* m_descrMessageQueue;  //!< Queue of messages coming from the DescriptorTrigger thread
      ObjectQueue<Message>* m_miscMessageQueue;   //!< Queue of messages coming from other sources
      std::queue<Message*> m_outStanding;
      bool m_threadStarted;

      /*******************/
      /* Private Methods */
      /*******************/
      bool checkPending();
      bool processMessage(Message*);
      int getDPMandFIFO(u_int size);
  };
} 
#endif

