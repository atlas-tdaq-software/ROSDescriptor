// -*- c++ -*-

/************************************************************************/
/*  ATLAS ROS Software							*/
/*									*/
/*  Class: RobinDescriptorChannel						*/
/*  Author: Markus Joos, PH/ESS						*/	
/**** C 2005 - Ecosoft: Made from at least 70% recycled source code *****/

#ifndef ROBINDATACHANNEL_H
#define ROBINDATACHANNEL_H

#include <vector>
#include <sstream>

#include "RolThread.h"

#include "rcc_time_stamp/tstamp.h"
#include "ROSEventFragment/EventFragment.h"

#include "ROSDescriptor/DescriptorDataChannel.h"

#include "DFThreads/DFFastMutex.h"
#include "ROSInfo/RobinDataChannelInfo.h"

namespace ROS 
{
   class RequestDescriptor;
   class ClearRequestDescriptor;
   class WrapperMemoryPool;
   class MemoryPage;
   //! Robin data channel

   //! Data channel for accesing robin using the descriptor passing
   //! threading model.
   //!  This class is mainly a copy of the non-descriptor version. Some
   //! methods are effectively obsolete.
  class RobinDescriptorChannel : public DescriptorDataChannel
  {
  public:
     RobinDescriptorChannel(u_int id, u_int configId,
                      u_int physicalAddress,
                      RolThread& rol,
                      u_int RobinphysicalAddress,
                      WrapperMemoryPool *mpool,
                      u_int crcInterval);
    ~RobinDescriptorChannel();
    ROS::EventFragment *getFragment(int ticket);
    void releaseFragment(const std::vector < u_int > * level1Ids);
    void releaseFragmentAll(const std::vector < u_int > * level1Ids);
    void releaseFragment(int level1Id);
    //u_long requestFragment(int level1Id);
    int requestFragment(int level1Id);
    int collectGarbage(u_int oldestId);
    void setExtraParameters(int value);
    virtual ISInfo*  getISInfo(void);
    void clearInfo(void);
    void stopFE(void);
    void prepareForRun(void);
    void probe(void);
    void enable(void);
    void disable(void);

     void descriptorRequestFragment(DataRequestDescriptor* descriptor);
     void descriptorReleaseFragment(ClearRequestDescriptor*);
     void gotFragment(RequestDescriptor*);
  private: 
     //    ROS::WrapperMemoryPool *m_memoryPool;
    ROS::RolThread& m_rol;

    DFFastMutex *m_requestMutex;            // The request mutex necessary to access the memory pool
    unsigned long long int m_gcdeleted;
    unsigned long long int m_gcfree;
    u_int m_id;
    u_int m_l1id_modulo;
    u_int m_RobinphysicalAddress;
     u_int m_crcInterval;
     u_int m_fragCount;
    u_int m_infoflag;
    tstamp m_tsStopLast;         // time stamp for level1 rate calculation
    u_int64_t m_numberOfLevel1Last;
    RobinDataChannelInfo m_statistics;


    u_int m_rdw1;
    u_int m_rdw2;
    u_int m_rdw3;
    u_int m_rdw4;
    u_int m_hdw1;
    u_int m_hdw2;
    u_int m_hdw3;
    u_int m_hdw4;


    enum Robin_Messages 
    {
      NOMESSAGE = 0,
      REQUESTFRAGMENT,
      RELEASEFRAGMENT,
      COLLECTGARBAGE
    };
  };
}
#endif
