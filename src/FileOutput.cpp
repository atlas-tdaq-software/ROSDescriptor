// $Id: FileOutput.cpp 92996 2010-07-08 14:06:24Z gcrone $
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone, University College London
//
//  $Log$
//  Revision 1.14  2009/02/18 17:46:28  gcrone
//  Update for new RawFileName API
//
//  Revision 1.13  2008/11/21 14:22:34  gcrone
//  Use stream type from database instead of hardcoded "calibration"
//
//  Revision 1.12  2008/11/18 17:54:19  gcrone
//  Use new convention for RawFileName
//
//  Revision 1.11  2008/11/13 15:14:51  gcrone
//  Use separate counter for number of calls to sendData and the number of fragments actually output
//
//  Revision 1.10  2008/11/13 14:42:52  gcrone
//  Move counter of fragmentsOutput to inside the if clause that writes
//  the data so we don't count events that we skip over.
//
//  Revision 1.9  2008/11/06 15:38:22  gcrone
//  Use RawFileName instead of AgreedFileName
//
//  Revision 1.8  2008/04/11 09:28:38  gcrone
//  Only use recording flag from runParams if we're the main DataOut.
//
//  Revision 1.7  2008/04/09 13:47:25  gcrone
//  Take type argument in constructor/create function
//
//  Revision 1.6  2008/03/06 15:50:33  gcrone
//  Try reading fileTag from our own config rather than RunParams.
//  Comment out unused arguments in callback functions to avoid compiler
//  warnings.
//
//  Revision 1.5  2008/02/28 18:17:24  gcrone
//  Update for new DataStorageCallback interface in tdaqCommon-01.09
//
//  Revision 1.4  2008/02/27 16:03:11  gcrone
//  Restore check on max events as missing RunController header file has
//  been fixed.
//
//  Revision 1.3  2008/02/27 15:09:43  gcrone
//  Disable check on max events for now as there isn't a MAX_EVT_DONE
//  issue in the new RunController.
//
//  Revision 1.2  2008/02/25 16:57:17  gcrone
//  Include classname in DEBUG_TEXT
//
//  Revision 1.1  2008/02/19 13:58:08  gcrone
//  Split file writing and IO emulation DataOuts
//
//
// //////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/uio.h>

#include "FileOutput.h"
#include "ROSDescriptor/DataRequestDescriptor.h"

#include "DFDebug/DFDebug.h"
#include "DFDebug/GlobalDebugSettings.h"
#include "DFSubSystemItem/Config.h"


#include "ROSIO/IOException.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSUtilities/ROSErrorReporting.h"


#include "EventStorage/DataWriter.h"
#include "EventStorage/EventStorageRecords.h"
#include "EventStorage/RawFileName.h"


#include "RunControl/Common/UserExceptions.h"
#include "rc/StorageInfoNamed.h"

#include "SFOTZ/SFOTZThread.h"
#include "SFOTZ/SFOTZDataWriterCallback.h"

#include "eformat/DetectorMask.h"

using EventStorage::DataWriter;
using std::ostringstream;
using namespace ROS;

FileOutput::StorageCallback::StorageCallback(std::string partitionName, std::string appName, bool interactiveMode) :
   m_interactiveMode(interactiveMode) {
   if (!m_interactiveMode) {
      char buf[HOST_NAME_MAX];
      (gethostname(buf, HOST_NAME_MAX)< 0) ? m_hostName="UnknownHost": m_hostName=buf;
      ostringstream myName;
      myName  << "RunParams.StorageInfo." << appName ;
      m_isName=myName.str();
      m_partition=partitionName;
   }
}

FileOutput::StorageCallback::~StorageCallback() {
   //   ISInfoDictionary dictionary(m_partition);
   //   dictionary.remove(m_isName);
}
void FileOutput::StorageCallback::FileWasOpened(std::string /*logicalfileName*/,
                                                 std::string fileName,
                                                 std::string /*streamtype*/,
                                                 std::string /*streamname*/,
                                                 std::string /*sfoid*/,
                                                 std::string /*guid*/,
                                                 unsigned int runNumber, 
                                                 unsigned int /*filenumber*/,
                                                 unsigned int /*lumiblock*/)
{
   if (m_interactiveMode) {
      std::cout << "Storage opened new file - " << fileName << std::endl;
   }
   else {
      StorageInfoNamed ISstorage(m_partition, m_isName.c_str());
      ISstorage.runNumber = runNumber;
      ISstorage.host = m_hostName;
      ISstorage.fileName = fileName;
      ISstorage.status = "opened";
      ISstorage.numberOfEvents = 0;
      ISstorage.checkin(); 
   }
}

void FileOutput::StorageCallback::FileWasClosed(std::string /*logicalfileName*/,
                                                 std::string fileName,
                                                 std::string /*streamtype*/,
                                                 std::string /*streamname*/,
                                                 std::string /*sfoid*/,
                                                 std::string /*guid*/,
                                                 std::string /*checksum*/,
                                                 unsigned int eventsInFile, 
                                                 unsigned int runNumber, 
                                                 unsigned int /*filenumber*/,
                                                 unsigned int /*lumiblock*/,
                                                 uint64_t /*filesize*/)
{
   if (m_interactiveMode) {
      std::cout << "Storage closed " << fileName << " with " << eventsInFile << " events\n";
   }
   else {
      StorageInfoNamed ISstorage(m_partition, m_isName.c_str());
      ISstorage.runNumber = runNumber;
      ISstorage.host = m_hostName;
      ISstorage.fileName = fileName;
      ISstorage.status = "closed";
      ISstorage.numberOfEvents = eventsInFile;
      ISstorage.checkin();
   }
}


// ************************************************************************
FileOutput::FileOutput(const DataOut::Type type) : DescriptorDataOut(type){
// ************************************************************************
   DEBUG_TEXT(DFDB_ROSIO, 15, "FileOutput::constructor:  called");

   m_mutex=DFFastMutex::Create();
   m_statistics.fragmentsOutput=0;
   m_statistics.dataOutput=0.0;
   m_useEventStorage = false;
   m_dataWriter = 0;
   m_outputFile=0;
   m_dbthread=0;
   m_dbCallBack=0;
}


// ************************************************************************
FileOutput::~FileOutput() noexcept {
// ************************************************************************
   DEBUG_TEXT(DFDB_ROSIO, 15, "FileOutput::destructor:  called");
   if (m_dataWriter != 0) {
      delete m_dataWriter;
      delete m_storageCallback;
      if (m_dbCallBack != 0) {
         delete m_dbCallBack;
      }
      m_dataWriter = 0;
   }
   else if (m_outputFile != 0){
      close (m_outputFile);
      m_outputFile=0;
   }

   m_mutex->destroy();
   DEBUG_TEXT(DFDB_ROSIO, 15, "FileOutput::destructor:  finished");
}


// ************************************************************************
void FileOutput::sendData(const Buffer* buffer, NodeID /*destination*/,
                               unsigned int /*transactionId*/,
                               unsigned int /*status*/){
// ************************************************************************
   DEBUG_TEXT(DFDB_ROSIO, 20,  "FileOutput::sendData  \"Sending\" buffer ");


  m_mutex->lock();
  if (m_recording && m_samplingGap && m_totalInput%m_samplingGap == 0) {
    int oldState;
    int ecode=pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &oldState);
    if (ecode !=0) {
       std::cout << "Error code " << ecode << " returned from pthread_setcancelstate\n";
    }
    struct iovec ioVec[Buffer::MAX_NUMBER_OF_PAGES];
    int pageNumber=0;
    for (Buffer::page_iterator page=buffer->begin();
	page!=buffer->end(); page++) {
      if (m_useEventStorage) {
	ioVec[pageNumber].iov_len= (*page)->usedSize();
	ioVec[pageNumber].iov_base=(*page)->address();
	pageNumber++;
      }
      else {
	write(m_outputFile, (*page)->address(), (*page)->usedSize());
      }
      m_statistics.dataOutput+=(*page)->usedSize()/(float)(1024*1024);
    }
    if (m_useEventStorage && (m_dataWriter != 0)) {
       if (m_dataWriter->good()) {
          EventStorage::DWError status=m_dataWriter->putData(pageNumber, ioVec);
          if (status != EventStorage::DWOK) {
             ecode=pthread_setcancelstate(oldState, 0);
             if (ecode !=0) {
                std::cout << "Error code " << ecode << " returned from pthread_setcancelstate\n";
             }
             m_mutex->unlock();
             CREATE_ROS_EXCEPTION(tException, IOException, DATAWRITER_SEND_ERROR,
                                  "");
             throw tException;
          } 
       }
    }
    ecode=pthread_setcancelstate(oldState, 0);
    if (ecode !=0) {
       std::cout << "Error code " << ecode << " returned from pthread_setcancelstate\n";
    }

    m_statistics.fragmentsOutput++;
  }
  m_totalInput++;

  if ( (m_maxEvents > 0) && (m_totalInput == m_maxEvents) ) {
     daq::rc::MAX_EVT_DONE issue(ERS_HERE,"reached max events");
     ers::warning(issue);
  }

  m_mutex->unlock();
}


// ************************************************************************
void FileOutput::send(DataRequestDescriptor* descriptor) {
// ************************************************************************
  if (m_recording && m_samplingGap && m_totalInput%m_samplingGap == 0) {
    int oldState;
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &oldState);

    if (m_useEventStorage && (m_dataWriter != 0)) {
       if (m_dataWriter->good()) {

          unsigned int nPages=descriptor->channelCount()+1;
          struct iovec* iov=new struct iovec[nPages+1];
          descriptor->fragment(iov, true);
          EventStorage::DWError status=m_dataWriter->putData(nPages, iov);
          delete [] iov;
          if (status != EventStorage::DWOK) {
             pthread_setcancelstate(oldState, 0);
             CREATE_ROS_EXCEPTION(tException, IOException, DATAWRITER_SEND_ERROR,
                                  "");
             throw tException;
          } 
       }
    }
    pthread_setcancelstate(oldState, 0);
    m_statistics.fragmentsOutput++;
  }
  m_totalInput++;

  if ( (m_maxEvents > 0) && (m_totalInput == m_maxEvents) ) {
     daq::rc::MAX_EVT_DONE issue(ERS_HERE,"reached max events");
     ers::warning(issue);
  }
}


// ************************************************************************
void FileOutput::userCommand(std::string& s1,std::string& s2) {
// ************************************************************************
   if (s1 == "samplingGap") {
      int gap=atol(s2.c_str());
      std::cout << "Changing sampling gap from " << m_samplingGap << " to " << gap << std::endl;
      m_samplingGap=gap;
   }
}

// ************************************************************************
void FileOutput::prepareForRun(const daq::rc::TransitionCmd&){
// ************************************************************************
   DEBUG_TEXT(DFDB_ROSIO, 15, "FileOutput::prepareForRun() entered");
      std::cout <<"run params=====>\n"; m_runParams->dump(); std::cout << "<======\n";
   m_runNumber = m_runParams->getInt ("runNumber");
   m_maxEvents = m_runParams->getInt ("maxNumberOfEvents");
   if (m_type==MAIN) {
      m_recording = m_runParams->getBool ("recording");
   }
   else {
      m_recording = true;
   }
   if (m_recording && m_samplingGap) {
      DEBUG_TEXT(DFDB_ROSIO, 15, "FileOutput::  recording enabled");

      // Use file tag from our config unless it's blank
      m_fileTag=m_configuration->getString("FileTag");
      if (m_fileTag=="") {
         m_fileTag=m_runParams->getString ("fileTag");
      }
      m_streamType=m_configuration->getString("StreamType");
      unsigned int lumiBlockNumber=0;

      std::string t0Tag=m_runParams->getString("T0ProjectTag");

      daq::RawFileName fileNameObj(t0Tag,
                                   m_runNumber,m_streamType,
                                   m_fileTag,lumiBlockNumber,m_appName);
      std::string fileName= fileNameObj.fileNameCore(); 

      if (m_useEventStorage) {
         EventStorage::run_parameters_record runParamRec;
         runParamRec.marker=RUN_PARAMETERS_MARKER;
         runParamRec.record_size=sizeof(runParamRec);
         runParamRec.run_number=m_runNumber;
         runParamRec.max_events=m_maxEvents;
         runParamRec.rec_enable=1;
         runParamRec.trigger_type=m_runParams->getUInt("triggerType");


         eformat::helper::DetectorMask mask(m_runParams->getString("detectorMask"));
         runParamRec.detector_mask_LS = mask.serialize().second;
         runParamRec.detector_mask_MS = mask.serialize().first;

         runParamRec.beam_type=m_runParams->getUInt("beamType");
         runParamRec.beam_energy=m_runParams->getInt("beamEnergy");


         std::vector<std::string> userMetaData;
         if (m_runParams->getBool("UserDataPresent")) {
            userMetaData=
               m_runParams->getVector<std::string>("UserMetaData");
         }
         m_dataWriter=new DataWriter(m_writingPath,
                                     fileName,
                                     runParamRec,
                                     userMetaData);
         bool status=m_dataWriter->good();
         if (!status) {
            CREATE_ROS_EXCEPTION(ex, IOException, DATAWRITER_OPEN_ERROR, "");
            throw(ex);
         }
         else {
            m_dataWriter->setMaxFileNE(m_maxEventsPerFile);
            m_dataWriter->setMaxFileMB(m_maxMegaBytesPerFile);
	    // instantiate callback function and register it
	    m_storageCallback = new StorageCallback(m_configuration->getString("partition"),
                                                    m_appName,
                                                    m_configuration->getBool("interactive"));
	    m_dataWriter->registerCallBack(m_storageCallback);

         }


         m_configuration->dump();
         if (m_configuration->isKey("fileTable")) {
            DEBUG_TEXT(DFDB_ROSIO, 10, "FileOutput::prepareForRun(): Using SFOTZ db");
            // OK, we need to set up database access for LAr calibration
            std::string connString=m_configuration->getString("connectionString");
            std::string username=m_configuration->getString("username");
            std::string password=m_configuration->getString("password");
            std::string indexTable=m_configuration->getString("indexTable");
            std::string runTable=m_configuration->getString("runTable");
            std::string lbTable=m_configuration->getString("lbTable");
            std::string fileTable=m_configuration->getString("fileTable");
            m_dbthread = new SFOTZ::SFOTZThread(connString,
                                                username,
                                                password,
                                                indexTable,
                                                runTable,
                                                lbTable,
                                                fileTable);
            m_dbCallBack=new SFOTZ::SFOTZDataWriterCallback(m_appName, m_dbthread);
            m_dbthread->start();
            m_dataWriter->registerCallBack(m_dbCallBack);
            DEBUG_TEXT(DFDB_ROSIO, 10, "FileOutput::prepareForRun(): db callback registered");

            SFOTZ::DBInfoPointer info(new SFOTZ::DBUpdateInfo());
            info->command = SFOTZ::DBUpdateInfo::RUNOPENED;
            info->runnr = m_runNumber;
            info->sfo = m_appName;
            info->streamtype = m_streamType;
            info->stream = m_fileTag;
            info->projecttag = t0Tag;
            info->maxnrsfos = 0;

            m_dbthread->add(info);

            info.reset(new SFOTZ::DBUpdateInfo());
            info->command = SFOTZ::DBUpdateInfo::LBOPENED;
            info->runnr = m_runNumber;
            info->sfo = m_appName;
            info->lbnr = lumiBlockNumber;
            info->streamtype = m_streamType;
            info->stream = m_fileTag;
            info->maxnrsfos = 0;

            m_dbthread->add(info);
         }

      }
      else {
         std::string outputFileName;
         if (m_writingPath.size() == 0) {
            outputFileName="/dev/null";
         }
         else {
            ostringstream runNumberStream;
            runNumberStream << m_runNumber;
            outputFileName=m_writingPath + "/" + m_appName + "_" + m_fileTag + "_" + runNumberStream.str() + ".dat";
         }
         DEBUG_TEXT(DFDB_ROSIO, 15,
                    "FileOutput::connecting to output File"
                    << outputFileName);

         m_outputFile=open(outputFileName.c_str(),
                           O_WRONLY|O_CREAT,
                           S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
      }
   }

   m_statistics.fragmentsOutput=0;
   m_statistics.dataOutput=0.0;
   m_totalInput=0;
}


// ************************************************************************
void FileOutput::stopGathering(const daq::rc::TransitionCmd&){
// ************************************************************************
   DEBUG_TEXT(DFDB_ROSIO, 10, "FileOutput::stopGathering entered");
   m_mutex->lock();
   if (m_dataWriter != 0) {
      delete m_dataWriter;
      delete m_storageCallback;
      if (m_dbCallBack != 0) {
         delete m_dbCallBack;
      }
      m_dataWriter = 0;
   }
   else if (m_outputFile != 0){
      close (m_outputFile);
      m_outputFile=0;
   }

   if (m_dbthread!=0) {
      SFOTZ::DBInfoPointer info(new SFOTZ::DBUpdateInfo());

      info->command = SFOTZ::DBUpdateInfo::LBCLOSED;
      info->runnr = m_runNumber;
      info->sfo = m_appName;
      info->lbnr = 0;
      m_dbthread->add(info);

      info.reset(new SFOTZ::DBUpdateInfo());
      info->command = SFOTZ::DBUpdateInfo::CHECKLBTRANS;
      info->runnr = m_runNumber;
      info->sfo = m_appName;
      info->lbnr = 0;
      info->streamtype = m_streamType;
      info->stream = m_fileTag;
      m_dbthread->add(info);

      info.reset(new SFOTZ::DBUpdateInfo());
      info->command = SFOTZ::DBUpdateInfo::RUNCLOSED;
      info->runnr = m_runNumber;
      info->sfo = m_appName;
      m_dbthread->add(info);

      info.reset(new SFOTZ::DBUpdateInfo());
      info->command = SFOTZ::DBUpdateInfo::CHECKRUNTRANS;
      info->runnr = m_runNumber;
      info->sfo = m_appName;
      info->streamtype = m_streamType;
      info->stream = m_fileTag;
      m_dbthread->add(info);


      DEBUG_TEXT(DFDB_ROSIO, 10, "Added end of run items to db thread. Db thread contains "
                 << m_dbthread->size() << " items");

      unsigned int actionTimeout_ms=4000;
      auto timeInit = std::chrono::system_clock::now();
      bool timedout = false;
      while(!m_dbthread->empty() && !timedout){
         sched_yield();
         auto elapsed=std::chrono::system_clock::now()-timeInit;
         uint32_t msec=std::chrono::duration_cast<std::chrono::milliseconds> (elapsed).count();
         timedout = (msec > actionTimeout_ms);
      }

      if(!m_dbthread->empty()){
         CREATE_ROS_EXCEPTION(exc, IOException, IOException::TIMEOUT,
                              "DB client still contains " << m_dbthread->size() << " entries.");
         ers::error(exc);
      }

      m_dbthread->stop();

      delete m_dbthread;
      m_dbthread=0;
   }

   m_mutex->unlock();
   DEBUG_TEXT(DFDB_ROSIO, 10, "FileOutput::stopGathering finished");
}


// ************************************************************************
void FileOutput::setup(DFCountedPointer<Config> configuration){
// ************************************************************************
   DEBUG_TEXT(DFDB_ROSIO, 15, "FileOutput::setup() entered");

   m_configuration=configuration;

   m_appName = m_configuration->getString("appName");

   m_writingPath=configuration->getString("writingPath");
   m_maxEventsPerFile=configuration->getInt("maxEventsPerFile");
   m_maxMegaBytesPerFile=configuration->getInt("maxMegaBytesPerFile");

   m_samplingGap=configuration->getInt("SamplingGap");

   m_useEventStorage=configuration->getBool("FormattedOutput");

   std::cout << "FileOutput::"
        << " writing data to file(s) in " << m_writingPath
        << " every " << m_samplingGap << " calls to sendData"
        << std::endl;
}

// ************************************************************************
ISInfo* FileOutput::getISInfo(){
// ************************************************************************
   return &m_statistics;
}


extern "C" {
   extern DataOut* createFileOutput(const DataOut::Type* myType);
}
DataOut* createFileOutput(const DataOut::Type* myType)
{
   return (new FileOutput(*myType));
}

