// $Id: RobinDataDrivenTrigger.cpp 45472 2008-02-14 13:48:19Z gcrone $
// //////////////////////////////////////////////////////////////////////
//   data driven trigger for the Robin based ROS
//
//   The trigger reads pairs of (first,last) L1ID from a queue connected to the
//   DDT UserAction that polls the Robin to retrieve the L1ID info from the Robin(s)
//   It then calls the trigger generator to generate requests for this 'bunch' of L1IDs
//
// Authors: G.Crone, M.Joos & J.Petersen ATLAS/TDAQ
//
// //////////////////////////////////////////////////////////////////////

#include "RobinDataDrivenTrigger.h"


#include "DFDebug/DFDebug.h"

using namespace ROS;

// will call the constructor of the EmulatedTrigger
/************************************/
RobinDataDrivenTrigger::RobinDataDrivenTrigger() 
/************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "RobinDataDrivenTrigger::constructor:  entered");
}

// will "call" the destructor of the EmulatedTrigger
/************************************/
RobinDataDrivenTrigger::~RobinDataDrivenTrigger() noexcept
/************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "RobinDataDrivenTrigger::destructor:  entered");
}


/*********************************/
void RobinDataDrivenTrigger::configure(const daq::rc::TransitionCmd& cmd)
/*********************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "RobinDataDrivenTrigger::configure:  entered");
  m_inputToTriggerQueue = TriggerInputQueue::instance();
  std::cout << " RobinDataDrivenTrigger: connect, Q pointer = " << m_inputToTriggerQueue
            << std::endl;

  EmulatedTrigger::configure(cmd);

}


/***********************************/
void RobinDataDrivenTrigger::unconfigure(const daq::rc::TransitionCmd& cmd)
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "RobinDataDrivenTrigger::unconfigure:  entered");

  EmulatedTrigger::unconfigure(cmd);

  m_inputToTriggerQueue->destroy();   // detach from Q
}


/***************************/
void RobinDataDrivenTrigger::run()
/***************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "RobinDataDrivenTrigger::run:  entered");

  while (true) {
    DEBUG_TEXT(DFDB_ROSTG, 20, "RobinDataDrivenTrigger::run: popping trigger Q");
    unsigned int first_level1Id = m_inputToTriggerQueue->pop();
    unsigned int last_level1Id = m_inputToTriggerQueue->pop();
    DEBUG_TEXT(DFDB_ROSTG, 10, "RobinDataDrivenTrigger::run: first L1ID = 0x" << std::hex << first_level1Id
                               << "  last L1ID = 0x" << last_level1Id << " popped from trigger Q");

    m_generator->generateL1idBunch(first_level1Id, last_level1Id) ;
  }
}


/** Shared library entry point */
extern "C" 
{
   extern TriggerIn* createRobinDataDrivenTrigger();
}
TriggerIn* createRobinDataDrivenTrigger()
{
   return (new RobinDataDrivenTrigger());
}
