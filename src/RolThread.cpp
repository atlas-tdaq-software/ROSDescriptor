// $Id:$
//

#include <iostream>

#include "RobinThread.h"
#include "RolThread.h"

#include "DFDebug/DFDebug.h"

#include "ROSDescriptor/DataRequestDescriptor.h"
#include "ROSDescriptor/ClearRequestDescriptor.h"

using namespace ROS;

/*****************************************************************************/
RolThread::RolThread(RobinThread* robinBoard, u_int rolId)
   : Rol(*robinBoard,rolId), m_robinThread(robinBoard) {
/*****************************************************************************/
   DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"RolThread::constructor: Called for rolId"<<rolId);
}


/**********************/
RolThread::~RolThread() {
/**********************/
  DEBUG_TEXT(DFDB_ROSROBIN, 15 ,"RolThread::destructor: Called");
}

/******************************************************************************/
void RolThread::requestFragment(DataRequestDescriptor* descriptor, u_int rolId) {
/******************************************************************************/
   u_int eventId=descriptor->level1Id();

   //   std::cout << "RolThread::requestFragment - l1Id=" << eventId << ", m_rolId=" << m_rolId << ", rolId=" << rolId << std::endl;
   m_robinThread->queueMsg(m_rolId,
                          enum_reqFragment,
                          reinterpret_cast<u_long>(descriptor->dataPage(rolId)->virtualAddress()),
                          1,
                          &eventId,
                          rolId,
                          descriptor);
}

/******************************************************************************/
void RolThread::releaseFragment(ClearRequestDescriptor* descriptor, u_int rolId) {
/*******************************************************************************/
   m_robinThread->queueMsg(m_rolId,
                          enum_reqClearAck,
                          reinterpret_cast<u_long>(descriptor->dataPage(rolId)->virtualAddress()),
                          descriptor->nLevel1Ids()+1,
                          descriptor->level1Ids(),
                          rolId,
                          descriptor);
}


