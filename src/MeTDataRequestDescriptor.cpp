#include "MeTDataRequestDescriptor.h"
#include "ROSIO/MEtException.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"

using namespace ROS;

unsigned int MeTDataRequestDescriptor::s_nextId=0;


MeTDataRequestDescriptor::MeTDataRequestDescriptor(unsigned int timeout) : DataRequestDescriptor(timeout) {
   m_descriptorId=s_nextId++;

   unsigned int bufferSize=c_maxMetBlockSize*m_allChannelList.size()+c_metHeaderSize;
   m_metBuffer=new unsigned int[bufferSize];
   m_robHeader=(ROBFragment::ROBHeader*) m_metBuffer;

   m_robHeader->generic.startOfHeaderMarker=EventFragment::s_robMarker;
   m_robHeader->generic.headerSize=(sizeof(ROBFragment::ROBHeader)/sizeof(u_int))-ROBFragment::s_nStatusElements+c_nStatusElements;


   m_robHeader->generic.formatVersionNumber=EventFragment::s_formatVersionNumber;
   m_robHeader->generic.numberOfStatusElements=c_nStatusElements;
   m_fragStatus=&m_robHeader->statusElement[0];

   m_robHeader->statusElement[c_nStatusElements]=0;  // Yes this is the word AFTER the status elements = checksum type

   m_rodHeader=(RODFragment::RODHeader*) &m_metBuffer[m_robHeader->generic.headerSize];
   m_rodHeader->startOfHeaderMarker=EventFragment::s_rodMarker;
   m_rodHeader->headerSize=sizeof(RODFragment::RODHeader) / sizeof (u_int);

   DEBUG_TEXT(DFDB_ROSIO, 20, "MeTDataRequestDescriptor " << m_descriptorId << " constructed");
}


MeTDataRequestDescriptor::~MeTDataRequestDescriptor() {
   delete [] m_metBuffer;

}


// Not really channel count but number of data blocks we have to send
unsigned int MeTDataRequestDescriptor::channelCount() {
   unsigned int nPages=m_channelList.size();
   if (nPages==0 && !m_metRequested) {
      nPages=m_allChannelList.size();
   }
   else if (m_metRequested) {
      nPages++;
   }
   return nPages;
}


// ************************************************************************
void MeTDataRequestDescriptor::initialise(unsigned int level1Id,
                                          const std::vector<unsigned int>* myChannels,
                                          unsigned int transactionId,
                                          unsigned int sequence, unsigned int tag) {
// ************************************************************************

   DataRequestDescriptor::initialise(level1Id, 0, transactionId,
                                     sequence,  tag);

   m_metRequested=false;
   if (myChannels!=0) {
      for (auto chanIter : *myChannels) {
         if ((chanIter&0x00f00000)!=0x00700000) {
            m_channelList.push_back(chanIter);
         }
         else {
            m_metRequested=true;
            m_sourceId=chanIter;
         }
      }
   }
   if (m_metRequested) {
      m_robHeader->generic.sourceIdentifier=m_sourceId;
      m_robHeader->generic.totalFragmentsize=m_robHeader->generic.headerSize+m_rodHeader->headerSize;
      m_writePtr=&m_metBuffer[m_robHeader->generic.totalFragmentsize];

      m_robCount=m_writePtr++;   // Initialise upper bits of robCount with words/rob
      if (m_sourceId>>16==0x007d) {
         *m_robCount=10<<16;     // 10 for LAr
      }
      else {
         *m_robCount=16<<16;     // 16 for Tile
      }
      m_robHeader->generic.totalFragmentsize++;

      m_firstAppend=true;
      m_needHeader=true;
   }
   else {
      if (m_channelList.size()!=0) {
         m_nChannels=m_channelList.size();
      }
   }
}






bool MeTDataRequestDescriptor::processFeb(unsigned int* dataPtr) {
   unsigned int offset=dataPtr[3]>>16;
   //   std::cout << "offset=" << offset << std::endl;
   if (offset!=0) {
      unsigned int nSamples=dataPtr[7]&0xffff;
      //      std::cout << "number of samples=" << nSamples << std::endl;
      if (nSamples==5) {
         offset+=75;
      }
      else if (nSamples==7) {
         offset+=76;
      }
      else {
         // Bad number of samples
         CREATE_ROS_EXCEPTION(nsampException, MEtException, MEtException::BAD_NSAMPLES, " " <<nSamples);
         ers::error(nsampException);
         *m_fragStatus|=0x00020008;
         return false;
      }

      DEBUG_TEXT(DFDB_ROSIO, 20, "Adding FEB block  id=" << dataPtr[1]
                 << ",  xenergy=" << dataPtr[offset]
                 << ",  yenergy=" << dataPtr[offset+1]
                 << ",  zenergy=" << dataPtr[offset+2]
                 << ",  summed=" << dataPtr[offset+3]
                 << std::dec);
      *m_writePtr++=dataPtr[1];         // FEB id
      *m_writePtr++=dataPtr[offset];    // X energy
      *m_writePtr++=dataPtr[offset+1];  // Y energy
      *m_writePtr++=dataPtr[offset+2];  // Z energy
      *m_writePtr++=dataPtr[offset+3];  // Summed energy
      m_robHeader->generic.totalFragmentsize+=5;
      return true;
   }
   else {
      CREATE_ROS_EXCEPTION(lengthException, MEtException, MEtException::BAD_LENGTH, "offset1=0");
      ers::error(lengthException);
      *m_fragStatus|=0x00040008;
      return false;
   }
}

void MeTDataRequestDescriptor::addEmptyBlock() {
   int blockSize=4;
   if (m_sourceId>>16==0x007d) {
      blockSize=5;
   }
   for (int word=0; word<blockSize; word++) {
      *m_writePtr++=0;
   }
   m_robHeader->generic.totalFragmentsize+=blockSize;
}


//************************************************************
void MeTDataRequestDescriptor::append(unsigned int* dataPtr) {
   DEBUG_TEXT(DFDB_ROSIO, 10, "MeTDataRequestDescriptor::append() called");
   ROBFragment::ROBHeader* robHeader=(ROBFragment::ROBHeader*) dataPtr;
//    std::cout << std::hex  << std::setfill('0');
//    std::cout <<"ROB header: ";for (int word=0;word<8;word++) {std::cout<< " " << std::setw(8) << dataPtr[word];}std::cout <<std::endl;
   dataPtr+=robHeader->generic.headerSize;
//    std::cout <<"ROD header: ";for (int word=0;word<8;word++) {std::cout<< " " << std::setw(8) << dataPtr[word];}std::cout <<std::endl;
   RODFragment::RODHeader* rodHeader=(RODFragment::RODHeader*) dataPtr;
   dataPtr+=rodHeader->headerSize;
   if (m_firstAppend) {
      m_firstAppend=false;

      m_rodHeader->sourceIdentifier=(rodHeader->sourceIdentifier&0xffff)|(m_sourceId&0xffff0000);
      DEBUG_TEXT(DFDB_ROSIO, 20, "input rodHeader->sourceIdentifier="
                 << std::hex << rodHeader->sourceIdentifier
                 << " modified rodHeader->sourceIdentifier="
                 << m_rodHeader->sourceIdentifier
                 << std::dec);
      m_rodHeader->runNumber=rodHeader->runNumber;
      m_rodHeader->level1Id=rodHeader->level1Id;
   }

   // Make sure that the rest of the header is copied from a real ROB
   // fragment and not one invented by the robin
   if (m_needHeader) {
      if (robHeader->statusElement[0]==0) {
         m_needHeader=false;
      }
      m_rodHeader->formatVersionNumber=rodHeader->formatVersionNumber;
      m_rodHeader->bunchCrossingId=rodHeader->bunchCrossingId;
      m_rodHeader->level1TriggerType=rodHeader->level1TriggerType;
      m_rodHeader->detectorEventType=rodHeader->detectorEventType;
      DEBUG_TEXT(DFDB_ROSIO, 20, "MeTDataRequestDescriptor::append() copied ROD header" << std::hex
                 <<"  formatVersionNumber=" << m_rodHeader->formatVersionNumber
                 <<", bunchCrossingId=" << m_rodHeader->bunchCrossingId
                 <<", detectorEventType=" << m_rodHeader->detectorEventType
                 <<std::dec);
   }


   if (m_sourceId>>16==0x007d) {
      // LAr

      bool firstOK=false;
      bool secondOK=false;
      if (robHeader->statusElement[0]!=0) {
         DEBUG_TEXT(DFDB_ROSIO, 20, "MeTDataRequestDescriptor::append() input robHeader has status=" << std::hex << robHeader->statusElement[0] << std::dec);
         *m_fragStatus|=robHeader->statusElement[0];
      }
      else {
         if (rodHeader->detectorEventType!=4) {
            CREATE_ROS_EXCEPTION(etypeException, MEtException, MEtException::BAD_TYPE, " " <<std::hex << rodHeader->detectorEventType << std::dec);
            ers::error(etypeException);
            *m_fragStatus|=0x00010008;
         }
         else {
            firstOK=processFeb(dataPtr);
         }
      }

      if (firstOK) {
         if (robHeader->generic.totalFragmentsize > robHeader->generic.headerSize + rodHeader->headerSize + dataPtr[0] + 7) {
            dataPtr+=dataPtr[0]+7;
            secondOK=processFeb(dataPtr);
         }
      }
      else {
         addEmptyBlock();
      }
      if (!secondOK) {
         addEmptyBlock();
      }
   }
   else {
      // Tile
      int blockInserted=0;
      unsigned int totalDataSize=0;
      unsigned int rodDataSize=robHeader->generic.totalFragmentsize-(robHeader->generic.headerSize+rodHeader->headerSize);
      if (robHeader->statusElement[0]!=0) {
         DEBUG_TEXT(DFDB_ROSIO, 20, "MeTDataRequestDescriptor::append() input robHeader has status=" << std::hex << robHeader->statusElement[0] << std::dec);
         *m_fragStatus|=robHeader->statusElement[0];
      }
      else {
         if (rodHeader->detectorEventType!=1) { // Physics event=1 for Tile
            CREATE_ROS_EXCEPTION(etypeException, MEtException, MEtException::BAD_TYPE, " " <<std::hex << rodHeader->detectorEventType << std::dec);
            ers::error(etypeException);
            *m_fragStatus|=0x00010008;
         }
         else {
            int block=0;
            while (dataPtr[0]==0xff1234ff && totalDataSize<rodDataSize) {
               unsigned int blockSize=dataPtr[1];
               unsigned int blockType=dataPtr[2]&0x000f0000;
               DEBUG_TEXT (DFDB_ROSIO, 15, "block " << block << ", type " <<std::hex << blockType <<std::dec << ", size " << blockSize);
               if (blockType==0x00040000 && blockSize==54) {
#if DEBUG_LEVEL>0
                  if ((DF::GlobalDebugSettings::packageId()==DFDB_ROSIO) || (DF::GlobalDebugSettings::packageId()==0)) {
                     if (DF::GlobalDebugSettings::traceLevel() >= 20) {
                        std::cout << std::hex << std::setfill('0');
                        for (int index=0; index<54; index++) {
                           std::cout << "   " << std::setw(8) << dataPtr[index];
                           if (index%8==7) std::cout << std::endl;
                        }
                        std::cout << std::setfill(' ') << std::dec << std::endl;
                     }
                  }
#endif
                  unsigned int drawerId=dataPtr[2];
                  DEBUG_TEXT (DFDB_ROSIO, 15, "Found energy sums " << std::hex
                              << "  Drawer=" << drawerId
                              << "  Et=" << dataPtr[51]
                              << "  EZ=" << dataPtr[52]
                              << "  Esum=" << dataPtr[53]
                              << std::dec);
                  *m_writePtr++=drawerId;
                  *m_writePtr++=dataPtr[51];
                  *m_writePtr++=dataPtr[52];
                  *m_writePtr++=dataPtr[53];
                  m_robHeader->generic.totalFragmentsize+=4;
                  blockInserted++;
               }
               totalDataSize+=blockSize;
               if (totalDataSize<rodDataSize) {
                  dataPtr+=blockSize;
               }
               else {
                  CREATE_ROS_EXCEPTION(lengthException, MEtException, MEtException::BAD_LENGTH, "sub block length " << blockSize << ", total length " << totalDataSize << " exceeds ROD data length " << rodDataSize);
                  ers::error(lengthException);
                  *m_fragStatus|=0x00040008;
               }
               block++;
            }
         }
      }
      // Pad any missing blocks with 0
      for (int block=blockInserted; block<4; block++) {
         addEmptyBlock();
      }
   }
   (*m_robCount)++;
   DEBUG_TEXT(DFDB_ROSIO, 10, "MeTDataRequestDescriptor::append() completed");

//    std::cout <<std::dec << std::setfill(' ');
}












unsigned int MeTDataRequestDescriptor::fragment(struct iovec* ioVec, bool addHeader) {
// ************************************************************************
   int index=0;
   int messageSize=0;
   if (addHeader) {
      m_header[0]=0xaa1234aa;
      m_header[1]=0;  // Total fragment size

      m_header[2]=sizeof(m_header)/sizeof(unsigned int); // Total header size
      m_header[3]=0x04000000;  // Format version number

//      m_header[4]=??   // Source Id
      m_header[5]=1;     // Number of status elements
      m_header[6]=0;     // Status word 0
      m_header[7]=0;     // CRC type none
      
      m_header[10]=level1Id();
      m_header[11]=0x0f;
      m_header[13]=level1Id();

      ioVec[index].iov_len=sizeof(m_header);
      ioVec[index].iov_base=&m_header;
      messageSize+= ioVec[index].iov_len ;
      index++;
   }

   std::vector<unsigned int>::iterator chanStartIter;
   std::vector<unsigned int>::iterator chanEndIter=chanStartIter;
   if (m_channelList.size()!=0) {
      chanStartIter=m_channelList.begin();
      chanEndIter=m_channelList.end();
   }
   else if (!m_metRequested) {
      chanStartIter=m_allChannelList.begin();
      chanEndIter=m_allChannelList.end();
   }
   for (std::vector<unsigned int>::iterator chanIter=chanStartIter; chanIter!=chanEndIter; chanIter++) {
      unsigned int id=(*chanIter);
      DataPage* page=dataPage(id);
      unsigned int* data=page->virtualAddress();
      if (data[0]!=0xdd1234dd) {
         std::cout << "Bad fragment marker " << std::hex << data[0] << " for channel " << id << std::dec << " l1 id " << level1Id() << std::endl;
      }

      ioVec[index].iov_base=data;
      ioVec[index].iov_len=data[1]*sizeof(unsigned int);
      messageSize+= ioVec[index].iov_len ;
      index++;
   }


   if (m_metRequested) {
      m_robHeader->statusElement[0]=0;
      m_robHeader->statusElement[1]=0;

      chanStartIter=m_allChannelList.begin();
      chanEndIter=m_allChannelList.end();
      for (std::vector<unsigned int>::iterator chanIter=chanStartIter; chanIter!=chanEndIter; chanIter++) {
         unsigned int id=(*chanIter);
         DataPage* page=dataPage(id);
         unsigned int* data=page->virtualAddress();
         if (data[0]!=0xdd1234dd) {
            std::cout << "Bad fragment marker " << std::hex << data[0] << " for channel " << id << std::dec << " l1 id " << level1Id() << std::endl;
         }
         append(data);
      }
      *m_writePtr++=*m_fragStatus;
      *m_writePtr++=1;  // status elements
      unsigned int fragSize=m_robHeader->generic.totalFragmentsize-m_robHeader->generic.headerSize-m_rodHeader->headerSize;
      // std::cout <<"Setting fragsize to 0x"<< std::hex<<fragSize<<std::endl;
      *m_writePtr++=fragSize;
      *m_writePtr++=1;  // status block follows data
      m_robHeader->generic.totalFragmentsize+=4;

      ioVec[index].iov_base=m_metBuffer;
      ioVec[index].iov_len=m_robHeader->generic.totalFragmentsize*sizeof(unsigned int);
      messageSize+= ioVec[index].iov_len ;
      index++;
#if 0
      std::cout << std::hex  << std::setfill('0');
      std::cout << "Message size=" << messageSize << ",  length of MeT block=" << m_robHeader->generic.totalFragmentsize*sizeof(unsigned int)<<",  index="<<index<<std::endl;
      std::cout <<"met header: ";for (int word=0;word<8;word++) {std::cout<< " " << std::setw(8) << m_metBuffer[word];}std::cout <<std::endl;
      std::cout <<"          : ";for (int word=8;word<16;word++) {std::cout<< " " << std::setw(8) << m_metBuffer[word];}std::cout <<std::endl;
      int nWords=messageSize/4;std::cout <<"...       : ";for (int word=nWords-8;word<nWords;word++) {std::cout<< " " << std::setw(8) << m_metBuffer[word];}std::cout <<std::endl;
      std::cout <<std::dec << std::setfill(' ');
#endif
   }

   ioVec[index].iov_base=0;
   ioVec[index].iov_len=0;

   if (addHeader) {
      m_header[1]=messageSize/sizeof(unsigned int);
   }
   //std::cout << "MeTDataRequestDescriptor::fragment index=" << index << ",  message size="<<messageSize<<std::endl;

   //   for (int entry=0;entry<index;entry++){unsigned int* tPtr=(unsigned int*)ioVec[entry].iov_base;std::cout <<std::hex<< "ioVec[" <<entry<<"] base="<<ioVec[entry].iov_base<<", len="<<ioVec[entry].iov_len<<"  1st data word="<<*tPtr<<", last="<< tPtr[ioVec[entry].iov_len/4-1] <<std::dec<<std::endl;}
   return messageSize;
}
