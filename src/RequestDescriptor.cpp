#include "ROSDescriptor/RequestDescriptor.h"
#include "ROSDescriptor/DescriptorException.h"
#include "ROSUtilities/ROSErrorReporting.h"
using namespace ROS;


uint16_t RequestDescriptor::s_nextId=0;
uint16_t RequestDescriptor::s_nDescriptors=0;
std::vector<RequestDescriptor*> RequestDescriptor::s_descriptorVector;

RequestDescriptor::RequestDescriptor() : m_nRepliesReceived(0) {
   m_id=(s_nextId);
//   std::cout << "Constructing Descriptor " << m_id << std::endl;
   s_descriptorVector.push_back(this);
   s_nextId++;
   s_nDescriptors++;
   for (std::vector<DataChannel*>::iterator chanIter=DataChannel::channels()->begin(); chanIter!=DataChannel::channels()->end(); chanIter++) {
      DescriptorDataChannel* ddChannel=dynamic_cast<DescriptorDataChannel*> (*chanIter);
      if (ddChannel==0) {
         CREATE_ROS_EXCEPTION(badChannelException, DescriptorException, DescriptorException::INCOMPATIBLE_CHANNEL,"");
         ers::fatal(badChannelException);
      }

      m_channels[(*chanIter)->id()]=ddChannel;
      m_allChannelList.push_back((*chanIter)->id());
      std::cout << "DFRequestDescriptor Getting data page from channel " << (*chanIter)->id() << std::endl;
      m_dataPage.push_back(ddChannel->getPage());
   }
}


RequestDescriptor::~RequestDescriptor() {
//    for (auto pageIter : m_dataPage) {
//       delete pageIter.second;
//    }
   for (auto headIter : m_headerPage) {
      if (headIter!=0) {
         delete headIter;
      }
   }

   s_descriptorVector[m_id]=0;
   s_nDescriptors--;
   if (s_nDescriptors==0) {
      s_nextId=0;
      s_descriptorVector.clear();
   }
}


// void RequestDescriptor::initialise(std::unique_ptr<std::vector<unsigned int> > list,
//                                    unsigned int sequence) {
//    ts_clock(&m_initTime);

//    m_channelList.clear();
//    for (std::vector<unsigned int>::iterator chanIter=list->begin(); chanIter!=list->end();chanIter++) {
// //      if (m_channels.find(*chanIter)!=m_channels.end()) {
//          m_channelList.push_back(*chanIter);
// //      }
// //      else {
// //         CREATE_ROS_EXCEPTION(badChannelException, DescriptorException, DescriptorException::BAD_CHANNEL_ID, " id=0x" << std::hex << *chanIter << std::dec);
// //         ers::error(badChannelException);
// //      }
//    }
//    m_nChannels=m_channelList.size();
//    //      std::cout << "Descriptor " << m_sequenceNumber << " initialised with " << m_nChannels << " channels selected\n";
//    if (m_nChannels==0) {
//       m_nChannels=m_allChannelList.size();
//    }
//    m_sequenceNumber=sequence;
//    m_tries=0;
// }


unsigned int RequestDescriptor::addChannel(unsigned int channelId,
                                   unsigned int* pageVirtualAddress,
                                   unsigned int* pagePhysicalAddress,
                                   unsigned int headerSize){

   m_allChannelList.push_back(channelId);

   unsigned int localId=m_pageVec.size();
   m_channelMap[channelId]=localId;

   m_dataPage.emplace_back(new DataPage(pageVirtualAddress,pagePhysicalAddress));

   m_headerPage.emplace_back(new std::vector<unsigned int>(headerSize));
   m_pageVec.push_back(pageVirtualAddress);

   m_fragmentStatus.push_back(0);

   m_responseReceived.push_back(false);

   return localId;
}

DataPage* RequestDescriptor::dataPage(unsigned int channelIndex){
   return m_dataPage[channelIndex];
}

std::vector<unsigned int>* RequestDescriptor::headerPage(unsigned int channelIndex){
   return m_headerPage[channelIndex];
}

std::vector<unsigned int>* RequestDescriptor::localIds() {
   return &m_localList;
}

void RequestDescriptor::initialise(const std::vector<unsigned int>* list,
                                   unsigned int sequence) {
   m_initialiseTime=std::chrono::system_clock::now();

   if (m_channelList.size()) {
      m_channelList.clear();
      m_localList.clear();
   }
   if (list && list->size()) {
      for (auto chanIter=list->begin(); chanIter!=list->end();chanIter++) {
//         if (m_channels.find(*chanIter)!=m_channels.end()) {
            m_channelList.push_back(*chanIter);

            m_localList.push_back(m_channelMap[*chanIter]);
//          }
//          else {
//             CREATE_ROS_EXCEPTION(badChannelException, DescriptorException, DescriptorException::BAD_CHANNEL_ID, " id=0x" << std::hex << *chanIter << std::dec);
//             ers::error(badChannelException);
//          }
      }
      m_nChannels=m_channelList.size();
      //      std::cout << "Descriptor " << m_sequenceNumber << " initialised with " << m_nChannels << " channels selected\n";
   }
   else {
      m_nChannels=m_allChannelList.size();
   }
   m_sequenceNumber=sequence;
   m_tries=0;
}

void RequestDescriptor::gotData(unsigned int channelIndex) {
   m_responseReceived[channelIndex]=true;
}


bool RequestDescriptor::complete(unsigned int channelIndex) {
   m_responseReceived[channelIndex]=true;
   m_nRepliesReceived++;

//    std::cout << "RequestDescriptor " << m_id
//               << " received reply for channel " << std::hex << channelId << std::dec
//               << ",  number of responses received=" << m_nRepliesReceived
//               << ", number required=" << m_nChannels << std::endl;
   if (m_nRepliesReceived==m_nChannels) {
      return true;
   }
   else {
//       dump();
      return false;
   }
}
