// -*- c++ -*-

#ifndef ROLTHREAD_H
#define ROLTHREAD_H

#include <vector>
#include <string>

#include "ROSRobin/Rol.h"
#include "RobinThread.h"

namespace ROS 
{
   class DataRequestDescriptor;
   class ClearRequestDescriptor;

   //! Special implementation of Rol with RequestDescriptor versions
   //!of requestFragment and releaseFragment methods
   class RolThread: public Rol {
   public:
      RolThread(RobinThread* robinBoard, u_int rolId);
      ~RolThread();
      void requestFragment(DataRequestDescriptor* descriptor, u_int rolId);
      void releaseFragment(ClearRequestDescriptor* descriptor, u_int rolId);
   private:
      RobinThread* m_robinThread;  //!< Pointer to the RobinThread that this RolThread belongs to
   };
}
#endif //ROL_H
