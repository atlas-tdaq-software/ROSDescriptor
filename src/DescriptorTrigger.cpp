// $Id: $
/*
    $Log$

*/
#include <thread>
#include <chrono>

#include "ROSDescriptor/DescriptorTrigger.h"
#include "ROSDescriptor/DescriptorReadoutModule.h"
#include "ROSDescriptor/DataRequestDescriptor.h"
#include "ROSDescriptor/DescriptorDataChannel.h"
#include "MeTDataRequestDescriptor.h"
#include "DataCollector.h"

//#define DEBUG 1

using namespace ROS;

DescriptorTrigger::DescriptorTrigger() : m_dataCollector(0),
                                         m_nextDataDescriptor(0),
                                         m_nextClearDescriptor(0),
                                         m_nDataDescriptors(0),
                                         m_nClearDescriptors(0),
                                         m_triggerActive(false) {
}

void DescriptorTrigger::getModules(IOManager* iom) {
   //m_ioManager=iom;
   for (auto module : *(iom->readoutModules())) {
      DescriptorReadoutModule* descrModule=
         dynamic_cast<DescriptorReadoutModule*>(module);
      if (descrModule) {
         m_readoutModules.push_back(descrModule);
      }
      else {
         std::cerr << "ReadoutModule is not a DescriptorReadoutModule\n";
      }
   }
}


void DescriptorTrigger::setupQueues(DFCountedPointer<Config> config) {

   createDescriptors(config);

   m_nextDataDescriptor=m_nDataDescriptors;
   m_nextClearDescriptor=m_nClearDescriptors;

   m_descriptorQueue=new ObjectQueue<RequestDescriptor>(m_nDataDescriptors+m_nClearDescriptors);
   m_dataCollector=new DataCollector(m_descriptorQueue);
}



void DescriptorTrigger::createDescriptors(DFCountedPointer<Config> config) {
   m_nDataDescriptors=config->getInt("DataDescriptors");
   m_nClearDescriptors=config->getInt("ClearDescriptors");
   unsigned int timeout=config->getUInt("DataRequestTimeout");

//   unsigned int detectorId=config->getUInt("SubDetectorId");
   for (unsigned int entry=0; entry<m_nDataDescriptors; entry++) {
//       if ((detectorId&0x00f0)==0x0040 || (detectorId&0x00f0)==0x0050) {
//      m_dataDescriptors.push_back(new MeTDataRequestDescriptor(timeout));
//       }
//       else {
      auto descriptor=new DataRequestDescriptor(timeout);
      for (auto module : m_readoutModules) {
         module->registerChannels(descriptor);
      }
      m_dataDescriptors.push_back(descriptor);
//      }
   }

   for (unsigned int entry=0; entry<m_nClearDescriptors; entry++) {
      auto descriptor=new ClearRequestDescriptor;
      for (auto module : m_readoutModules) {
         module->registerChannels(descriptor);
      }
      m_clearDescriptors.push_back(descriptor);
   }

   std::cout << "DescriptorTrigger::configure "
             << "Created " << m_nDataDescriptors << " DataRequestDescriptors and "
             << m_nClearDescriptors << " ClearRequestDescriptors\n";
}


void DescriptorTrigger::deleteQueues() {
   std::cout << "DescriptorTrigger::deleteQueues() "
             << m_nextDataDescriptor << " data descriptors";
   if (m_nClearDescriptors>0) {
      std::cout << " and " << m_nextClearDescriptor << " clear descriptors";
   }
   std::cout << " available\n";


   delete m_dataCollector;
   m_dataCollector=0;
   delete m_descriptorQueue;
   m_descriptorQueue=0;

   for (unsigned int desc=0;desc<m_nDataDescriptors;desc++) {
      delete m_dataDescriptors[desc];
   }

   for (unsigned int desc=0;desc<m_nClearDescriptors;desc++) {
      delete m_clearDescriptors[desc];
   }
}



void DescriptorTrigger::start() {
   // debug
   if (m_nextDataDescriptor!=m_nDataDescriptors) {std::cout << "+ WARNING + + WARNING + Wrong # of data descriptors " << m_nextDataDescriptor << " + WARNING + + WARNING + \n";}
   //

   // Clear any stale descriptors from our input queue before we start
   // otherwise they may be re-tried
   processDescriptorQueue();

   std::cout << "DescriptorTrigger::start "
             << m_nextDataDescriptor << " data descriptors and "
             << m_nextClearDescriptor << " clear descriptors available\n";
   if (m_nextDataDescriptor!=m_nDataDescriptors) {
      std::cout << "+ WARNING + + WARNING + Wrong # of data descriptors " << m_nextDataDescriptor << " + WARNING + + WARNING + \n";
      checkDescripors();
   }

   m_triggerActive=true;
   m_dataCollector->start();
}



void DescriptorTrigger::stop() {
   m_triggerActive=false;
   bool timeout=false;
   int startTime=time(0);
   std::cout << "DescriptorTrigger::stop "
             << m_nextDataDescriptor << " data descriptors and "
             << m_nextClearDescriptor << " clear descriptors available\n";
   while ((m_nextDataDescriptor<m_nDataDescriptors || m_nextClearDescriptor<m_nClearDescriptors) && !timeout){
      processDescriptorQueue();
      timeout=(time(0)-startTime > 4);
      if (timeout) {
         std::cout << "Timeout waiting for descriptors to come back  start="<<startTime<<", now="<<time(0)<<std::endl;
         m_dataCollector->dump();
      }
      else {
         std::this_thread::sleep_for(std::chrono::microseconds(100));
      }
   }
   std::cout << "DescriptorTrigger::stop "
             << m_nextDataDescriptor << " data descriptors and " << m_nextClearDescriptor << " clear descriptors available\n";

   m_dataCollector->stop();
}

void DescriptorTrigger::user(const daq::rc::UserCmd& command) {
	if (command.commandName()=="dump") {
      std::vector<std::string> arguments=command.commandParameters();
      for (auto argv : arguments) {
         if (argv=="descriptors") {
            uint32_t ndumped=0;
            uint16_t id=0;
            while (ndumped<m_nDataDescriptors+m_nClearDescriptors) {
               RequestDescriptor* descr=RequestDescriptor::getDescriptor(id);
               if (descr) {
                  descr->dump();
                  ndumped++;
               }
               id++;
            }
         }
      }
   }
}



void DescriptorTrigger::submitDataRequest(unsigned int level1Id,
                                          const std::vector<unsigned int>* dataChannels,
                                          unsigned int transactionId,
                                          unsigned int requestNumber, unsigned int tag){
#ifdef DEBUG
   if (m_nextDataDescriptor==0) {
      std::cout << "Out of DataRequestDescriptors\n";
   }
#endif
   while (m_nextDataDescriptor==0) {
      int nProc=processDescriptorQueue();
      if (nProc==0) {
         std::this_thread::sleep_for(std::chrono::nanoseconds(1));
      }
   }
   DataRequestDescriptor* request=
      dynamic_cast<DataRequestDescriptor*>(m_dataDescriptors[--m_nextDataDescriptor]);
   if (request){
      request->initialise(level1Id, dataChannels, transactionId, requestNumber, tag);
      request->submit();
   }
   else {
      std::cerr << "Submitting request which is not a DataRequest!!\n";
   }
}



void DescriptorTrigger::submitClearRequest(unsigned int* level1IdList, unsigned int nClears,
                                           unsigned int lastValid,
                                           unsigned int sequence) {
#ifdef DEBUG
   if (m_nextClearDescriptor==0) {
      std::cout << "Out of ClearRequestDescriptors\n";
   }
#endif
   while (m_nextClearDescriptor==0) {
      int nProc=processDescriptorQueue();
      if (nProc==0) {
         sched_yield();
      }
   }
   ClearRequestDescriptor* request=m_clearDescriptors[--m_nextClearDescriptor];
   request->initialise(level1IdList, nClears, lastValid, sequence);
   std::map<unsigned int, DescriptorDataChannel*>* dataChannels=request->channels();
   std::map<unsigned int, DescriptorDataChannel*>::iterator chanStartIter=dataChannels->begin();
   std::map<unsigned int, DescriptorDataChannel*>::iterator chanEndIter=dataChannels->end();
   for (std::map<unsigned int, DescriptorDataChannel*>::iterator chanIter=chanStartIter; chanIter!=chanEndIter; chanIter++) {
      (*chanIter).second->descriptorReleaseFragment(request);
   }
}

void DescriptorTrigger::checkDescripors() {
   std::cout << "Descriptot Ids:";
   std::set<int> seen;
   int duplicate=-1;
   for (unsigned int desc=0; desc<m_nextDataDescriptor; desc++) {
      int id=m_dataDescriptors[desc]->getId();
      std::cout << " " << id;
      if (seen.find(id)!=seen.end()){
         std::cout << "<--";
         duplicate=desc;
      }
      else {
         seen.insert(id);
      }
   }
   std::cout << std::endl;
   if (duplicate!=-1) {
      std::cout << "Duplicated descriptor entry " << duplicate << std::endl;
      m_dataDescriptors[duplicate]->dump();
   }

   for (unsigned int desc=0; desc<m_nDataDescriptors; desc++) {
      int id=m_dataDescriptors[desc]->getId();
      if (seen.find(id)==seen.end()){
         std::cout << "missing descriptor entry " << desc << std::endl;
         m_dataDescriptors[desc]->dump();
      }
   }
}


int DescriptorTrigger::processDescriptorQueue() {
   std::lock_guard<std::mutex> lock(m_mutex);
   int nProc=0;
   while (RequestDescriptor* descriptor=m_descriptorQueue->fastPop(false)) {
      nProc++;
      if (DataRequestDescriptor* dataDescr=dynamic_cast<DataRequestDescriptor*>(descriptor)) {
         if (dataDescr->terminated() || !m_triggerActive) {
            m_dataDescriptors[m_nextDataDescriptor++]=dataDescr;
            // debug
            if (m_nextDataDescriptor>m_nDataDescriptors) {
               std::cout << "+ WARNING + + WARNING + Too many data descriptors " << m_nextDataDescriptor << " + WARNING + + WARNING + \n";
               checkDescripors();
            }
            //
         }
         else {
            dataDescr->retry();
         }
      }
      else if (ClearRequestDescriptor* clearDescr=dynamic_cast<ClearRequestDescriptor*>(descriptor)) {
         m_clearDescriptors[m_nextClearDescriptor++]=clearDescr;
      }
   }
   //   std::cout << "DCTrigger::processDescriptorQueue  processed " << nProc << " descriptors\n";
   return nProc;
}


