#include <thread>
#include <chrono>

#include <sys/time.h>
#include <sys/resource.h>
#include <errno.h>



#include "DFDataCollector.h"
#include "DFDescriptorTrigger.h"
#include "DFRequestDescriptor.h"

#include "ROSDescriptor/ObjectQueue.h"

#include "ROSDescriptor/DataRequestDescriptor.h"

using namespace ROS;



// ************************************************************************
DFDataCollector::DFDataCollector(DFDescriptorTrigger* trigger,
                             ObjectQueue<RequestDescriptor>* collectionQueue,
                             ObjectQueue<RequestDescriptor>* returnQueue) :
// ************************************************************************
   DataCollector(returnQueue),
   m_trigger(trigger),
   m_requestsTimedOut(0),
   m_collectionQueue(collectionQueue) {
}



// ************************************************************************
DFDataCollector::~DFDataCollector() {
// ************************************************************************
}


// ************************************************************************
uint64_t DFDataCollector::timeOuts() {
// ************************************************************************
   return m_requestsTimedOut;
}

// ************************************************************************
void DFDataCollector::start() {
// ************************************************************************
   m_collectionQueue->open();
   DataCollector::start();
}


// ************************************************************************
void DFDataCollector::dump() {
// ************************************************************************
   std::cout << "****  DFDataCollector collection queues  ****\n";
   m_collectionQueue->dump();

   std::cout << "****  DFDataCollector return queue  ****\n";
   m_returnQueue->dump();
}


// ************************************************************************
void DFDataCollector::stop() {
// ************************************************************************
   m_runActive=false;
   m_collectionQueue->close();

   std::cout << "DFDataCollector::stop waiting for thread to finish\n";
   m_thread->join();

   delete m_thread;
   m_thread=0;
}

#define DEBUG
// ************************************************************************
void DFDataCollector::run() {
// ************************************************************************
   struct timeval startTime;
   struct timezone dummy;
   gettimeofday(&startTime,&dummy);

   struct rusage resourcesStart;
   int status=getrusage(RUSAGE_SELF, &resourcesStart);
   if (status) {
      perror("Error from getrusage");
   }

   std::cout << "DFDataCollector::run: Entering loop\n";

   m_runActive=true;
   do {
      RequestDescriptor* descriptor=m_collectionQueue->fastPop(true);
      if (descriptor) {
//          std::cout << "DFDataCollector::run dequeued descriptor\n";
         DFRequestDescriptor* dfDesc=dynamic_cast<DFRequestDescriptor*>(descriptor);
         if (dfDesc) {
            //std::cout << "DFDataCollector::run  event " << dfDesc->level1Id();
            dfDesc->gotReply();
            if (dfDesc->complete()) {
                //std::cout << " is complete\n";

               bool timedOut=dfDesc->timedOut();
               if (timedOut) {
                  m_requestsTimedOut++;
               }

               bool pending=dfDesc->channelsPending();
#ifdef DEBUG
               if (descriptor->tries() > 1) {
                  std::cout << "Request ";
                  if (pending) {
                     std::cout << "timed out (descriptor "; descriptor->dump(); std::cout << ")";
                  }
                  else {
                     std::cout << "completed";
                  }
                  std::cout << " after " << descriptor->tries() << " tries\n";
               }
#endif

               if (!pending || timedOut) {
//                     std::cout << "DFDataCollector::run  sending event " << descriptor->level1Id() << "\n";
                  dfDesc->terminate();
                  dfDesc->send(m_returnQueue);
               }
               else {
                  //  Requeue this one
                  m_trigger->resubmit(dfDesc);
               }
            }
             else {
                //std::cout << " not finished yet\n";
             }
         }
         else {
            std::cout << "DFDataCollector received a non-DF descriptor\n";
         }
      }
      else {
         std::cout << "Wake up without descriptor\n";
      }
   } while (m_runActive);



   struct rusage resourcesEnd;
   status=getrusage(RUSAGE_SELF, &resourcesEnd);
   if (status) {
      perror("Error from getrusage");
   }
   struct timeval endTime;
   gettimeofday(&endTime,&dummy);
   float elapsedTime=(endTime.tv_sec-startTime.tv_sec) +
      (endTime.tv_usec-startTime.tv_usec)/1000000.0;
   float elapsedUserTime=(resourcesEnd.ru_utime.tv_sec-resourcesStart.ru_utime.tv_sec) +
      (resourcesEnd.ru_utime.tv_usec-resourcesStart.ru_utime.tv_usec)/1000000.0;
   float elapsedSystemTime=(resourcesEnd.ru_stime.tv_sec-resourcesStart.ru_stime.tv_sec) +
      (resourcesEnd.ru_stime.tv_usec-resourcesStart.ru_stime.tv_usec)/1000000.0;
   std::cout << "DFDataCollector::run()  Elapsed time "<< elapsedTime << "s  (" << elapsedUserTime
             << "s user, " << elapsedSystemTime << "s system). "
             << resourcesEnd.ru_nvcsw-resourcesStart.ru_nvcsw << " voluntary context switches ("
             << resourcesEnd.ru_nivcsw-resourcesStart.ru_nivcsw<< " involuntary).\n";
}


// ************************************************************************
//void DFDataCollector::cleanup() {
// ************************************************************************
//}
