#include <iostream>

#include "ROSDescriptor/ClearRequestDescriptor.h"
using namespace ROS;


ClearRequestDescriptor::ClearRequestDescriptor() : m_level1Ids(0) {
}


ClearRequestDescriptor::~ClearRequestDescriptor() {
   if (m_level1Ids) {
      delete [] m_level1Ids;
   }
}


/**************************************************************/
void ClearRequestDescriptor::initialise(unsigned int* level1Id,
                                        unsigned int nLevel1Ids,
                                        unsigned int oldestValid,
                                        unsigned int sequence) {
/**************************************************************/

   if (m_level1Ids) {
      delete [] m_level1Ids;
   }
   m_level1Ids=level1Id;
   m_nLevel1Ids=nLevel1Ids;
   m_oldest=oldestValid;
   m_nRepliesReceived=0;
   m_nChannels=m_channels.size();

   for (auto chanIter : m_allChannelList) {
      m_responseReceived[chanIter]=false;
   }

}


unsigned int ClearRequestDescriptor::oldestValid(){
   return m_oldest;
}


void ClearRequestDescriptor::submit() {
   for (auto chanIter: m_channels) {
      chanIter.second->descriptorReleaseFragment(this);
   }
}

void ClearRequestDescriptor::dump() {
   std::cout << "ClearRequestDescriptor::nChannels=" << m_channels.size()
             << ", replies=" << m_nRepliesReceived
             << ", nlevel1Ids=" << m_nLevel1Ids
             << ", level1Ids=";
   for (unsigned int index=1; index<=m_nLevel1Ids; index++) {
      std::cout << m_level1Ids[index] << " ";
   }
   std::cout << std::endl;

   for (auto chanIter : m_channels) {
      DataPage* page=chanIter.second->getPage();
      std::cout  << std::hex
                 << "Channel " << chanIter.first
                 << " page virtAddr=" << page->virtualAddress()
                 << " physAddre=" << page->physicalAddress()
                 << std::dec << std::endl;
   }
}
