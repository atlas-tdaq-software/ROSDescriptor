// -*- c++ -*-
//
// ///////////////////////////////////////////////////////////////////////////

#ifndef TCPOUTPUT_H
#define TCPOUTPUT_H

#include <string>

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>      /* close ... */
#include <sys/time.h>    /* select struct timeval */
#include <string.h>      /* memset & bzero in FD_ZERO */
#include <netinet/in.h>  /* struct sockaddr_in ... */
#include <arpa/inet.h>   /* for inet_addr */
#include <netinet/tcp.h> /* TCP_NODELAY */
#include <sys/socket.h>  /* socket setsockopt bind listen connect */
#include <netdb.h>       /* getprotobyname gethostbyaddr */
#include <errno.h>       /* errno */

#include "ROSDescriptor/DescriptorDataOut.h"

#include "ROSInfo/TCPDataOutInfo.h"

#include "ROSCore/DataOut.h"

#define TCP_WRTIMEOUT       {0,10000}

#define TCP_BUFFER_LENGTH   256*1024


namespace ROS {

   /**
      Implementation of DataOut to send data over a TCP connection.
   */

   class TCPOutput : public DescriptorDataOut {
   private:
      unsigned short m_port;
      std::vector<std::string> m_destinationNode;

      int m_fragmentsInput;
      int m_samplingGap;
      int m_TCPbufSize;
      bool m_throwIfUnavailable;

      std::vector<int> m_socket;
      struct timeval m_wrtimeout;

      int m_errno;

      TCPDataOutInfo m_stats;
   public:
      TCPOutput ();
      virtual ~TCPOutput () noexcept;

      void setup (DFCountedPointer<Config> configuration);

      /** Opens output stream specified by configuration. */
      void connect (const daq::rc::TransitionCmd&);
      /** Closes output stream */
      void disconnect (const daq::rc::TransitionCmd&);

      /** Reset conters **/
      void prepareForRun (const daq::rc::TransitionCmd&);

      void sendData (const Buffer* buffer, const NodeID,
                     const unsigned int transactionId=1,
                     const unsigned int fragStatus=0);
      void send(DataRequestDescriptor* descriptor);

      ISInfo* getISInfo ();

   private:
      int TCPSafeSend (const void* data, int length, int socketNumber);
   };
}
#endif
