#include <thread>
#include <chrono>

#include <sys/time.h>
#include <sys/resource.h>
#include <errno.h>



#include "DataCollector.h"

#include "ROSDescriptor/ObjectQueue.h"

#include "ROSDescriptor/DataRequestDescriptor.h"
#include "ROSDescriptor/DescriptorDataChannel.h"
#include "ROSDescriptor/DescriptorDataOut.h"

using namespace ROS;



// ************************************************************************
DataCollector::DataCollector(ObjectQueue<RequestDescriptor>* returnQueue) :
// ************************************************************************
   m_returnQueue(returnQueue), m_stopTimeout(100000) {
   std::vector<DataChannel*>* channels=DataChannel::channels();
   for (std::vector<DataChannel*>::iterator chanIter=channels->begin(); chanIter!=channels->end(); chanIter++) {
      // get Q for channel
      m_channelQueue.push_back(dynamic_cast<DescriptorDataChannel*>(*chanIter)->getAckQueue());
      // get id of channel
      m_channelId.push_back((*chanIter)->id());
   }
}



// ************************************************************************
DataCollector::~DataCollector() {
// ************************************************************************
}


// ************************************************************************
void DataCollector::start() {
// ************************************************************************
   m_runActive=true;
   m_thread=new std::thread(&DataCollector::run,this);

}


// ************************************************************************
void DataCollector::stop() {
// ************************************************************************
   m_runActive=false;

   std::cout << "DataCollector::stop waiting for thread to finish\n";
   std::this_thread::sleep_for(std::chrono::milliseconds(1));

   m_thread->join();

   delete m_thread;
   m_thread=0;

}


// ************************************************************************
void DataCollector::dump() {
// ************************************************************************
   std::cout << "****  DataCollector channel queues  ****\n";
   for (unsigned int chanIndex=0; chanIndex<m_channelQueue.size(); chanIndex++) {
      m_channelQueue[chanIndex]->dump();
   }

   std::cout << "****  DataCollector return queue  ****\n";
   m_returnQueue->dump();
}

#define DEBUG
// ************************************************************************
void DataCollector::run() {
// ************************************************************************
   struct timeval startTime;
   struct timezone dummy;
   gettimeofday(&startTime,&dummy);

   struct rusage resourcesStart;
   int status=getrusage(RUSAGE_SELF, &resourcesStart);
   if (status) {
      perror("Error from getrusage");
   }


   std::cout << "DataCollector::run Polling on " << m_channelQueue.size() << " ack queues\n";

   m_runActive=true;
   int nProcessed;
   do {
      nProcessed=0;
      for (unsigned int chanIndex=0; chanIndex<m_channelQueue.size(); chanIndex++) {
         RequestDescriptor* descriptor=m_channelQueue[chanIndex]->fastPop(false);
         if (descriptor) {


            //std::cout << "DataCollector::run dequeued descriptor\n";
#ifdef DEBUG
            if (!m_runActive) std::cout << "DataCollector::run  got descriptor from chanIndex " << chanIndex <<std::endl;
            //descriptor->dump();
#endif
            if (descriptor->complete(m_channelId[chanIndex])) {
#ifdef DEBUG
//               if (!m_runActive)
               //std::cout << "DataCollector::run  event is complete\n";
               //              descriptor->dump();
#endif
               DataRequestDescriptor* dataDesc=dynamic_cast<DataRequestDescriptor*>(descriptor);
               if (dataDesc) {
//                  std::cout << "DataCollector::run  event " << dataDesc->level1Id() << " is complete\n";

                  bool timedOut=dataDesc->timedOut();
                  bool pending=dataDesc->channelsPending();

#if 1
                  if (dataDesc->tries() > 1) {
                     std::cout << "Request ";
                     if (pending) {
                        std::cout << "timed out (descriptor "; dataDesc->dump(); std::cout << ")";
                     }
                     else {
                        std::cout << "completed";
                     }
                     std::cout << " after " << dataDesc->tries() << " tries\n";
                  }
#endif

                  if (!pending || timedOut) {
//                     std::cout << "DataCollector::run  sending event " << dataDesc->level1Id() << "\n";
                     DescriptorDataOut* mainOut=dynamic_cast<DescriptorDataOut*>(DataOut::main());
                     if (mainOut != 0) {
                        //std::cout << "sending event " << dataDesc->level1Id() << "\n";
                        mainOut->send(dataDesc);
                     }
                     else if (DataOut::main()) {
                        std::cout << "Main DataOut does not support Descriptor passing interface\n";
                     }
                     dataDesc->terminate();
                     // Put descriptor back on free list
                     //            std::cout << "DataCollector::run finished with descriptor for event " << dataDesc->level1Id() << "\n";
                     m_returnQueue->fastPush(dataDesc);
                  }
                  else {
                     // Let the trigger requeue this one
                     m_returnQueue->push(descriptor);
                  }
               }
               else {
                  //                  std::cout << "Not a data request\n";
                  // Put descriptor back on free list
                  m_returnQueue->push(descriptor);
               }
            }
            nProcessed++;
         }
      }

      if (nProcessed==0 && m_runActive) {
         std::this_thread::sleep_for(std::chrono::nanoseconds(1));
      }
   } while (m_runActive || nProcessed>0);



   struct rusage resourcesEnd;
   status=getrusage(RUSAGE_SELF, &resourcesEnd);
   if (status) {
      perror("Error from getrusage");
   }
   struct timeval endTime;
   gettimeofday(&endTime,&dummy);
   float elapsedTime=(endTime.tv_sec-startTime.tv_sec) +
      (endTime.tv_usec-startTime.tv_usec)/1000000.0;
   float elapsedUserTime=(resourcesEnd.ru_utime.tv_sec-resourcesStart.ru_utime.tv_sec) +
      (resourcesEnd.ru_utime.tv_usec-resourcesStart.ru_utime.tv_usec)/1000000.0;
   float elapsedSystemTime=(resourcesEnd.ru_stime.tv_sec-resourcesStart.ru_stime.tv_sec) +
      (resourcesEnd.ru_stime.tv_usec-resourcesStart.ru_stime.tv_usec)/1000000.0;
   std::cout << "DataCollector::run()  Elapsed time "<< elapsedTime << "s  (" << elapsedUserTime
             << "s user, " << elapsedSystemTime << "s system). "
             << resourcesEnd.ru_nvcsw-resourcesStart.ru_nvcsw << " voluntary context switches ("
             << resourcesEnd.ru_nivcsw-resourcesStart.ru_nivcsw<< " involuntary).\n";
}


// ************************************************************************
void DataCollector::cleanup() {
// ************************************************************************
}
