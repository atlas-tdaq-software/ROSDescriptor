
#include "DFRequestDescriptor.h"
#include "RosDataDescriptorOutputMsg.h"
#include "ROSasyncmsg/DataServer.h"
#include "ROSDescriptor/DescriptorException.h"
#include "ROSUtilities/ROSErrorReporting.h"

#include "ers/ers.h"

#include <iomanip>


using namespace ROS;


// ************************************************************************
DFRequestDescriptor::DFRequestDescriptor(unsigned int timeout)
   : DataRequestDescriptor(timeout),m_destination(0) {
// ************************************************************************
}


// ************************************************************************
DFRequestDescriptor::~DFRequestDescriptor() {
// ************************************************************************
}



// ************************************************************************
void DFRequestDescriptor::initialise(unsigned int level1Id,
//                                     std::unique_ptr<std::vector<unsigned int> > myChannels,
                                     const std::vector<unsigned int>* myChannels,
                                     DataServerSession* destination,
                                     unsigned int transactionId,
                                     std::chrono::time_point<std::chrono::system_clock> requestTime) {
// ************************************************************************
   RequestDescriptor::initialise(myChannels,0);
#if 0
   std::cout << "DFRequestDescriptor::initialise:"
             << " level1Id=" << level1Id
      ;//             <<std::endl;
#endif
   if (channelList()->size()>0) {
      for (auto chanIter : *channelList()) {
         //std::cout << " selected req  Adding channel " << chanIter;
         unsigned int index=m_channelMap[chanIter];
         m_responseReceived[index]=false;
         m_fragmentStatus[index]=0;
      }
   }
//    else {
//       //std::cout << " full data req ";
//       for (auto chanIter : m_allChannelList) {
//          //std::cout << "   Adding channel " << chanIter;
//          m_responseReceived[chanIter]=false;
//          m_status[chanIter]=0;
//       }
//    }
   //std::cout  << std::endl;
   m_requestTime=requestTime;
   m_level1Id=level1Id;
   m_destination=destination;
   m_transactionId=transactionId;
   m_nRepliesReceived=0;
   m_terminated=false;
   m_submitted=false;
   m_repliesExpected=0;
}

void DFRequestDescriptor::send(ObjectQueue<RequestDescriptor> * ackQueue) {
   std::unique_ptr<RosDataDescriptorOutputMsg> dataMsg(
      new RosDataDescriptorOutputMsg(m_transactionId,this,ackQueue));

   try {
      m_destination->asyncSend(std::move(dataMsg));
   }
   catch (std::exception& exc) {
      std::cerr << "DFRequestDescriptor::send caught a std::exception\n";
   }

}

unsigned int DFRequestDescriptor::size() {
   uint32_t sum=0;
   // Do sum addition
   for (auto channel : m_channelList) {
      if (m_headerPage[channel]!=0) {
         sum+=m_headerPage[channel]->at(1)*sizeof(unsigned int);
      }
      else {
         auto page=m_dataPage[channel];
         unsigned int* data=page->virtualAddress();
         sum+=data[1]*sizeof(unsigned int);
      }
   }
   return sum;
}

unsigned int DFRequestDescriptor::nChannels() {
   return m_allChannelList.size();
}

// ************************************************************************
void DFRequestDescriptor::dump() {
// ************************************************************************
   std::cout << "DFRequestDescriptor " << m_id << ": addr=" << this
             << " level1Id=" << m_level1Id << " (" << std::hex << m_level1Id << ")" << std::dec
      << ", terminated=" <<  m_terminated
      //<< ", tries=" << m_tries
             << ", destination=" << m_destination;
   if (m_destination!=0) {
      std::cout << " (" << m_destination->remoteEndpoint() << ")";
   }
   std::cout << ", transactionId=" << std::hex << m_transactionId << std::dec
             << ", replies=" << m_nRepliesReceived << "/" << m_repliesExpected
             << ", channels=" << m_channelList.size()
             << ", channel ids=";
   for (auto chanIter : m_channelList) {
      std::cout << chanIter << " (0x" << std::hex << chanIter << ") " <<std::dec;
   }
   std::cout << std::endl;


   std::cout  << std::hex << std::setfill('0');
   for (auto chanIter : m_localList) {
      std::cout << "Channel " << chanIter
                << ": response=" << m_responseReceived[chanIter]
                << ", Status="  << std::setw(8) << m_fragmentStatus[chanIter];
         //<< ", header address=" << m_headerPage[chanIter];
      if (m_headerPage[chanIter]!=0) {
         std::cout << ", Header:";
         for (unsigned int word=0;word<8; word++) {
            std::cout << " " << m_headerPage[chanIter]->at(word);
         }
      }
      std::cout << ", Data:";
      unsigned int* tPtr=m_dataPage[chanIter]->virtualAddress();
      for (unsigned int word=0;word<16; word++) {
         std::cout << " " << std::setw(8) << tPtr[word];
      }
      std::cout << "...\n";
   }

   std::cout << std::dec << std::setfill(' ');
}
