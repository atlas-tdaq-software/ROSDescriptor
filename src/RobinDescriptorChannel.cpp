/*********************************************************/
/*  ATLAS ROS Software 			 		 */
/*							 */
/*  Class: RobinDescriptorChannel 			 	 */
/*  Author: Markus Joos, CERN PH/ESE 			 */
/*							 */
/*** C 2007 - The software with that certain something ***/
     
#include "RobinDescriptorChannel.h"

#include "ROSDescriptor/DataRequestDescriptor.h"
#include "ROSDescriptor/ClearRequestDescriptor.h"

#include "ROSRobin/Rol.h"
#include "ROSRobin/ROSRobinExceptions.h"
#include "ROSModules/ModulesException.h"

#include "ROSEventFragment/ROBFragment.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/WrapperMemoryPool.h"

#include "DFSubSystemItem/Config.h"
#include "DFSubSystemItem/ConfigException.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "rcc_time_stamp/tstamp.h"
//#include "robin_ppc/robin.h"

#include "eformat/eformat.h"


#define PCI_ROBIN_SYNCHRONIZE

using namespace ROS;

/*********************************************************************************/
RobinDescriptorChannel::RobinDescriptorChannel(u_int id, u_int configId, u_int physicalAddress, 
                                   RolThread& rol_ptr, u_int RobinphysicalAddress,
				   WrapperMemoryPool *mpool, unsigned int crcInterval) :
/*********************************************************************************/
   DescriptorDataChannel(id, configId, physicalAddress, 50, mpool),
  m_rol(rol_ptr),
  m_gcdeleted(0),
  m_gcfree(0),
  m_id(id),
  m_l1id_modulo(0),
  m_RobinphysicalAddress(RobinphysicalAddress),
  m_crcInterval(crcInterval),
  m_fragCount(0)
{
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDescriptorChannel::constructor: Called");

  for (u_int bit = 0; bit < 32; bit++) {
    m_statistics.statusErrors.push_back(0);
  }
  clearInfo();

  if (configId == 0)
    m_infoflag = 1;
  else
    m_infoflag = 0;

  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDescriptorChannel::constructor: m_memoryPool has " << m_memoryPool->numberOfPages() << " pages");

  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDescriptorChannel::constructor: Done");
}


/***********************************/
RobinDescriptorChannel::~RobinDescriptorChannel()
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 10, "RobinDescriptorChannel::destructor: Called");

  DEBUG_TEXT(DFDB_ROSFM, 10, "RobinDescriptorChannel::destructor: Done");
}


/***********************************/
ISInfo* RobinDescriptorChannel::getISInfo()
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::getISInfo: called");
  return &m_statistics;
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::getISInfo: done");
}


/**************************************************/
void RobinDescriptorChannel::setExtraParameters(int value)
/**************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 10, "RobinDescriptorChannel::setExtraParameters: Called");
  m_l1id_modulo = value;
  DEBUG_TEXT(DFDB_ROSFM, 10, "RobinDescriptorChannel::setExtraParameters: Done");
}



/*************************************************************************/
void RobinDescriptorChannel::descriptorRequestFragment(DataRequestDescriptor* descriptor) {
/*************************************************************************/
   //std::cout << "RobinDescriptorChannel::descriptorRequestFragment  chann " << std::hex << m_id << "  --  " << std::dec; descriptor->dump();
   m_rol.requestFragment(descriptor,m_id);
}



/*************************************************/
int RobinDescriptorChannel::requestFragment(int level1Id) {
/*************************************************/
   DEBUG_TEXT(DFDB_ROSFM, 10, "RobinDescriptorChannel::requestFragment (ROL " << m_id << "): Entering with L1ID = " << level1Id);
   return (0);
}


/*******************************************************/
EventFragment * RobinDescriptorChannel::getFragment(int /*ticket*/) {
/*******************************************************/
   return(0);
}


/*****************************************************************/
void RobinDescriptorChannel::gotFragment(RequestDescriptor* descriptor) {
/*****************************************************************/
   DataRequestDescriptor* dd=dynamic_cast<DataRequestDescriptor*> (descriptor);
   if (dd) {
      //std::cout << "chann " << std::hex << m_id << std::dec << "  " ;dd->dump();
      unsigned int* data=dd->dataPage(m_id)->virtualAddress();
      const eformat::ROBFragment<const unsigned int*> fragment(data);
      unsigned int l1Id = fragment.rod_lvl1_id(); 
      if (l1Id != dd->level1Id()) {
         CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_ILLL1ID,
                              "ERROR in RobinDescriptorChannel::getFragment(ROL " << m_id
                              << "): Wrong L1ID 0x"
                              << HEX(l1Id) << " received but 0x"
                              << HEX(dd->level1Id()) << " expected.");
         ers::error(ex1);
      }
      const unsigned int* status;
      fragment.status(status);
      if ((status[0] & 0xf0000000) == enum_fragStatusLost) {
         m_statistics.numberOfNeverToCome++;
         u_int sid = fragment.rob_source_id();
         CREATE_ROS_EXCEPTION(ex10, ModulesException, PCIROBIN_ROBSW,
                              "RobinDescriptorChannel: Lost fragment detected. The L1ID is 0x"
                              << HEX(l1Id) << ". The ROB Source ID is 0x" << HEX(sid));
         ers::warning(ex10);
      }
      if ((status[0] & 0xf0000000) == enum_fragStatusPending) {
         m_statistics.numberOfMayCome++;
         dd->status(m_id,status[0]);
         //std::cout  << "chann " << std::hex << m_id << std::dec << "may come fragment " << l1Id << std::endl;
      }

      if (m_crcInterval && m_fragCount++%m_crcInterval==0) {
         if(!fragment.checksum()) {
            CREATE_ROS_EXCEPTION(chkExc, ROSRobinExceptions, BADCRC, " L1 id " << l1Id << ", rolId " << std::hex << m_id);
            ers::warning(chkExc);
         }
#if 0
         else {
            std::cout << "l1 id " << l1Id << " CRC check OK\n";
         }
#endif
      }

      //Update the error array
      for (u_int bit = 0; bit < 32; bit++) {
         if (status[0] & (1 << bit)) {
            m_statistics.statusErrors[bit]++;
         }
      }
   }
   else {
      ClearRequestDescriptor* cd=dynamic_cast<ClearRequestDescriptor*> (descriptor);
      if (cd) {
         //std::cout << "chann " << std::hex << m_id << std::dec << "  " ;cd->dump();
         unsigned int* data=cd->dataPage(m_id)->virtualAddress();

         if ((data[3] & 0xff)!=enum_respClearAck) {
            std::cout << "Unexpected response code\n";
         }

         if (data[5]) {
#if DEBUG_LEVEL>0
            if ((DF::GlobalDebugSettings::packageId()==DFDB_ROSFM) || (DF::GlobalDebugSettings::packageId()==0)) {
               if (DF::GlobalDebugSettings::traceLevel() >= 5) {

                  std::cout << "Undeleted fragments from rol 0x" << std::hex << m_id;
                  for (unsigned int offset=0;offset<data[5]; offset++) {
                     std::cout << "  " << data[6+offset];
                  }
                  std::cout << std::dec << std::endl;
               }
            }
#endif
            if (data[5]>1) {
               CREATE_ROS_EXCEPTION(clrExc1, ROSRobinExceptions, BADDELETE,"Failed to delete "
                                    << data[5] << " fragments from rol 0x" << std::hex << m_id 
                                    << ", ids 0x" << data[6] << "..0x" << data[5+data[5]]);
               ers::warning(clrExc1);
            }
            else {
               CREATE_ROS_EXCEPTION(clrExc2, ROSRobinExceptions, BADDELETE,"Failed to delete "
                                    << data[5] << " fragment from rol 0x" << std::hex << m_id 
                                    << ", id 0x" << data[6]);
               ers::warning(clrExc2);
            }

            m_statistics.numberOfNotDeleted+=data[5];

            bool match=false;
            unsigned int* levelIdList=cd->level1Ids();
            for (unsigned int index=0; index<cd->nLevel1Ids();index++) {
               if (data[6]==levelIdList[index]) {
                  match=true;
               }
            }
            if (!match) {
               std::cout << "WTF? " << data[6] << " is not in list (" << levelIdList[0] << ".." << levelIdList[cd->nLevel1Ids()-1] << ")\n";
            }
         }
         
      }
   }
   if (m_ackQueue) {
      m_ackQueue->fastPush(descriptor);
   }
}


/**************************************************************************/
void RobinDescriptorChannel::descriptorReleaseFragment(ClearRequestDescriptor* descriptor) {
/**************************************************************************/
   m_rol.releaseFragment(descriptor, m_id);
}


/**************************************************************************/
void RobinDescriptorChannel::releaseFragment(const std::vector <u_int>* /*level1Ids*/) {
/**************************************************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::releaseFragment(1): Called");

   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::releaseFragment(1): Done ");
}


/*****************************************************************************/
void RobinDescriptorChannel::releaseFragmentAll(const std::vector <u_int> *level1Ids) {
/*****************************************************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::releaseFragmentAll: Called");
  
   std::vector<u_int>::const_iterator first = level1Ids->begin();
   DEBUG_TEXT(DFDB_ROSFM, 10, "RobinDescriptorChannel::releaseFragmentAll(ROL " << m_id << "): first L1ID = " << *first);

   m_statistics.numberOfNotDeleted += m_rol.releaseFragmentAll(level1Ids);  //Blocking
   m_statistics.lastmsg = RELEASEFRAGMENT;
   m_statistics.lastl1id = *first;
   
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::releaseFragmentAll: Done");
}


/**************************************************/
void RobinDescriptorChannel::releaseFragment(int level1Id) {
/**************************************************/
   std::vector<u_int> level1Ids;

   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::releaseFragment(2): Called ");

   level1Ids.push_back((u_int)level1Id);  
   releaseFragment(&level1Ids);
   m_statistics.lastmsg = RELEASEFRAGMENT;
   m_statistics.lastl1id = level1Id;
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::releaseFragment(2): Done ");
}


/**************************************************/    
int RobinDescriptorChannel::collectGarbage(u_int oldestId) {
/**************************************************/    
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::collectGarbage: Called for oldestId = " << oldestId);
   Rol::GCBlock res = m_rol.collectGarbage(oldestId);
   m_statistics.lastmsg = COLLECTGARBAGE;
   m_statistics.lastl1id = oldestId;
  
   DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDescriptorChannel::collectGarbage: res.done    = " << res.done);
   DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDescriptorChannel::collectGarbage: res.deleted = " << res.deleted);
   DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDescriptorChannel::collectGarbage: res.free    = " << res.free);

   if (res.done) {
      m_statistics.gcOK++;
      m_gcdeleted += res.deleted;
   }  
   else {
      m_statistics.gcBad++;
   }

   m_gcfree += res.free;
   m_statistics.gcAvDeleted = m_gcdeleted / (m_statistics.gcOK + m_statistics.gcBad);
   m_statistics.gcAvFree = m_gcfree / (m_statistics.gcOK + m_statistics.gcBad);
    
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::collectGarbage: Done ");
   return(res.done);
}


/****************************************/
void RobinDescriptorChannel::prepareForRun(void) {
/****************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::prepareForRun: Called");
  
   if (!m_l1id_modulo) {   //if mode = Rolemu the link must not be enabled
      m_rol.reset();
      m_rol.setConfig(enum_cfgRolEnabled, 1);
      m_rol.clearStatistics();
   }
  
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::prepareForRun: S-Link enabled");
}


/*********************************/
void RobinDescriptorChannel::stopFE(void) {
/*********************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::stopFE: Called");
   m_rol.setConfig(enum_cfgRolEnabled, 0);
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::stopFE: S-Link disabled");
}


/****************************/
void RobinDescriptorChannel::probe() {
/****************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::probe: Entered");
  
   StatisticsBlock statistics = m_rol.getStatistics();

   //The Robin specific parameters should only be processed once
   if (m_infoflag) {
      //Check for over temperature
      if (statistics.tempOk == 0) {
         DEBUG_TEXT(DFDB_ROSFM, 5, "RobinDescriptorChannel::probe: Robin is too hot");
         CREATE_ROS_EXCEPTION(ex11, ModulesException, PCIROBIN_OVTMP, "RobinDescriptorChannel: Over temperature flag set on Robin " << m_RobinphysicalAddress);
         ers::warning(ex11);
      } 
   }
  
   // This is just a selection of the most interesting parameters
   //MJ: 64-BIT PARAMETERS HAVE TO BE WORD-SWAPPED!!!!!!!!!!!!!!!!!!!!!!!!!!
   m_statistics.pagesFree      = statistics.pagesFree;    
   m_statistics.pagesInUse     = statistics.pagesInUse;    
   m_statistics.mostRecentId   = statistics.mostRecentId;    
   m_statistics.rolXoffStat    = statistics.rolXoffStat;    
   m_statistics.rolDownStat    = statistics.rolDownStat;    
   m_statistics.bufferFull     = statistics.bufferFull;  
   m_statistics.numberOfXoffs = statistics.errors[enum_errXoff];

   //Create some 64-bit counters locally. The Robin cannot do it yet (fast enough)
   if (m_rdw1 > statistics.fragStat[enum_fragsReceived]){
      m_hdw1++;
   }
   m_rdw1 = statistics.fragStat[enum_fragsReceived];
   m_statistics.fragsreceived = ((u_int64_t)m_hdw1 << 32) | (u_int64_t)statistics.fragStat[enum_fragsReceived];
  
   if (m_rdw2 > statistics.fragStat[enum_fragsTruncated]){
      m_hdw2++;
   }
   m_rdw2 = statistics.fragStat[enum_fragsTruncated];
   m_statistics.fragstruncated = ((u_int64_t)m_hdw2 << 32) | (u_int64_t)statistics.fragStat[enum_fragsTruncated];
  
   if (m_rdw3 > statistics.fragStat[enum_fragsCorrupted]){
      m_hdw3++;
   }
   m_rdw3 = statistics.fragStat[enum_fragsCorrupted];
   m_statistics.fragscorrupted = ((u_int64_t)m_hdw3 << 32) | (u_int64_t)statistics.fragStat[enum_fragsCorrupted];

   if (m_rdw4 > statistics.fragStat[enum_fragsReplaced]) {
      m_hdw4++;
   }
   m_rdw4 = statistics.fragStat[enum_fragsReplaced];
   m_statistics.fragsreplaced = ((u_int64_t)m_hdw4 << 32) | (u_int64_t)statistics.fragStat[enum_fragsReplaced];
  
   // level1 rate (for this channel)
   tstamp tsStop;
   ts_clock(&tsStop);

   u_int64_t numberOfLevel1Delta;
   if (m_numberOfLevel1Last == 0xffffffffffffffffLL) {
      numberOfLevel1Delta = m_statistics.fragsreceived;
   }
   else {
      numberOfLevel1Delta = m_statistics.fragsreceived - m_numberOfLevel1Last;
   }

   m_statistics.level1RateHz = (u_int)(numberOfLevel1Delta / (float)ts_duration(m_tsStopLast, tsStop));
   m_tsStopLast = tsStop;
  
   m_numberOfLevel1Last = m_statistics.fragsreceived;
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::probe: Done");
}



/********************************/
void RobinDescriptorChannel::clearInfo() {
/********************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::clearInfo: Entered");
  
   for (u_int bit = 0; bit < 32; bit++) {
      m_statistics.statusErrors[bit] = 0;
   }

   m_statistics.numberOfMayCome     = 0;
   m_statistics.numberOfNeverToCome = 0;
   m_statistics.numberOfNotDeleted  = 0;
   m_statistics.gcOK                = 0;
   m_statistics.gcBad               = 0;
   m_statistics.gcAvDeleted         = 0;
   m_statistics.gcAvFree            = 0;
   m_statistics.pagesFree           = 0;
   m_statistics.pagesInUse          = 0;
   m_statistics.mostRecentId        = 0;
   m_statistics.rolXoffStat         = 0;
   m_statistics.rolDownStat         = 0;
   m_statistics.bufferFull          = 0;
   m_statistics.fragsreceived       = 0;
   m_statistics.fragsreplaced       = 0;
   m_statistics.fragstruncated      = 0;
   m_statistics.fragscorrupted      = 0;
   m_statistics.level1RateHz        = 0;
   m_statistics.lastmsg             = 0;
   m_statistics.lastl1id            = 0;
   m_statistics.numberOfXoffs       = 0;

   m_rdw1 = 0;
   m_rdw2 = 0;
   m_rdw3 = 0;
   m_rdw4 = 0;
   m_hdw1 = 0;
   m_hdw2 = 0;
   m_hdw3 = 0;
   m_hdw4 = 0;

   ts_clock(&m_tsStopLast);
   m_numberOfLevel1Last = 0xffffffffffffffffLL;

   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::clearInfo: Done");
}


/*****************************/
void RobinDescriptorChannel::enable(){
   /*****************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::enable: Called ");
   m_rol.configureDiscardMode(1);
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::enable: Done ");
}


/******************************/
void RobinDescriptorChannel::disable() {
/******************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::disable: Called ");
   m_rol.configureDiscardMode(0);
   DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDescriptorChannel::disable: Done ");
}



 
