// $Id: EmulatedDescriptorModule.cpp 91605 2010-05-03 15:01:26Z joos $
/*
  ATLAS ROS Software

  Class: EmulatedDescriptorModule
  Authors: ATLAS ROS group 	
*/

#include <chrono>

#include "EmulatedDescriptorModule.h"

#include "ROSDescriptor/DataRequestDescriptor.h"

#include "ROSEventFragment/ROBFragment.h"

#include <stdint.h>
#include <string>

#include "rcc_time_stamp/tstamp.h"
#include "DFDebug/DFDebug.h"


using namespace ROS;


/********************************************/
EmulatedDescriptorModule::EmulatedDescriptorModule()
/********************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, " EmulatedDescriptorModule::constructor: Entered (empty function)");
}


/*********************************************/
EmulatedDescriptorModule::~EmulatedDescriptorModule() noexcept
/*********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDescriptorModule::destructor entered");

  for (auto page : m_pages) {
     delete [] page;
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDescriptorModule::destructor completed");
}


/***********************************************************************/
void EmulatedDescriptorModule::setup(DFCountedPointer<Config> configuration) {
/***********************************************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDescriptorModule::setup: called");
   m_configuration = configuration;

   DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDescriptorModule::setup: done");
}


/*****************************************/
void EmulatedDescriptorModule::configure(const daq::rc::TransitionCmd&) {
/*****************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDescriptorModule::configure: called");

   m_getDelay               = m_configuration->getInt("GetFragmentDelay");


   m_getMiss                = m_configuration->getFloat("GetMissingFragment");


   m_pageSize=m_configuration->getInt("fragmentSize");
   if (m_pageSize<25) {
      m_pageSize=72;
   }

   std::string dataSizeEnum=m_configuration->getString("DataSize");
   m_randomSize=(dataSizeEnum=="RANDOM");
   m_detectorEventType      = m_configuration->getInt("DetEvType");
   m_numberOfStatusElements = m_configuration->getInt("NumberOfStatusElements");
   m_maxDataElements=m_pageSize
//     -((sizeof(ROBFragment::ROBHeader)/sizeof(u_int))-ROBFragment::s_nStatusElements+m_numberOfStatusElements)
      -(sizeof(RODFragment::RODHeader)+sizeof(RODFragment::RODTrailer))/sizeof(unsigned int);
   m_statusBlockPosition    = m_configuration->getInt("StatusBlockPosition");

   unsigned int numberOfDataChannels = m_configuration->getInt("numberOfChannels");
   for (unsigned int channelNumber = 0; channelNumber < numberOfDataChannels; channelNumber++) {
      unsigned int id=m_configuration->getInt("channelId", channelNumber);
      m_channelIds.push_back(id);
   }

   m_messageQueue=new ObjectQueue<Message>(8000, "EmulatedDescriptorChannel message");

   m_maxL1=m_configuration->getUInt("MaxL1");
   m_storeSize=m_configuration->getUInt("StoreSize");
   if (m_storeSize>m_maxL1) {
      m_storeSize=m_maxL1;
   }
   for (m_lastl1=0; m_lastl1<m_storeSize; m_lastl1++) {
      m_fragmentSet.insert(m_lastl1);
   }
   m_lastl1--;
   m_notCleared=0;
   m_wrapCount=0;

   tstamp now;
   ts_clock(&now);
   uint64_t timeNow=now.high;
   m_freeTime=(timeNow<<32)+now.low;

   DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDescriptorModule::configure: done");
}

/***************************************************************************/
void EmulatedDescriptorModule::registerChannels(RequestDescriptor* descr) {
/***************************************************************************/
   //std::cout << "EmulatedDescriptorModule::registerChannels() Registering " << m_channelIds.size() << " channels\n";
   unsigned int headerSize=
      (sizeof(ROBFragment::ROBHeader)/sizeof(u_int))
      -ROBFragment::s_nStatusElements+m_numberOfStatusElements;

   for (auto id : m_channelIds) {
      unsigned int* page=new unsigned int[m_pageSize];
      m_pages.push_back(page);
      unsigned int index=descr->addChannel(id, page, 0, headerSize);

      ROBFragment::ROBHeader* robHeader=(ROBFragment::ROBHeader*)descr->headerPage(index)->data();

      robHeader->generic.startOfHeaderMarker=EventFragment::s_robMarker;
      robHeader->generic.headerSize=headerSize;
      robHeader->generic.sourceIdentifier=id;
      robHeader->generic.formatVersionNumber=EventFragment::s_formatVersionNumber;
      robHeader->generic.numberOfStatusElements=m_numberOfStatusElements;
   }
}

void EmulatedDescriptorModule::prepareForRun(const daq::rc::TransitionCmd&) {
#ifndef TBB_QUEUE
   m_messageQueue->open();
#endif
   m_thread=new std::thread(&EmulatedDescriptorModule::run,this);
}

void EmulatedDescriptorModule::stopGathering(const daq::rc::TransitionCmd&) {
   m_active=false;
#ifdef TBB_QUEUE
   m_messageQueue.abort();
#else
   m_messageQueue->close();
#endif
   std::cout << "EmulatedDescriptorModule::stopGathering() Waiting for thread to exit\n";
   m_thread->join();
}

/*****************************************/
void EmulatedDescriptorModule::unconfigure(const daq::rc::TransitionCmd&) {
/*****************************************/
   delete m_thread;
   m_thread=0;
#ifndef TBB_QUEUE
   delete m_messageQueue;
   m_messageQueue=0;
#endif
}


/*****************************************/
void EmulatedDescriptorModule::clearInfo(void)
/*****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDescriptorModule::clearInfo: called");
}


// /*********************************************************************/
const std::vector<DataChannel *> *EmulatedDescriptorModule::channels(void) {
// /*********************************************************************/

   DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDescriptorModule::channels: called / done");
//   return &m_dataChannels;
   return 0;
}




/**************************************************************************************/
void EmulatedDescriptorModule::channelRequestFragment(unsigned int id,
                                                      DataRequestDescriptor* descriptor,
                                                      bool last) {
/**************************************************************************************/

   //std::cout << "EmulatedDescriptorModule::channelRequestFragment() requesting data from channel " << id << std::endl;
   tstamp currentTime;
   ts_clock(&currentTime);
   uint64_t now=currentTime.high;
   now=(now<<32)+currentTime.low;

   if (m_freeTime < now) {
      m_freeTime=now;
   }

   m_freeTime+=m_getDelay;

   unsigned int index=descriptor->getChannelIndex(id);
   ROBFragment::ROBHeader* robHeader=
      (ROBFragment::ROBHeader*)descriptor->headerPage(index)->data();

   unsigned int* data=descriptor->dataPage(index)->virtualAddress();

   RODFragment::RODHeader* rodHeader=(RODFragment::RODHeader*) &data[0];
   rodHeader->startOfHeaderMarker=EventFragment::s_rodMarker;
   rodHeader->headerSize=sizeof(RODFragment::RODHeader) / sizeof (u_int);
   rodHeader->formatVersionNumber=0x03010000;
   rodHeader->sourceIdentifier=id;
   rodHeader->level1Id=descriptor->level1Id();
   rodHeader->detectorEventType=m_detectorEventType;

   unsigned int numberOfDataElements;
   if (m_fragmentSet.find(descriptor->level1Id())!=m_fragmentSet.end()) {
      if (m_randomSize) {
         numberOfDataElements=rand()%m_maxDataElements;
      }
      else {
         numberOfDataElements=m_maxDataElements;
      }
      for (unsigned int word=0;word<m_numberOfStatusElements;word++) {
         robHeader->statusElement[word]=0;
      }
   }
   else {
      numberOfDataElements=0;
      robHeader->statusElement[0]=0x40000000;
      if (m_numberOfStatusElements>1) {
         robHeader->statusElement[1]=m_lastl1;
      }
      descriptor->status(index,robHeader->statusElement[0]);
   }

   RODFragment::RODTrailer* rodTrailer=
      (RODFragment::RODTrailer*) &data[rodHeader->headerSize+numberOfDataElements];
   rodTrailer->numberOfStatusElements=0;
   rodTrailer->numberOfDataElements=numberOfDataElements;
   rodTrailer->statusBlockPosition=0;

   robHeader->generic.totalFragmentsize
      =robHeader->generic.headerSize+rodHeader->headerSize+numberOfDataElements+3;

   if (last) {
      //std::cout << "EmulatedDescriptorModule::ChannelRequestfragment() queuing request with EmulatedDescriptorReadoutModule  descriptor->dataPage(id)->virtualAddress()="<<std::hex << descriptor->dataPage(id)->virtualAddress() <<std::dec <<"\n";
      queueRequest(descriptor,m_getDelay, index);
   }
}


void EmulatedDescriptorModule::dump() {
   int index=0;
   std::cout << "EmulatedDescriptorModule::  Fragment buffer:" << std::hex;
   for (auto frag : m_fragmentSet) {
      std::cout << " " << frag;
      index++;
      if (index%16==0) {
         std::cout << "\n\t\t";
      }
   }
   std::cout << std::dec << std::endl;
}
void EmulatedDescriptorModule::user(const daq::rc::UserCmd& command) {
	if (command.commandName()=="dump") {
//      std::vector<std::string> arguments=command.commandParameters();
//      for (auto argv : arguments) {
//          if (argv=="descriptors") {
//          }
//      }
      dump();
   }
}

void EmulatedDescriptorModule::requestFragment(DataRequestDescriptor* descriptor) {
//    std::cout << "EmulatedDescriptorModule::requestFragment(): ";
   m_lastRequestedId=descriptor->level1Id();

   auto list=descriptor->channelList();
   if (list->size() !=0) {
      for (unsigned int index=0;index<list->size(); index++) {
         bool last=false;
         if (index==list->size()-1) {
            last=true;
         }
         channelRequestFragment((*list)[index],descriptor,last);
      }
   }
   descriptor->expectReply(1);
}

void EmulatedDescriptorModule::clearFragments(std::vector<uint32_t>& level1Ids){
   bool error=false;
   unsigned int last=m_lastl1;
   for (auto l1 : level1Ids) {
      l1=l1&0x0fffffff;
      auto iter=m_fragmentSet.find(l1);
      if (iter!=m_fragmentSet.end()) {
         m_fragmentSet.erase(iter);
         if (m_lastl1<m_maxL1) {
            m_lastl1++;
         }
         else {
            m_lastl1=0;
            m_wrapCount++;
         }
         m_fragmentSet.insert(m_lastl1);
      }
      else {
         if (!error) {
            std::cout << "Failed to delete id" << std::hex;
            error=true;
         }
         std::cout << " " << l1;
         m_notCleared++;
      }
   }
   if (error) {
//      m_active=false;
      std::cout << ", last l1 on entry=" << last
                << ", now=" << m_lastl1 << ", last requested=" << m_lastRequestedId
                << std::dec << ", wraps=" << m_wrapCount << std::endl;
      std::cout << "level1Ids vector:" << std::hex;
      for (auto l1 : level1Ids) {
         std::cout << " " << l1;
      }
      std::cout << std::dec << std::endl;
      dump();
   }
}

void EmulatedDescriptorModule::queueRequest(DataRequestDescriptor* request, unsigned int requestDelay, 
//DescriptorDataChannel* chan) {
                                            unsigned int chanId) {
   tstamp now;
   ts_clock(&now);
   uint64_t timeNow=now.high;
   timeNow=(timeNow<<32)+now.low;
   if (timeNow<m_freeTime) {
      m_freeTime+=requestDelay;
   }
   else {
      m_freeTime=timeNow+requestDelay;
   }

   Message* message=new Message(request, chanId, m_freeTime);
#ifdef TBB_QUEUE
   m_messageQueue.push(message);
#else
   m_messageQueue->push(message);
#endif
}

void EmulatedDescriptorModule::gotFragment(unsigned int channelIndex, DataRequestDescriptor* descriptor){
   descriptor->gotData(channelIndex);
   ROBFragment::ROBHeader* robHeader=
      (ROBFragment::ROBHeader*)descriptor->headerPage(channelIndex)->data();
   descriptor->status(channelIndex,robHeader->statusElement[0]);
}

void   EmulatedDescriptorModule::run() {
   m_active=true;
   float meanYield=0;
   unsigned int nRequests=0;
   while (m_active) {

#ifdef TBB_QUEUE
      try {
         Message* message;
         m_messageQueue.pop(message);
#else
      Message* message=m_messageQueue->fastPop(true);
#endif
      if (message) {
//          std::cout << "EmulatedDescriptorModule::ModuleThread::run() dequeued message for time "
//                    << message->completeTime();
         uint64_t timeNow;
         float nyield=0;
         do {
            tstamp now;
            ts_clock(&now);
            timeNow=now.high;
            timeNow=(timeNow<<32)+now.low;
//             std::cout << ", time now " << timeNow;

            if  (timeNow<message->completeTime()){
//                std::cout << ", remaining=" << message->completeTime()-timeNow;
               nyield++;
//               yieldOrCancel();
               std::this_thread::sleep_for(std::chrono::nanoseconds(1));
            }
         } while (timeNow<message->completeTime());
//          std::cout << std::endl;

         nRequests++;
         meanYield+=nyield/nRequests;
         gotFragment(message->channel(),message->descriptor());
         if (m_collectorQueue!=0) {
//             std::cout << "EmulatedDescriptorModule::ModuleThread::run() pushing descriptor to collector\n";
            m_collectorQueue->push(message->descriptor());
         }
//          else {
//             std::cout << "EmulatedDescriptorModule::ModuleThread::run() No collector queue\n";
//          }
         delete message;
      }
#ifdef TBB_QUEUE
      }
      catch (tbb::user_abort& abortException) {
         std::cout << "EmulatedDescriptorModule run aborting...\n";
         m_active=false;
      }
#endif
   }
   std::cout << "EmulatedDescriptorModule handled " << nRequests << " requests, average yields/request=" << meanYield << std::endl;
}

//FOR THE PLUGIN FACTORY
extern "C" {
   extern ReadoutModule* createEmulatedDescriptorModule();
}
ReadoutModule* createEmulatedDescriptorModule(){
   return (new EmulatedDescriptorModule());
}
