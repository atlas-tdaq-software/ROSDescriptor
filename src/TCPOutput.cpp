//
// //////////////////////////////////////////////////////////////////////
#include <iostream>
#include <fstream>
#include <iomanip>

#include "TCPOutput.h"
#include "ROSDescriptor/DataRequestDescriptor.h"
#include "DFSubSystemItem/Config.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSIO/IOException.h"
#include "rcc_time_stamp/tstamp.h"

using namespace ROS;


/**********************/
TCPOutput::TCPOutput() {
/**********************/
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::constructor:  called");
  struct timeval tout = TCP_WRTIMEOUT;

  m_stats.dataOutput=0.0;
  m_stats.fragmentsOutput=0;
  m_stats.timeouts = 0;

  m_port = 0;

  bcopy (&tout, &m_wrtimeout, sizeof (struct timeval));
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::constructor:  done");
}


/***********************/
TCPOutput::~TCPOutput() noexcept
/***********************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::destructor:  called");
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::destructor:  done");
}


/********************************************************/
int TCPOutput::TCPSafeSend (const void* data, int length, int socketNumber)
/********************************************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::TCPSafeSend:  called");
  int data_sent = 0;

  while (data_sent < length)
  {
    int result = ::send (m_socket[socketNumber], data, length, 0);
    if (result < 0 && errno != EINTR)
    {
     m_errno = errno;
     DEBUG_TEXT(DFDB_ROSIO, 5, "TCPOutput::TCPSafeSend:  Error in ::send (errno = " << errno << ")");
     return result;
    }
    else
      data_sent += result;
  }

  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::TCPSafeSend:  done");
  return data_sent;
}

/********************************************************************/
void TCPOutput::sendData(const Buffer* buffer, NodeID /*destination*/,
		 	  unsigned int /*transactionId*/,
                          unsigned int /*fragStatus*/)
/********************************************************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::sendData:  called");

  if (m_samplingGap && m_fragmentsInput%m_samplingGap == 0) {
     // Choose destination socket
     int socketNumber=m_stats.fragmentsOutput%m_socket.size();

    int nsel;
    fd_set fdset;
    struct timeval timeout;

    FD_ZERO (&fdset);
    FD_SET  (m_socket[socketNumber], &fdset);
    bcopy (&m_wrtimeout, &timeout, sizeof (struct timeval));

    if ((nsel = select (m_socket[socketNumber] + 1, 0, &fdset, 0, &timeout)) <= 0)
    {
      ++m_stats.timeouts;

      if (m_throwIfUnavailable)
        return;

      FD_SET  (m_socket[socketNumber], &fdset);
      bcopy (&m_wrtimeout, &timeout, sizeof (struct timeval));

      while ((nsel = select (m_socket[socketNumber] + 1, 0, &fdset, 0, &timeout)) <= 0)
      {
	++m_stats.timeouts;
        FD_ZERO (&fdset);
        FD_SET  (m_socket[socketNumber], &fdset);
	bcopy (&m_wrtimeout, &timeout, sizeof (struct timeval));
      }
    }

    unsigned int bytes = buffer->size();
    unsigned int n_size = htonl (bytes);

    DEBUG_TEXT(DFDB_ROSIO, 20, "TCPOutput::sendData:  The ROS fragment has a size of " << bytes << " bytes");

    if (TCPSafeSend ((const char *) &n_size, (int) sizeof (int), socketNumber) != (int) sizeof (int))
    {
      CREATE_ROS_EXCEPTION( tException, IOException, IOException::TCP_SEND_ERROR,
                            "a call to send returned " << m_errno);
      throw tException;
    }

    for (Buffer::page_iterator page=buffer->begin(); page!=buffer->end(); page++) 
    {
      register int s = (*page)->usedSize();

      if (TCPSafeSend ((const char *) (*page)->address(), s, socketNumber) != s)
      {
         CREATE_ROS_EXCEPTION( tException, IOException, IOException::TCP_SEND_ERROR,
                            "a call to send returned " << m_errno);
         throw tException;
      }
    }
    m_stats.fragmentsOutput++;
    m_stats.dataOutput+=bytes/(float)(1024*1024);
  }

  m_fragmentsInput++;
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::sendData:  done");
}



/********************************************************************/
void TCPOutput::send(DataRequestDescriptor* descriptor) {
/********************************************************************/
   DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::send:  called");

   if (m_samplingGap && m_fragmentsInput%m_samplingGap == 0) {
      // Choose destination socket
      int socketNumber=m_stats.fragmentsOutput%m_socket.size();

      int nsel;
      fd_set fdset;
      struct timeval timeout;

      FD_ZERO (&fdset);
      FD_SET  (m_socket[socketNumber], &fdset);
      bcopy (&m_wrtimeout, &timeout, sizeof (struct timeval));

      if ((nsel=select (m_socket[socketNumber]+1, 0, &fdset, 0, &timeout)) <= 0) {
         ++m_stats.timeouts;
         if (m_throwIfUnavailable) {
            return;
         }

         FD_SET  (m_socket[socketNumber], &fdset);
         bcopy (&m_wrtimeout, &timeout, sizeof (struct timeval));

         while ((nsel=select (m_socket[socketNumber]+1, 0, &fdset, 0, &timeout)) <= 0) {
            ++m_stats.timeouts;
            FD_ZERO (&fdset);
            FD_SET  (m_socket[socketNumber], &fdset);
            bcopy (&m_wrtimeout, &timeout, sizeof (struct timeval));
         }
      }

      unsigned int nPages=descriptor->channelCount()+1;
      struct iovec* iov=new struct iovec[nPages+1];
      unsigned int bytes=descriptor->fragment(iov, true);
      unsigned int nSize=htonl (bytes);

      DEBUG_TEXT(DFDB_ROSIO, 20, "TCPOutput::send:  The fragment has a size of " << bytes << " bytes");
      if (TCPSafeSend ((const char *) &nSize, sizeof(int), socketNumber)!=sizeof(int)) {
         CREATE_ROS_EXCEPTION(tException, IOException, IOException::TCP_SEND_ERROR,
                               "a call to send returned " << m_errno);
         throw tException;
      }

      for (unsigned int page=0; page<nPages; page++) {
         DEBUG_TEXT(DFDB_ROSIO, 20, "Sending page " << page << ", size " << iov[page].iov_len);
         unsigned int sent=TCPSafeSend(iov[page].iov_base, iov[page].iov_len, socketNumber);
         if (sent != iov[page].iov_len) {
            CREATE_ROS_EXCEPTION(tException, IOException, IOException::TCP_SEND_ERROR,
                                 "a call to send returned " << m_errno);
            throw tException;
         }
      }
      m_stats.fragmentsOutput++;
      m_stats.dataOutput+=bytes/(float)(1024*1024);

      delete [] iov;
   }

   m_fragmentsInput++;
   DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::send:  done");
}



/***************************/
void TCPOutput::connect (const daq::rc::TransitionCmd&)
/***************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::connect:  called");
  /* open a socket and connect to the destination node */

  for (std::vector<std::string>::const_iterator destIter=m_destinationNode.begin();
       destIter!=m_destinationNode.end(); destIter++) {

     int option = 1;
     struct sockaddr_in saddr;

     int newSocket;
     if ((newSocket = ::socket (AF_INET, SOCK_STREAM, 0)) < 0)
        {
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_CREATE_ERROR, "");
           throw tException;
        }

     if (setsockopt (newSocket, SOL_SOCKET, SO_KEEPALIVE,
                     &option, sizeof (option)))
        {
           close (newSocket);
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_OPTION_ERROR, "option KEEPALIVE");
           throw tException;
        }

     if (setsockopt (newSocket, SOL_SOCKET, SO_REUSEADDR,
                     &option, sizeof (option)))
        {
           close (newSocket);
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_OPTION_ERROR, "option REUSEADDR");
           throw tException;
        }

     option = m_TCPbufSize;

     if (setsockopt (newSocket, SOL_SOCKET, SO_SNDBUF,
                     &option, sizeof (option)))
        {
           close (newSocket);
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_OPTION_ERROR, "option SNDBUF");
           throw tException;
        }

     if (setsockopt (newSocket, SOL_SOCKET, SO_RCVBUF,
                     &option, sizeof (option)))
        {
           close (newSocket);
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_OPTION_ERROR, "option RCVBUF");
           throw tException;
        }

     option = 0; /* 0 to enable Nagle algorithm */

     if (setsockopt (newSocket,
                     getprotobyname ("tcp")->p_proto,
                     TCP_NODELAY,
                     &option, sizeof (option)))
        {
           close (newSocket);
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_OPTION_ERROR, "option NODELAY");
           throw tException;
        }

     struct hostent *hptr = gethostbyname (destIter->c_str ());
     std::cout << "m_destinationNode=<" << destIter->c_str() << ">\n";
     if (hptr == 0)
        {
           close (newSocket);
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::ADDRESS_TRANSLATION_ERROR, *destIter);
           throw tException;
        }

     saddr.sin_family      = AF_INET;
     saddr.sin_port        = htons (m_port);
     memcpy (&saddr.sin_addr.s_addr, hptr->h_addr, hptr->h_length);
     DEBUG_TEXT(DFDB_ROSIO, 20, "TCPOutput::connect:  m_port = " << m_port);

     if (::connect (newSocket, (struct sockaddr *) &saddr,
                    sizeof (saddr)))
        {
           perror ("connect error");
           close (newSocket);
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::TCP_CONNECT_ERROR, "Cannot connect to "
                                << *destIter << " port " << m_port);
           throw tException;
        }

     std::cout << "TCPOutput : connected to " << *destIter
          << " port " << m_port << std::endl;

     m_socket.push_back(newSocket);
  }
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::connect:  done");
}


/****************************/
void TCPOutput::disconnect(const daq::rc::TransitionCmd&)
/****************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::disconnect:  called");
  /* close TCP connection */


   while (!m_socket.empty()) {
      int socket=m_socket.back();
      m_socket.pop_back();
      close(socket);
   }
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::disconnect:  done");
}


/*******************************/
void TCPOutput::prepareForRun (const daq::rc::TransitionCmd&)
/*******************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::prepareForRun:  called");
  m_stats.dataOutput = 0.0;
  m_stats.fragmentsOutput = 0;
  m_fragmentsInput = 0;
  m_stats.timeouts = 0;
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::prepareForRun:  done");
}


/*************************************************************/
void TCPOutput::setup (DFCountedPointer<Config> configuration)
/*************************************************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::setup:  called");
  m_TCPbufSize         = configuration->getInt ("TCPBufferSize");

  m_destinationNode    = configuration->getVector<std::string> ("DestinationNode");
  m_port = (unsigned short) configuration->getInt ("DestinationPort");

  m_samplingGap        = configuration->getInt ("SamplingGap");
  m_throwIfUnavailable = configuration->getBool ("ThrowIfUnavailable");

  std::cout << "TCPOutput::"
       << " sending data to ";
  for (std::vector<std::string>::const_iterator destIter=m_destinationNode.begin();
       destIter!=m_destinationNode.end(); destIter++) {
     std::cout << *destIter << ",";
  }
  std::cout << " destination port " << m_port
       << " every " << m_samplingGap << " calls to sendData"
       << std::endl;
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPOutput::setup:  done");
}


/********************************************/
ISInfo* TCPOutput::getISInfo()
/********************************************/
{
   return &m_stats;
}


extern "C" 
{
   extern TCPOutput* createTCPOutput();
}

TCPOutput* createTCPOutput()
{
   return (new TCPOutput());
}

