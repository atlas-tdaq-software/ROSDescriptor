#include <chrono>
#include <thread>

#include "ROSDescriptor/DataRequestDescriptor.h"
#include "ROSDescriptor/DescriptorException.h"
#include "ROSDescriptor/DescriptorDataOut.h"
#include "ROSUtilities/ROSErrorReporting.h"

#include "ers/ers.h"


using namespace ROS;


// ************************************************************************
DataRequestDescriptor::DataRequestDescriptor(unsigned int timeout) :
   m_timeout(timeout),
   m_transactionId(0),
   m_level1Id(0) {
// ************************************************************************
   //   m_descriptorId=s_numberOfDescriptors++;

   //   m_ioVec=new struct iovec[index+2];
   for (std::vector<DataChannel*>::iterator chanIter=DataChannel::channels()->begin(); chanIter!=DataChannel::channels()->end(); chanIter++) {
      DescriptorDataChannel* ddChannel=dynamic_cast<DescriptorDataChannel*> (*chanIter);
      if (ddChannel==0) {
         CREATE_ROS_EXCEPTION(badChannelException, DescriptorException, DescriptorException::INCOMPATIBLE_CHANNEL,"");
         ers::fatal(badChannelException);
      }
//      m_dataPage[(*chanIter)->id()]=ddChannel->getPage();
   }
   for (unsigned int index=0;index<sizeof(m_header)/sizeof(unsigned int);index++) {
      m_header[index]=0;
   }
}


// ************************************************************************
DataRequestDescriptor::~DataRequestDescriptor() {
// ************************************************************************
//   delete [] m_ioVec;
   for (auto pIter : m_dataPage) {
      delete pIter;
   }
}



// ************************************************************************
void DataRequestDescriptor::initialise(unsigned int level1Id,
                                       const std::vector<unsigned int>* myChannels,
                                       unsigned int transactionId,
                                       unsigned int sequence, unsigned int tag) {
// ************************************************************************
   RequestDescriptor::initialise(myChannels,sequence);
#if 0
   std::cout << "DataRequestDescriptor::initialise:"
             << " level1Id=" << level1Id
             << " sequence=" << sequence
             <<std::endl;
#endif
   for (auto chanIter : *myChannels) {
      m_responseReceived[chanIter]=false;
      m_status[chanIter]=0;
   }

   m_level1Id=level1Id;
   m_transactionId=transactionId;
   m_nRepliesReceived=0;
   m_terminated=false;
   m_tag=tag;
   m_submitted=false;
   m_repliesExpected=0;
}

bool DataRequestDescriptor::complete() {
   while (!m_submitted) {
      std::this_thread::sleep_for(std::chrono::nanoseconds(1));
   }

   return (m_nRepliesReceived==m_repliesExpected);
}


// ************************************************************************
void DataRequestDescriptor::retry() {
// ************************************************************************
   m_nRepliesReceived=0;
   std::vector<unsigned int>::iterator chanStartIter=channelList()->begin();
   std::vector<unsigned int>::iterator chanEndIter=channelList()->end();
   for (std::vector<unsigned int>::iterator chanIter=chanStartIter; chanIter!=chanEndIter; chanIter++) {
      if (m_status[(*chanIter)]!=0) {
         m_responseReceived[(*chanIter)]=false;
         m_status[(*chanIter)]=0;
      }
      else {
         m_nRepliesReceived++;
      }
   }
   submit();
}

// ************************************************************************
void DataRequestDescriptor::submit() {
// ************************************************************************
   m_tries++;
   std::vector<unsigned int>::iterator chanStartIter;
   std::vector<unsigned int>::iterator chanEndIter;
   if (m_channelList.size()) {
      chanStartIter=m_channelList.begin();
      chanEndIter=m_channelList.end();
   }
   else {
      chanStartIter=m_allChannelList.begin();
      chanEndIter=m_allChannelList.end();
   }
   for (std::vector<unsigned int>::iterator chanIter=chanStartIter; chanIter!=chanEndIter; chanIter++) {
      if (!m_responseReceived[(*chanIter)]) {
         m_channels[(*chanIter)]->descriptorRequestFragment(this);
      }
   }
}

// ************************************************************************
bool DataRequestDescriptor::channelsPending() {
// ************************************************************************
   std::vector<unsigned int>::iterator chanStartIter;
   std::vector<unsigned int>::iterator chanEndIter;

   for (auto chanIter : m_localList) {
      if ((m_fragmentStatus[chanIter]&0x40000000) != 0) {
         return true;
      }
   }
   return false;
}



// ************************************************************************
bool DataRequestDescriptor::timedOut() {
// ************************************************************************
   auto elapsed=std::chrono::system_clock::now()-m_initialiseTime;
   uint32_t usec=std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count();
   if (m_timeout && usec > m_timeout) {
#if 0
      std::cout << "DataRequestDescriptor l1 ID " << m_level1Id
                << " timedOut,  timeout=" << m_timeout
                << ",  epalsed=" << usec
                << std::endl;
#endif
      return true;
   }
   else {
      return false;
   }
}



// ************************************************************************
void DataRequestDescriptor::dump() {
// ************************************************************************
   std::vector<unsigned int>*  channels=channelList();
   std::cout << "DataRequestDescriptor:: level1Id=" << m_level1Id
             << ", sequence=" << m_sequenceNumber
             << ", tries=" << m_tries
             << ", transactionId=" << m_transactionId
             << ", parts received=" << m_nRepliesReceived
             << ", parts required=" << m_nChannels
             << ", channel list size=" << channels->size()
             << ", channel ids=";
   std::vector<unsigned int>::iterator chanStartIter=channels->begin();
   std::vector<unsigned int>::iterator chanEndIter=channels->end();
   for (std::vector<unsigned int>::iterator chanIter=chanStartIter; chanIter!=chanEndIter; chanIter++) {
      std::cout << (*chanIter) << " (0x" << std::hex << (*chanIter) << ") " <<std::dec;
   }
   std::cout << std::endl;

   std::cout  << std::hex << std::setfill('0');
   for (std::vector<unsigned int>::iterator chanIter=chanStartIter; chanIter!=chanEndIter; chanIter++) {
      std::cout << "Channel " << (*chanIter) << ", Status="  << std::setw(8) << m_status[(*chanIter)]
                << ", Data ...";
      unsigned int* tPtr=m_dataPage[(*chanIter)]->virtualAddress();
      for (unsigned int word=0;word<16; word++) {
         std::cout << " " << std::setw(8) << tPtr[word];
      }
      std::cout << "...\n";
   }

   std::cout << std::dec << std::setfill(' ');
}



// ************************************************************************
unsigned int DataRequestDescriptor::fragment(struct iovec* ioVec, bool addHeader) {
// ************************************************************************
   int index=0;
   int messageSize=0;
   if (addHeader) {
      m_header[0]=0xaa1234aa;
      m_header[1]=0;  // Total fragment size

      m_header[2]=sizeof(m_header)/sizeof(unsigned int); // Total header size
      m_header[3]=0x04000000;  // Format version number

//      m_header[4]=??   // Source Id
      m_header[5]=1;     // Number of status elements
      m_header[6]=0;     // Status word 0
      m_header[7]=0;     // CRC type none
      
      m_header[10]=level1Id();
      m_header[11]=0x0f;
      m_header[13]=level1Id();

      ioVec[index].iov_len=sizeof(m_header);
      ioVec[index].iov_base=&m_header;
      messageSize+= ioVec[index].iov_len ;
      index++;
   }

   std::vector<unsigned int>::iterator chanStartIter=channelList()->begin();
   std::vector<unsigned int>::iterator chanEndIter=channelList()->end();
   for (std::vector<unsigned int>::iterator chanIter=chanStartIter; chanIter!=chanEndIter; chanIter++) {
      unsigned int id=(*chanIter);
      DataPage* page=dataPage(id);
      unsigned int* data=page->virtualAddress();
      if (data[0]!=0xdd1234dd) {
         std::cout << "Bad fragment marker " << std::hex << data[0] << " for channel " << id << std::dec << " l1 id " << level1Id() << std::endl;
      }

      ioVec[index].iov_base=data;
      ioVec[index].iov_len=data[1]*sizeof(unsigned int);
      messageSize+= ioVec[index].iov_len ;
      index++;
   }

   ioVec[index].iov_base=0;
   ioVec[index].iov_len=0;

   if (addHeader) {
      m_header[1]=messageSize/sizeof(unsigned int);
   }

    //    std::cout << "DataRequestDescriptor::fragment index=" << index << std::endl;
    return messageSize;
}



// ************************************************************************

unsigned int DataRequestDescriptor::getNumDMAs(){
	return m_numDMAs;
}

void DataRequestDescriptor::setNumDMAs(unsigned int numDMAs){
	m_numDMAs = numDMAs;
}
