// $Id: EmulatedTrigger.cpp 56430 2009-01-27 19:00:10Z gcrone $
// //////////////////////////////////////////////////////////////////////
//  Dummy TriggerIn for testing
//
// The trigger generation algorithm is : for EACH L1id it's decided
// whether to generate a  request ( ROI, EB or delete) or not
// This results in a (small) overhead per L1ID but the code is much simpler.
//
// Authors: Markus Joos & Jorgen Petersen ATLAS/TDAQ
//
// NB! it is assumed  that ROLs are numbered consecutively from
// firstROL to lastROL e.g. 1 to 8
//
// //////////////////////////////////////////////////////////////////////

#include <vector>
#include <iostream>
#include <cstdlib>
#include "rcc_time_stamp/tstamp.h"


#include "EmulatedTrigger.h"


#include "DFSubSystemItem/Config.h"
#include "DFDebug/DFDebug.h"

#include "ROSIO/IOException.h"
#include "ROSCore/DataChannel.h"
#include "ROSCore/CoreException.h"
#include "ROSUtilities/ROSErrorReporting.h"

using namespace ROS;


/************************************/
EmulatedTrigger::EmulatedTrigger() 
/************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTrigger::constructor:  called");
  m_numberOfLevel1Last=0;

}

/************************************/
EmulatedTrigger::~EmulatedTrigger() noexcept
/************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTrigger::destructor:  called");
}

/*****************************************************************************************/
void EmulatedTrigger::setup(IOManager* iomanager, DFCountedPointer<Config> configuration)
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTrigger::setup:  called");
  m_configuration=configuration;
  m_iomanager=iomanager;
}

/*********************************/
void EmulatedTrigger::configure(const daq::rc::TransitionCmd&)
/*********************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTrigger::configure:  called");

  getModules(m_iomanager);

  char ebqueue[]="EBQueue";
  m_ebQueue=DFFastQueue<unsigned int>::Create(ebqueue,200);
  char relqueue[]="RELEASEQueue";
  m_releaseQueue=DFFastQueue<unsigned int>::Create(relqueue,200);

  m_generator = new Generator(this,m_configuration,m_ebQueue,m_releaseQueue);


   setupQueues(m_configuration);
}


/***********************************/
void EmulatedTrigger::unconfigure(const daq::rc::TransitionCmd&)
/***********************************/
{
   DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTrigger::unconfigure:  called");
   delete m_generator;

   // Get all our descruptors back in m_dataDescriptors and m_clearDescriptors 
   processDescriptorQueue();

   deleteQueues();
}


/***************************************/
void EmulatedTrigger::prepareForRun(const daq::rc::TransitionCmd&)
/***************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTrigger::prepareForRun:  called");
  ts_clock(&m_tsStart);
  m_tsStopLast = m_tsStart;
  m_numberOfLevel1Last=0;

  m_generator->clearCounters();
  m_generator->resetQueues();

  start();
  startExecution();
}


/***********************************/
void EmulatedTrigger::stopGathering(const daq::rc::TransitionCmd&)
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTrigger::stopEB:  called");
  stopExecution();
  waitForCondition(TERMINATED);  //, m_stopTimeout);

  m_generator->flushReleaseQueue();
  stop();
}


ISInfo* EmulatedTrigger::getISInfo() {
   return m_generator->getISInfo();
}

/***************************/
void EmulatedTrigger::run()
/***************************/
{
  // The main action thread.  Generates L2 requests, EB requests and
  // release requests in proportion controlled by the configuration. 
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTrigger::run:  called");
  ts_clock(&m_tsStart);
  m_tsStopLast = m_tsStart;
  m_generator->generate(0, 1) ;
}


/*******************************/
void EmulatedTrigger::cleanup()
/*******************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTrigger::cleanup:  called");
  m_generator->printStatistics();
}


/***********************************************/
int EmulatedTrigger::processDescriptorQueue() {
/***********************************************/
   int nProc=0;
   while (RequestDescriptor* descriptor=m_descriptorQueue->fastPop(false)) {
      nProc++;
      if (DataRequestDescriptor* dataDescr=dynamic_cast<DataRequestDescriptor*>(descriptor)) {
         if (dataDescr->terminated() || !m_triggerActive) {
            m_dataDescriptors[m_nextDataDescriptor++]=dataDescr;

            //debug
            if (m_nextDataDescriptor>m_nDataDescriptors) {std::cout << "+ WARNING + + WARNING + Too many data descriptors " << m_nextDataDescriptor << " + WARNING + + WARNING + \n";}
            //

            if (m_triggerActive) {
               if (dataDescr->transactionId()==0) {
                  m_ebQueue->push(dataDescr->level1Id());
               }
               else {
                  m_releaseQueue->push(dataDescr->level1Id());
               }
            }
         }
         else {
            dataDescr->retry();
         }
      }
      else if (ClearRequestDescriptor* clearDescr=dynamic_cast<ClearRequestDescriptor*>(descriptor)) {
         m_clearDescriptors[m_nextClearDescriptor++]=clearDescr;
      }
   }
   //   if (nProc>0)   std::cout << "EmulatedTrigger::processDescriptorQueue  processed " << nProc << " descriptors\n";
   return nProc;
}


/***********************************************************************/
EmulatedTrigger::Generator::Generator(EmulatedTrigger* trigger,
                                        DFCountedPointer<Config> config,
                                        DFFastQueue<unsigned int>* ebQueue,
                                        DFFastQueue<unsigned int>* releaseQueue)
   : TriggerGenerator(config),
     m_trigger(trigger),
     m_ebQueue(ebQueue),
     m_releaseQueue(releaseQueue)
/***********************************************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTrigger::Generator constructor  called");

// garbage collection parameters
  m_testGarbageCollection = config->getBool("TestGarbageCollection");
  if (m_testGarbageCollection) {
    m_deltaNumberOfLostClears = config->getInt("DeltaNumberOfLostClears");
    m_clearLossThreshold = config->getInt("ClearLossThreshold");
  }

  std::cout << " Generator::Generator: m_testGarbageCollection   = " << m_testGarbageCollection << std::endl;
  if (m_testGarbageCollection) {
    std::cout << " m_deltaNumberOfLostClears = " << m_deltaNumberOfLostClears << std::endl;
    std::cout << "m_clearLossThreshold = " <<m_clearLossThreshold << std::endl;
  }

  unsigned int nChannels=config->getInt("numberOfChannels");
  for (unsigned int channel=0; channel<nChannels; channel++) {
     m_channelList.push_back(config->getInt("channelId", channel));
  }

}

/***********************************************************************/
EmulatedTrigger::Generator::~Generator() {

  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTrigger::Generator destructor  called");

}

/***********************************************************************/
void EmulatedTrigger::Generator::l2Request(const unsigned int level1Id,
                                  const std::vector<unsigned int> * rols,
                                  const unsigned int /*destination*/,
                                  const unsigned int transactionId)
/***********************************************************************/
{
   m_trigger->submitDataRequest(level1Id,
                                rols,
                                transactionId,
                                0,0);
   m_stats.l2RequestsQueued++;
}

/*****************************************************************************************/
void EmulatedTrigger::Generator::releaseRequest(const std::vector<unsigned int>* level1Ids,
						  const unsigned int oldestLevel1Id)
/*****************************************************************************************/
{    
   m_releaseRequestsReceived++;
   if (m_testGarbageCollection && (m_releaseRequestsReceived % m_deltaNumberOfLostClears) == 0) {
     m_stats.clearRequestsLost++;
     if ((m_stats.clearRequestsLost % m_clearLossThreshold) == 0) {
       DEBUG_TEXT(DFDB_ROSTG, 5, "Generator::releaseRequest : oldestLevel1Id = " << oldestLevel1Id);
       //       m_ioManager->queueRequest(new GarbageCollectionRequest(oldestLevel1Id, DataChannel::channels()));
       m_stats.gcRequestsQueued++;
     }
   }
   else {
      unsigned int* l1List=new unsigned int[level1Ids->size()+1];
      l1List[0]=level1Ids->size();
      int index=1;
      for (std::vector<unsigned int>::const_iterator idIter=level1Ids->begin();
           idIter!=level1Ids->end(); idIter++) {
         l1List[index++]=*idIter;
      }
      m_trigger->submitClearRequest(l1List, level1Ids->size(), oldestLevel1Id,m_stats.clearRequestsQueued++);
   }
   m_stats.numberOfLevel1+=level1Ids->size();	// outside or inside loop?  FIXME
}

/*****************************************************************************************/
void EmulatedTrigger::Generator::ebRequest(const unsigned int level1Id,
                                             const unsigned int /*destination*/)
/*****************************************************************************************/
{
   m_trigger->submitDataRequest(level1Id,
                                &m_channelList,
                                0,
                                0,0);
   m_stats.ebRequestsQueued++;
}

/*****************************************************************************************/
void EmulatedTrigger::Generator::flushReleaseQueue(void)
/*****************************************************************************************/
{
  processReleaseQueue(true);
}

/*****************************************************************************************/
void EmulatedTrigger::Generator::clearCounters()
/*****************************************************************************************/
{
   m_stats.l2RequestsQueued=0;
   m_stats.clearRequestsQueued=0;
   m_stats.clearRequestsLost=0;
   m_stats.gcRequestsQueued=0;
   m_stats.numberOfLevel1=0;
   m_stats.ebRequestsQueued=0;
   m_releaseRequestsReceived=0;
}

void EmulatedTrigger::Generator::resetQueues()
{
   DEBUG_TEXT(DFDB_ROSTG, 10, "EmulatedTrigger::Generator::resetQueues Clearing " << m_ebQueue->numberOfElements()
             << " entries from EB queue, " << m_releaseQueue->numberOfElements() << " entries from release queue");

   m_ebQueue->reset();
   m_releaseQueue->reset();


   std::cout << "Release queue size " << releaseQueueSize() << std::endl;
   while (!m_queue_ebrequest_ids.empty()) m_queue_ebrequest_ids.pop();
   while (!m_queue_all_ids.empty()) m_queue_all_ids.pop();
   while (!m_queue_ebfinished_ids.empty()) m_queue_ebfinished_ids.pop();

   std::cout << "Release queue size " << releaseQueueSize() << std::endl;
}

/** Shared library entry point */
extern "C" 
{
   extern TriggerIn* createEmulatedTrigger();
}
TriggerIn* createEmulatedTrigger()
{
   return (new EmulatedTrigger());
}
