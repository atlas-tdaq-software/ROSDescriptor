// -*- c++ -*-
//
#ifndef ROS_DATA_DUMMY_OUTPUT_MSG_H
#define ROS_DATA_DUMMY_OUTPUT_MSG_H

#include "ROSasyncmsg/RosDataMsg.h"

#define FRAG_SIZE 0x1800

namespace ROS {
class RosDataDummyOutputMsg: public RosDataMsg,
                        public daq::asyncmsg::OutputMessage {
public:

   explicit RosDataDummyOutputMsg(std::uint32_t transactionId,
                                       std::uint32_t level1Id)
      : RosDataMsg(transactionId) {
      m_frag[13]=level1Id;
      m_frag[FRAG_SIZE-3]=0;
      m_frag[FRAG_SIZE-2]=FRAG_SIZE-12;
   };

   virtual void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const {
      //std::cout << "RosDataDummyOutputMsg::toBuffers()\n";
      buffers.emplace_back(m_frag,4*FRAG_SIZE);
   }

   std::uint32_t size() const {return 4*FRAG_SIZE;};

   unsigned int m_frag[FRAG_SIZE]={0xdd1234dd, 0x00000090, 0x00000008, 0x05000000, 0x004200c0,
                             0x00000001, 0x00000000, 0x00000000, 0xee1234ee, 0x00000009,
                             0x03010000, 0x004200c0, 0x00000000};
};
}
#endif
