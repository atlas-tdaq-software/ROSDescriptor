// -*- c++ -*- $Id: $

/************************************************************************/
/*  ATLAS ROS Software							*/
/*									*/
/*  Class: RobinDescriptorReadoutModule						*/
/*  Author: Markus Joos, PH/ESS						*/	
/**** C 2005 - Ecosoft: Made from at least 70% recycled source code *****/

#ifndef ROBINDESCRIPTORREADOUTMODULE_H
#define ROBINDESCRIPTORREADOUTMODULE_H

#include <vector>
#include <string>
#include <map>


#include "RobinDescriptorChannel.h"


#include "RobinThread.h"

#include "ROSDescriptor/DescriptorReadoutModule.h"


namespace ROS 
{
   class WrapperMemoryPool;
   class DDTScheduledUserAction;
   class RobinDescriptorReadoutModule : public DescriptorReadoutModule 
  {
  public:
    RobinDescriptorReadoutModule(void);
    virtual ~RobinDescriptorReadoutModule() noexcept;
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual const std::vector<DataChannel *> * channels();
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopGathering(const daq::rc::TransitionCmd&);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);         //MJ: undo setup() (e.g. ROBIN_Close)
    virtual void clearInfo();
    virtual void publishFullStatistics(void);
    virtual void disable(const std::vector<std::string>& uid);
    virtual void enable(const std::vector<std::string>& uid);
    virtual void user(const daq::rc::UserCmd& command);
     virtual void publish();

     virtual void clearFragments(std::vector<unsigned int> &l1IDs);
     virtual void requestFragment(DataRequestDescriptor* descriptor);
     virtual void registerChannels(RequestDescriptor* descriptor);
  private:
    std::string getBistErrorString(u_int bistStatus);

    WrapperMemoryPool *m_memoryPool;
    RobinThread* m_robIn;
    u_int m_physicalAddress;           // The RobIn ID number (= PCI logical number)
    u_int m_numberOfDataChannels;
    u_int m_firstRolPhysicalAddress;
    u_int m_rolPhysicalAddressTable[3];
    std::map <std::string, RobinDescriptorChannel *> m_uidMap;
    std::vector <DataChannel *> m_dataChannels;
    DDTScheduledUserAction *m_DDTscheduledUserAction;
    static bool s_firstModuleFound;
    static bool s_DDTCreated;
    DFCountedPointer<Config> m_configuration;

     std::map<u_int, RobinDescriptorChannel*> m_channelMap;
  };
}
#endif 
