// -*- c++ -*-
#ifndef METDATAREQUESTDESCRIPTOR_H
#define METDATAREQUESTDESCRIPTOR_H

#include "ROSDescriptor/DataRequestDescriptor.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSEventFragment/RODFragment.h"


namespace ROS {
   //! Special DataRequestDescriptor for Missing Et data requests.

   //! When a special channel Id is given in the initialise method,
   //!data is requested from all the configured robin data channels and
   //!a pseudo ROB fragment containing just the energy sums
   //!extracted from them is constructed. Any other channel Ids given are
   //!processed as for a normal DataRequestDescriptor.
   class MeTDataRequestDescriptor : public DataRequestDescriptor {
   public:
      MeTDataRequestDescriptor(unsigned int timeout);

      virtual ~MeTDataRequestDescriptor();


      /**
       * Per request initialisation. we construct the MEt ROBHeader
       * here so we have to override the base class implementation.
       **/
      virtual void initialise(unsigned int level1Id,
                              const std::vector<unsigned int>* channelList,
                              unsigned int transactionId,
                              unsigned int sequence, unsigned int tag);

      /**
       *  Here we actually build the MeT event fragment from the
       *  energy sums in the ROBFragments retrieved from the robins
       **/
      virtual unsigned int fragment(struct iovec*,bool addHeader);

      /**
       *  Our channel count includes any specifically requested data
       *  channels plus our pseudo channel for the Missing Et.
       **/
      virtual unsigned int channelCount();

   private:

      static const unsigned int c_maxMetBlockSize=50;
      static const unsigned int c_metHeaderSize=10;
      static const unsigned int c_nStatusElements=2;

      bool processFeb(unsigned int* dataPtr);
      void addEmptyBlock();
      void append(unsigned int* robFragment);

      bool m_metRequested;
      unsigned int m_sourceId;

      ROBFragment::ROBHeader* m_robHeader;
      RODFragment::RODHeader* m_rodHeader;
      unsigned int* m_metBuffer;  //!< Buffer where the MEt fragment is built. should be allocated in the constructor.
      unsigned int* m_fragStatus; //!< Pointer to the MEt fragments status word array in the MEt buffer
      unsigned int* m_robCount;   //!< Poiinter to the word containing the ROB count in the MEt buffer
      unsigned int* m_writePtr;   //!< Pointer to the next free word in the MEt buffer

      bool m_needHeader;       //!< Flag whether we have copied a ROD header from one of our input channels yet.
      bool m_firstAppend;      //!< Flag if this is the first ROB fragment to be processed for this event

      unsigned int m_descriptorId;
      static unsigned int s_nextId;
   };
}
#endif
