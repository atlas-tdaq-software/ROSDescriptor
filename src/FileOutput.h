// -*- c++ -*- $Id: FileOutput.h 92996 2010-07-08 14:06:24Z gcrone $ 
// ///////////////////////////////////////////////////////////////////////////
//
// $Log$
// Revision 1.5  2008/11/13 15:14:51  gcrone
// Use separate counter for number of calls to sendData and the number of fragments actually output
//
// Revision 1.4  2008/07/18 13:11:41  gcrone
// Specify namespace std:: where necessary
//
// Revision 1.3  2008/04/09 13:53:48  gcrone
// Take type argument in constructor/create function
//
// Revision 1.2  2008/02/28 18:17:23  gcrone
// Update for new DataStorageCallback interface in tdaqCommon-01.09
//
// Revision 1.1  2008/02/19 13:58:08  gcrone
// Split file writing and IO emulation DataOuts
//
//
// ///////////////////////////////////////////////////////////////////////////
#ifndef FILEDATAOUT_H
#define FILEDATAOUT_H

#include <string>

#include "ROSDescriptor/DescriptorDataOut.h"

#include "DFThreads/DFFastMutex.h"

#include "EventStorage/DataWriterCallBack.h"
#include "ROSInfo/DataOutInfo.h"

#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX 256
#endif


namespace EventStorage {
   class DataWriter;
}
namespace SFOTZ {
   class SFOTZThread;
   class SFOTZDataWriterCallback;
}
namespace ROS {
   /**
      Output plugin to write fragments to file.
   */
   class FileOutput : public DescriptorDataOut
   {
      class StorageCallback: public EventStorage::DataWriterCallBack {
      public:
         StorageCallback(std::string partitionName, std::string appName, bool interactiveMode);
         virtual ~StorageCallback();
         // this will be called after a successful file opening
         virtual void FileWasOpened(std::string logicalfileName,
                                    std::string fileName,
                                    std::string streamtype,
                                    std::string streamname,
                                    std::string sfoid,
                                    std::string guid,
                                    unsigned int runNumber, 
                                    unsigned int filenumber,
                                    unsigned int lumiblock);

         // this will be called after a successful file closing
         virtual void FileWasClosed(std::string logicalfileName,
                                    std::string fileName,
                                    std::string streamtype,
                                    std::string streamname,
                                    std::string sfoid,
                                    std::string guid,
                                    std::string checksum,
                                    unsigned int eventsInFile, 
                                    unsigned int runNumber, 
                                    unsigned int filenumber,
                                    unsigned int lumiblock,
                                    uint64_t filesize);
      private:
         std::string m_isName;
         std::string m_hostName;
         std::string m_partition;
         bool m_interactiveMode;
      };
   public:
      FileOutput(const DataOut::Type type);
      virtual ~FileOutput() noexcept;
      void setup(DFCountedPointer<Config> configuration);

      void prepareForRun(const daq::rc::TransitionCmd&);
      void stopGathering(const daq::rc::TransitionCmd&);

      void sendData(const Buffer* buffer, const NodeID,
                    const unsigned int transactionId=1,
                    const unsigned int status=0);

      void send(DataRequestDescriptor* descriptor);
      void userCommand(std::string& s1,std::string& s2);
      ISInfo* getISInfo();
   private:
      DataOutInfo m_statistics;
      bool m_doNothing;
      std::string m_writingPath;
      int m_outputFile;
      int m_samplingGap;

      DFFastMutex* m_mutex;

      // For the EventStorage DataWriter 
      bool m_useEventStorage;
      int m_maxMegaBytesPerFile;
      int m_maxEventsPerFile;
      EventStorage::DataWriter* m_dataWriter;
      StorageCallback* m_storageCallback;

      std::string m_appName;
      std::string m_fileTag;
      std::string m_streamType;

      SFOTZ::SFOTZThread* m_dbthread;
      SFOTZ::SFOTZDataWriterCallback* m_dbCallBack;

      DFCountedPointer<Config> m_configuration;

      // Values extracted from runParameters
      bool m_recording;
      unsigned int m_runNumber;
      unsigned int m_maxEvents;
      unsigned int m_totalInput;
   };
}
#endif
