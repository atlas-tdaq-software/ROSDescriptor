#include <thread>
#include <chrono>

#include "DFDescriptorTrigger.h"
#include "DFRequestDescriptor.h"
#include "DFDataCollector.h"

//#define LOOPBACK
#ifdef LOOPBACK
# include "RosDataDummyOutputMsg.h"
#endif

#include "ROSasyncmsg/DataServer.h"

#include "ROSasyncmsg/DataRequestMsg.h"
#include "ROSasyncmsg/RosDataMsg.h"
#include "ROSasyncmsg/ClearMsg.h"

#include "RosDataDescriptorOutputMsg.h"

#include "asyncmsg/asyncmsg.h"

#include "ROSDescriptor/ObjectQueue.h"

#include "ROSInfo/DcTriggerInInfo.h"

#include "ROSCore/ReadoutModule.h"
#include "ROSDescriptor/DescriptorReadoutModule.h"

#include "dal/Partition.h"
#include "DFdal/DFParameters.h"



using namespace ROS;



DFDescriptorTrigger::DFDescriptorTrigger():
   m_nameService(0),
   m_monitoringService(monsvc::MonitoringService::instance()) {
   resetCounters();

#ifdef LOOPBACK
   std::cout << "\n\t==================================================\n"
             << "\t[                                                ]\n"
             << "\t[    WARNING     WARNING    WARNING              ]\n"
             << "\t[            DUMMY data version                  ]\n"
             << "\t[    WARNING     WARNING    WARNING              ]\n"
             << "\t[                                                ]\n"
             << "\t==================================================\n\n";

#endif
}


DFDescriptorTrigger::~DFDescriptorTrigger() noexcept {
}

void DFDescriptorTrigger::setup(IOManager* iom, DFCountedPointer<Config> config) {
   m_ioManager=iom;

   m_nDataServerThreads=config->getInt("nDataServerThreads");
   m_nIOServices=config->getInt("nIOServices");

   m_standalone=config->isKey("standalone");

   m_nDataDescriptors=config->getInt("DataDescriptors");
   m_dataRequestTimeout=config->getUInt("DataRequestTimeout");

//    std::string myName=config->getString("UID");
//    m_triggerConf=
   //unsigned int detectorId=config->getUInt("SubDetectorId");

}

void DFDescriptorTrigger::configure(const daq::rc::TransitionCmd&) { 


   for (auto module : *(m_ioManager->readoutModules())) {
      DescriptorReadoutModule* descrModule=
         dynamic_cast<DescriptorReadoutModule*>(module);
      if (descrModule) {
         m_readoutModules.push_back(descrModule);
      }
      else {
         std::cerr << "ReadoutModule is not a DescriptorReadoutModule\n";
      }
   }


   for (unsigned int entry=0; entry<m_nDataDescriptors; entry++) {
      DFRequestDescriptor* descriptor=new DFRequestDescriptor(m_dataRequestTimeout);
      m_dataDescriptors.push_back(descriptor);
      for (auto module : m_readoutModules) {
         module->registerChannels(descriptor);
      }
   }
   m_nextDataDescriptor=m_nDataDescriptors;

   m_descriptorQueue=
      new ObjectQueue<RequestDescriptor>(m_nDataDescriptors,"Trigger return ");


   for (unsigned int entry=0; entry<m_nDataDescriptors; entry++) {
      m_descriptorQueue->fastPush(m_dataDescriptors[entry]);
   }

   unsigned int nChannels=dynamic_cast<DFRequestDescriptor*>(m_dataDescriptors[0])->nChannels();
   std::cout << "\n\n DFDescriptorTrigger::setup n channels=" << nChannels << std::endl;
   m_collectorQueue=new ObjectQueue<RequestDescriptor>(nChannels*m_nDataDescriptors, "Collector input ");
   m_dataCollector=new DFDataCollector(this,
                                   m_collectorQueue,
                                   m_descriptorQueue);

   unsigned int maxTime=m_dataRequestTimeout;
   if (maxTime==0) {
      maxTime=500000;
   }
   m_timeHistogram=m_monitoringService.register_object("/EXPERT/requestLatency",
                                                       new TH1F("requestLatency", "Request latency (#mu s)", 500, 0, float(maxTime)),
                                                       true);
   float maxSize=nChannels*512;
   m_sizeHistogram=m_monitoringService.register_object("EXPERT/dataSize",
                                                       new TH1F("dataSize","data size (32bit words)", 500, 0, maxSize),
                                                       true);

   const daq::df::DFParameters* dfPars=
      m_configurationDB->cast<daq::df::DFParameters>(m_partition->get_DataFlowParameters());

   if (!m_standalone) {
      // Try and get networks from OKS
      m_ipcPartition=IPCPartition(m_partition->UID().c_str());

      std::vector<std::string> networks=dfPars->get_DefaultDataNetworks();
//       std::cout << "Network list:\n";
//       for (auto niter=networks.begin(); niter!=networks.end(); niter++) {
//          std::cout << (*niter) << std::endl;
//       }
//       std::cout << "DFDescriptorTriggerIn::configure() Creating NameService\n";
      m_nameService=new daq::asyncmsg::NameService(m_ipcPartition,
                                                   networks);
   }

   std::cout << "DFDescriptorTrigger::configure() opening data server\n";
//   m_dataIoServices.reset(new std::vector<boost::asio::io_service>(m_nDataServerThreads));
   m_dataIoServices.reset(new std::vector<boost::asio::io_service>(m_nIOServices));
   m_dataServer=std::make_shared<DescriptorDataServer>(*m_dataIoServices,
                                                       m_ioManager->getName(),
                                                       m_nameService,
                                                       this);
   m_dataServer->configure(m_nDataServerThreads);
   //------>>
   int service=m_dataServer->getIoService();
   std::shared_ptr<DescriptorDataServerSession> session=std::make_shared<DescriptorDataServerSession>(
                                (*m_dataIoServices)[service],
                                this);
   m_dataServer->asyncAccept(session);
   //------>>


   std::string mcAddress=dfPars->get_MulticastAddress();
   std::string proto=mcAddress.substr(0,mcAddress.find(":"));
   if (!proto.empty()) {
      mcAddress=mcAddress.substr(mcAddress.find(":")+1);
   }

   m_clearIoService.reset(new std::vector<boost::asio::io_service>(1));
   if (mcAddress.empty() || proto=="tcp") {
      // use TCP
      m_clearServer=std::make_shared<DescriptorDataServer>(*m_clearIoService,
                                                           m_ioManager->getName(),
                                                           m_nameService,
                                                           this);
      m_clearServer->configure(1,"CLEAR_");
      m_clearServer
         ->asyncAccept(std::make_shared<DescriptorDataServerSession>(
                          (*m_clearIoService)[0],this));
      std::cout << "DFDescriptorTrigger created unicast ClearSession\n";
   }
   else {
       auto sPos=mcAddress.find("/");
       std::string mcInterface;
       if (sPos!=std::string::npos) {
          mcInterface=mcAddress.substr(sPos+1);
       }
       mcAddress=mcAddress.substr(0,sPos);
       m_clearSession=std::make_shared<DescriptorClearSession>((*m_clearIoService)[0],
                                                               mcAddress,
                                                               mcInterface,
                                                               this);
       m_clearSession->asyncReceive();
       std::cout << "DFDescriptorTrigger created multicast ClearSession\n";
   }

   for (auto module : m_readoutModules) {
      module->setCollectorQueue(m_collectorQueue);
   }

}

void DFDescriptorTrigger::prepareForRun(const daq::rc::TransitionCmd&){
//   std::cout << "DFDescriptorTrigger::prepareForRun()\n";
   // Descriptor::start will start the data collector thread
   resetCounters();
   m_timeHistogram->Reset();
   start();
}

void DFDescriptorTrigger::unconfigure(const daq::rc::TransitionCmd&){

   deleteQueues();

   m_dataServer->unconfigure();
   m_dataServer.reset();

   if (m_clearServer) {
      m_clearServer->unconfigure();
      m_clearServer.reset();
   }

   if (m_clearSession) {
      m_clearSession->close();
      m_clearSession.reset();
   }
   if (m_clearIoService) {
      for (unsigned int service=0;service<m_clearIoService->size();service++) {
         m_clearIoService->at(service).stop();
      }
      m_clearIoService.reset();
   }

   if (m_nameService!=0) {
      delete m_nameService;
      m_nameService=0;
   }

   if (m_monitoringService.remove_object(m_timeHistogram.get())) {
      std::cout << "Deregistered timing histo\n";
   }
   if (m_monitoringService.remove_object(m_sizeHistogram.get())) {
      std::cout << "Deregistered size histo\n";
   }
}


void DFDescriptorTrigger::stopGathering(const daq::rc::TransitionCmd&){
   std::cout << "DFDescriptorTrigger::stopEB: Stopping threads\n";
   m_triggerActive = false; 
   stop();
}

unsigned int DFDescriptorTrigger::triggerCount() {
   return m_stats.numberOfLevel1;
}


int DFDescriptorTrigger::processDescriptorQueue() {
   return 0;
}


void DFDescriptorTrigger::submitDataRequest(unsigned int level1Id,
//                                  std::unique_ptr<std::vector<unsigned int> > dataChannels,
                                  std::vector<unsigned int>* dataChannels,
                                  DescriptorDataServerSession* destination,
                                  unsigned int transactionId){
   m_requestsReceived++;
   std::chrono::time_point<std::chrono::system_clock> requestTime=std::chrono::system_clock::now();
   DFRequestDescriptor* descriptor=
      dynamic_cast<DFRequestDescriptor*>(m_descriptorQueue->pop());
   if (descriptor){
//      descriptor->initialise(level1Id, std::move(dataChannels), destination, transactionId);
      descriptor->initialise(level1Id, dataChannels, destination, transactionId, requestTime);
      //descriptor->submit();
      for (auto module : m_readoutModules) {
         module->requestFragment(descriptor);
      }

      descriptor->submitted(true);
   }
   else {
      std::cerr << "Submitting request which is not a DataRequest!!\n";
   }
}

void DFDescriptorTrigger::resubmit(DataRequestDescriptor* descriptor) {
   DFRequestDescriptor* dfDescriptor=dynamic_cast<DFRequestDescriptor*>(descriptor);
   dfDescriptor->submitted(false);
   for (auto module : m_readoutModules) {
      module->requestFragment(descriptor);
   }
   dfDescriptor->submitted(true);
}

void DFDescriptorTrigger::submitClearRequest(std::vector<unsigned int>& level1IdList) {
   for (auto module : m_readoutModules) {
      module->clearFragments(level1IdList);
   }
   m_clearRequestsReceived++;
   m_stats.numberOfLevel1+=level1IdList.size();
}

void DFDescriptorTrigger::finalise(DFRequestDescriptor* descriptor) {
   auto elapsed=std::chrono::system_clock::now()-descriptor->requestTime();
   m_timeHistogram->Fill(std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count());
   m_sizeHistogram->Fill(descriptor->size());
}

ISInfo* DFDescriptorTrigger::getISInfo() {
   m_stats.dataRequestDescriptorsFree=m_descriptorQueue->size();
   m_stats.requestsTimedOut=reinterpret_cast<DFDataCollector*>(m_dataCollector)->timeOuts();
   m_stats.clearRequestsReceived=m_clearRequestsReceived;
   m_stats.requestsReceived=m_requestsReceived;

   auto now=std::chrono::system_clock::now();
   auto elapsed=now-m_lastStatsTime;
   float seconds=(float)std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count()/1e6;
   if (seconds>0) {
      float l1Interval=m_stats.numberOfLevel1-m_lastStats.numberOfLevel1;
      m_stats.level1Rate=l1Interval/seconds;
      float reqInterval=m_stats.requestsReceived-m_lastStats.requestsReceived;
      m_stats.requestRate=reqInterval/seconds;
//       std::cout << "\n\tL1 Rate " << m_stats.level1Rate << std::endl;
   }

   m_lastStats.requestsReceived=m_stats.requestsReceived;
   m_lastStats.clearRequestsMissed=m_stats.clearRequestsMissed;
   m_lastStats.numberOfLevel1=m_stats.numberOfLevel1;
   m_lastStatsTime=now;

   return &m_stats;
}

void DFDescriptorTrigger::resetCounters() {


   m_clearRequestsReceived=0;
   m_requestsReceived=0;

   m_stats.clearRequestsReceived=0;
   m_stats.requestsReceived=0;
   m_stats.clearRequestsMissed=0;
   m_stats.numberOfLevel1=0;

   m_lastStats.clearRequestsReceived=0;
   m_lastStats.requestsReceived=0;
   m_lastStats.clearRequestsMissed=0;
   m_lastStats.numberOfLevel1=0;

   m_lastStatsTime=std::chrono::system_clock::now();
   m_counterResetTime=std::chrono::system_clock::now();
}


// ==============================================================================


DescriptorDataServer::DescriptorDataServer(std::vector<boost::asio::io_service>& ioServices,
                                           const std::string& iomName,
                                           daq::asyncmsg::NameService* nameService,
                                           DFDescriptorTrigger* trigger) :
   DataServer(ioServices,iomName,nameService),m_trigger(trigger) {
}

DescriptorDataServer::~DescriptorDataServer() {
}
void DescriptorDataServer::onAccept(std::shared_ptr<daq::asyncmsg::Session> session) noexcept {
      std::lock_guard<std::mutex> lock(m_sessionsMutex);
      m_sessions.emplace_back(std::dynamic_pointer_cast<DataServerSession>(session));
      
      int server=getIoService();
      asyncAccept(std::make_shared<DescriptorDataServerSession>
               (m_ioServerService[server],m_trigger));
}



// ==============================================================================


DescriptorDataServerSession::DescriptorDataServerSession(boost::asio::io_service& ioService,
                                                         DFDescriptorTrigger* trigger) :
   DataServerSession(ioService),m_trigger(trigger) {
   

}
DescriptorDataServerSession::~DescriptorDataServerSession() {
}


void DescriptorDataServerSession::onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) {

   if (message->typeId()==DataRequestMsg::TYPE_ID) {
      std::unique_ptr<DataRequestMsg> requestMsg(dynamic_cast<DataRequestMsg*>(message.release()));
      if (requestMsg==0) {
         std::cerr << "Failed to cast to DataRequestMsg\n";
         return;
      }

//        std::cout << std::hex
//                  << "Received request message " << requestMsg->transactionId()
//                  << " for event " << requestMsg->level1Id()
//                  << " fingering " << requestMsg->nRequestedRols() << " ROLs:";
//        std::cout << std::dec << std::endl;
#ifdef LOOPBACK
      std::unique_ptr<RosDataDummyOutputMsg> dataMsg(
         new RosDataDummyOutputMsg(requestMsg->transactionId(),
                                   requestMsg->level1Id()));
      asyncSend(std::move(dataMsg));
#else
       m_trigger->submitDataRequest(requestMsg->level1Id(),
//                                    std::move(requestMsg->requestedRols()),
                                    requestMsg->requestedRols(),
                                    this,
                                    requestMsg->transactionId()
          );
#endif
   }
   else if (message->typeId()==ClearMsg::TYPE_ID) {
      std::unique_ptr<ClearMsg> clearMsg(dynamic_cast<ClearMsg*>(message.release()));
      if (clearMsg!=0) {
         //std::cout << "Received clear message for " << clearMsg->nEvents() << " events\n";
         m_trigger->submitClearRequest(clearMsg->m_l1Ids);
      }
      else {
         std::cerr << "failed to cast to ClearMsg\n";
      }
   }
   else 
      std::cout << "WTF type of message was that?\n";

   asyncReceive();
}

void DescriptorDataServerSession::onReceive(std::uint32_t, std::uint32_t) noexcept{
   ERS_LOG("Received 0 length message");
}


void DescriptorDataServerSession::onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage>  message) noexcept{
   //std::cout << "Sent message " << message->transactionId() << std::endl;
//   std::unique_ptr<const RosDataDescriptorOutputMsg>
//      dataMessage(dynamic_cast<const RosDataDescriptorOutputMsg*>(message.release()));
//   if (dataMessage) {
//      m_trigger->finalise(dataMessage->m_descriptor);
//   }
}

// ==============================================================================

void DescriptorClearSession::onReceive(
   std::unique_ptr<daq::asyncmsg::InputMessage> message) {
   if (message->typeId()==ClearMsg::TYPE_ID) {
      std::unique_ptr<ClearMsg> clearMsg(dynamic_cast<ClearMsg*>(message.release()));
      if (clearMsg!=0) {
         //std::cout << "Received clear message for " << clearMsg->nEvents() << " events\n";
         m_trigger->submitClearRequest(clearMsg->m_l1Ids);
      }
      else {
         std::cerr << "failed to cast to ClearMsg\n";
      }
   }
   asyncReceive();
}


/** Shared library entry point */
extern "C" {
   extern TriggerIn* createDFDescriptorTrigger();
}
TriggerIn* createDFDescriptorTrigger()
{
   return (new DFDescriptorTrigger());
}
