// -*- c++ -*- $Id: RobinDataDrivenTrigger.h 45472 2008-02-14 13:48:19Z gcrone $ 
// ///////////////////////////////////////////////////////////////////////////
//  $Log$
// ///////////////////////////////////////////////////////////////////////////
#ifndef ROBINDATADRIVENTRIGGER_H
#define ROBINDATADRIVENTRIGGER_H

#include "ROSCore/TriggerInputQueue.h"
#include "EmulatedTrigger.h"

class ISInfo;

namespace ROS {

   //! Descriptor version of data driven trigger for robin.

   //! Nothing special about this version except that it inherits from
   //! the Descriptor version of the emulated trigger. 
   class RobinDataDrivenTrigger : public EmulatedTrigger {
   public:
      RobinDataDrivenTrigger();
      virtual ~RobinDataDrivenTrigger() noexcept;

      virtual void configure(const daq::rc::TransitionCmd&);
      virtual void unconfigure(const daq::rc::TransitionCmd&);
   protected:
      virtual void run();
   private:
      TriggerInputQueue* m_inputToTriggerQueue;
   }; 
}

#endif
