// -*- c++ -*-
// $Id: EmulatedDescriptorModule.h 51543 2008-07-18 11:12:20Z gcrone $

/*
  ATLAS ROS Software

  Class: EmulatedDescriptorModule
  Authors: ATLAS ROS group 	
*/


#ifndef EMULATEDREADOUTMODULE_H
#define EMULATEDREADOUTMODULE_H

#include <thread>
#include <string>

#ifdef TBB_QUEUE
#include "tbb/concurrent_queue.h"
#else
#include "ROSDescriptor/ObjectQueue.h"
#endif

#include "ROSDescriptor/DescriptorReadoutModule.h"


namespace ROS {
   class DataRequestDescriptor;
   class DescriptorDataChannel;
   class EmulatedDescriptorModule : public DescriptorReadoutModule {
   public:
      EmulatedDescriptorModule();
      virtual ~EmulatedDescriptorModule() noexcept;
      virtual void setup(DFCountedPointer<Config> configuration);
      virtual void configure(const daq::rc::TransitionCmd&);
      virtual void unconfigure(const daq::rc::TransitionCmd&);
      virtual void clearInfo();
      virtual void prepareForRun(const daq::rc::TransitionCmd&);
      virtual void stopGathering(const daq::rc::TransitionCmd&);

      void user(const daq::rc::UserCmd& command);
      void dump();
      virtual const std::vector<DataChannel *> *channels(); //Get the list of channels connected to this module
      ISInfo *getISInfo();
      void queueRequest(DataRequestDescriptor*, unsigned int requestDelay,
//                        DescriptorDataChannel*);
                        unsigned int channelId);
      virtual void requestFragment(DataRequestDescriptor* descriptor);

      virtual void clearFragments(std::vector<uint32_t>& level1Ids);
      void registerChannels(RequestDescriptor* descr);
  private:
      void run();

      void gotFragment(unsigned int channelId, DataRequestDescriptor* descr);
      class Message {
      public:
         Message(DataRequestDescriptor* request,
                 unsigned int chan, //DescriptorDataChannel* chan,
                 uint64_t endTime) :
            m_completeTime(endTime), m_channel(chan), m_descriptor(request) {};
         uint64_t completeTime() {return m_completeTime;};
         //DescriptorDataChannel* channel() {return m_channel;};
         unsigned int channel() {return m_channel;};
         DataRequestDescriptor* descriptor() {return m_descriptor;};
      private:
         uint64_t m_completeTime;
         //DescriptorDataChannel* m_channel;
         unsigned int m_channel;
         DataRequestDescriptor* m_descriptor;
      };
      bool m_active;
      std::thread* m_thread;
//       std::vector<DataChannel*> m_dataChannels;
//       std::vector<DescriptorDataChannel*> m_channels;
//       std::map<unsigned int, DescriptorDataChannel*> m_channelMap;
      DFCountedPointer<Config> m_configuration;

#ifdef TBB_QUEUE
      tbb::concurrent_bounded_queue<Message*> m_messageQueue;
#else
      ObjectQueue<Message>* m_messageQueue;
#endif

      void channelRequestFragment(unsigned int,DataRequestDescriptor*, bool last);

      std::vector<unsigned int> m_channelIds;
      std::vector<unsigned int*> m_pages;
      unsigned int m_getDelay;
      unsigned int m_pageSize;
      unsigned int m_detectorEventType;	          // DB parameter "detectorEventType"
      unsigned int m_numberOfStatusElements;	  // DB parameter "NumberOfStatusElements"
      unsigned int m_maxDataElements;	  // Number of data words in the ROB (ROD) fragment 
      unsigned int m_statusBlockPosition;	  // DB parameter "StatusBlockPosition"
      float m_getMiss;	                       // DB parameter "GetMissingFragment". Controls the chance for a "may come"
      bool m_randomSize;

      uint64_t m_freeTime;

      unsigned int m_notCleared;
      unsigned int m_storeSize;
      unsigned int m_lastl1;
      unsigned int m_maxL1;
      unsigned int m_wrapCount;
      std::set<unsigned int> m_fragmentSet;
      std::mutex m_mutex;
      unsigned int m_lastRequestedId;
   };

   inline ISInfo* EmulatedDescriptorModule::getISInfo(){
      std::cout << "Number of failed clears: " << m_notCleared << std::endl;
      std::cout << "Event counter wrapped: " << m_wrapCount << std::endl;
      return 0;
   }
}
#endif //EMULATEDREADOUTMODULE_H
