#include "ROSDescriptor/DescriptorDataChannel.h"
#include "ROSMemoryPool/MemoryPool.h"
#include "ROSMemoryPool/MemoryPage.h"

using namespace ROS;


DescriptorDataChannel::DescriptorDataChannel(unsigned int id,
                                             unsigned int configId,
                                             unsigned int physicalAddress,
                                             unsigned int ackQueueSize,
                                             MemoryPool* mpool) :
   DataChannel(id, configId, physicalAddress), m_memoryPool(mpool) {
   if (ackQueueSize>0) {
      m_ackQueue=new ObjectQueue<RequestDescriptor>(ackQueueSize);
   }
   else {
      m_ackQueue=0;
   }
}


DescriptorDataChannel::~DescriptorDataChannel() {
   if (m_ackQueue) {
      delete m_ackQueue;
   }
}

ObjectQueue<RequestDescriptor>* DescriptorDataChannel::getAckQueue() {
   return m_ackQueue;
}

/*************************************/
DataPage* DescriptorDataChannel::getPage() {
/*************************************/
   if (m_memoryPool!=0) {
      MemoryPage* mPage=m_memoryPool->getPage();
      DataPage* dPage=new DataPage(reinterpret_cast<unsigned int*>(mPage->address()),
                                reinterpret_cast<unsigned int*>(mPage->physicalAddress()));
      return dPage;
   }
   else {
      std::cerr << "No memory pool set\n";
      return 0;
   }
}

#if 0
DescriptorDataChannel::releaseFragment(const std::vector <u_int> *level1Ids) {
/**************************************************************************/

}
#endif
